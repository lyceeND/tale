## Modularité

Utiliser des API (Application Programming Interface) ou des bibliothèques. Exploiter leur documentation
Créer des modules simples et les documenter

## Mise au point des programmes - Gestion des bugs

Dans la pratique de la programmation, savoir répondre aux causes typiques de bugs : pbs liés au typage,
effets de bords non désirés, débordement dans le tableaux, instruction conditionnalle non exhaaustive,
coix des inégalités, comparaisons et calculs entre flottants, mauvais nommage des variables

On prolonge le travail accompi en classe de première sur l'utilisation de la spécification, des assertions, de la documentation des programmes, et de la construction de jeux de tests. Les élèves prennent progressivement à anticiper leurs erreurs.
