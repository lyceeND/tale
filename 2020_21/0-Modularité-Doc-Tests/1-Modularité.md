# Modules et documentations

En bref :

> Écrire un module et sa documentation pour le distribuer ou le réutiliser plus
> tard, ainsi qu'utiliser des modules écrits par d'autres personnes (et donc en
> consulter la documentation) sont des actions courantes en développement
> logiciel, et plus particulièrement encore en Python où l'offre de modules
> tiers est pléthorique.

## Programmation modulaire

Le principe de modularité est particulièrement important dans tout développement
logiciel. Il simplifie les tests, permet de réutiliser du code, et facilite
la maintenance.

Ce principe peut opérer à plusieurs niveaux :

- découper le code en fonctions ;
- grouper les fonctions dans une classe (on les appelle alors méthodes) ;
- grouper des fonctions par thèmes, dans un fichier séparé ;
- grouper des fichiers en bibliothèque.
  
La programmation modulaire intervient :

- lors de l'écriture de portions de code pour les réutiliser plus tard,
  ou les distribuer ;
- lors de l'utilisation du code écrit par un autre
  programmeur, parce qu'il correspond à un besoin.

<!--Les deux types d'interactions sont traités dans cette fiche.-->

## Modularité Python

<!--Le langage Python permet d'utiliser la modularité aux différents niveaux évoqués
plus hauts :-->

- On peut (on doit...) naturellement écrire des fonctions et des procédures, et les réutiliser.
- Python permet de faire de la programmation orientée objets (fiche 9) et donc
  de grouper des fonctions (alors appelées méthodes) au sein de classes, selon le type d'objet sur
  lequel elles agissent.
- Fonctions, procédures et classes peuvent être groupées par thème dans un
  fichier séparé alors appelé **module**. Les modules peuvent eux-même être
  groupés en **packages**.
  <!--, généralement parce qu'elles relèvent du même thème (par exemple,
  des fonctions de cryptographie, ou des fonctions de traitement d'image). En
  Python, un tel fichier s'appelle un **module**. 
  Les *modules* Python peuvent être groupés ensemble au sein d'un **package**.
  Nous n'en parlerons pas particulièrement ici. La plupart des choses qui
  s'appliquent aux modules s'appliquent aussi aux packages.-->

## Importation des modules

<!-- Python est célèbre pour proposer des modules ou package pour à peu près
n'importe quelle tâche : faire du calcul symbolique, faire des graphismes, des
interfaces, tracer des courbes, analyser des pages Web... 
Utiliser des modules est donc extrêmement courant en Python. Outre les modules
de la bibliothèques standard (ce sont les modules qui viennent préinstallés avec
Python), comme le module  `math`, la plupart des modules externes sont
disponibles sur un dépôt appelé Pypi (Python Package Index).

L'installation de modules externe dépend de choses techniques comme : la version
du système d'exploitation, la version de Python, le type de distribution Python
etc... La méthode la plus courante est d'utiliser l'outil `pip`. Cherchez dans
la documentation spécifique à votre installation pour plus de détails.
-->

Certains modules Python sont
installés par défaut (librairie standard) et d'autres peuvent être ajoutés en
utilisant des outils comme `pip`. Le dépôt Pypi (PYthon Package Index, `https://pypi.org/`) référence
la plupart des modules tiers.

Pour importer un module installé, on utilise le mot clé `import`, dans une de
ses variantes :

~~~python
# Le module random est importé
import random
# Les fonctions doivent être préfixées du nom du module
a = random.randint(1, 10)
~~~

~~~python
# Le module est importé. Il est renommé en rnd
import random as rnd
# le nouveau nom donné est généralement plus court...
a = rnd.randint(1, 10)
~~~

~~~python
# On importe uniquement la fonction randint
from random import randint
# Plus de préfixe, mais seule la
# fonction randint est disponible
a = randint(1, 10)
~~~

Toutes les variantes contenant des `*` dans la ligne d'importation visent à
rendre disponibles des fonctions *sans avoir à les préfixer*, ce qui peut
paraître commode pour le programmeur débutant. En réalité, il n'en est rien : on ne
maîtrise plus la liste exacte de ce qui est importé dans l'espace de noms
principal, ce qui peut être source de bugs complexes à découvrir. De plus, en
reprenant plus tard son travail ou en le distribuant, on n'aura plus de trace
simple de l'endroit où se trouvent les fonctions et classes utilisées, rendant
la maintenance difficile : `import *` est à bannir !

## Documentation

Que l'on écrive une fonction, un module, ou qu'on en utilise, la documentation
est centrale pour que le travail soit réutilisable. Parmi les différents
mécanismes, l'un des plus simples est la **docstring** qui peut être
attachée à une fonction, à une procédure, à une méthode, à une classe, à un
module, ou à un package ! Dans tous les cas, c'est une chaîne de caractères qui
doit figurer au début de l'entité qu'elle documente.

<!-- 
- Donner un exemple de doctring ? Gérard <== A priori pas la place (TODO : VÉRIFIER ce 
point avec la mise en page définitive
- début de fonction ou procédure (juste après la ligne `def...`)
- début de fichier pour un module

-->

Cette documentation est ensuite consultable à l'aide de la commande `help` :

~~~python
>>> import random
>>> help(random) # affiche la docstring du module
>>> help(print) # affiche la docstring de la fonction
~~~

# Consultation de la documentation

Les fonctions, classes etc... disponibles dans un module et la façon de les
utiliser constituent l'API (Application Programming Interface) du module. C'est
cette API qui doit être documentée. 

Outre le module `math`, le module `itertools` (bibliothèque standard) contient
des fonctions d'énumération très pratiques. Voici comment consulter sa
documentation (accessible simplement parce que le module contient des docstrings)
:

~~~python
>>> import random
>>> help(random) # consultation de la docstring du module et liste des fonctions
Help on module random:

NAME
    random - Random variable generators.

MODULE REFERENCE
    https://docs.python.org/3.7/library/random

    The following documentation is automatically generated from the Python
    source files.  It may be incomplete, incorrect or include features that
    are considered implementation detail and may vary between Python
    implementations.  When in doubt, consult the module reference at the
    location listed above.
....

>>> help(random.choice)

choice(seq) method of random.Random instance
    Choose a random element from a non-empty sequence.
~~~

