# Débugage

En bref :

> La phase de débugage d'un programme est malheureusement très gourmande en
> temps. C'est particulièrement vrai avec des langages comme Python dont le
> typage est dynamique, repoussant ainsi au moment de l'exécution la découverte
> de certaines erreurs. Lire l'intégralité des messages d'erreur et se
> familiariser avec eux permet à la longue de gagner énormément de temps.

## Le Traceback de Python

Lorsque l'interpréteur Python rencontre un problème, une **exception** est levée.
Si elle n'est pas *capturée*, cette exception provoque l'arrêt du programme et
l'affichage d'un message appelé **traceback**. Ce message permet de connaître la
nature et le contexte de l'incident.

<!-- Cet arrêt peut avoir lieu à deux moments
différents : lors de la lecture du code par l'interpréteur, ou ensuite, lors de
son exécution.-->

## Erreurs de syntaxe

Les erreurs de syntaxe empêchent l'interpréteur de comprendre le code écrit et
provoquent la levée d'une exception *avant même l'exécution du code*. Ces erreurs sont
souvent faciles à trouver :

- parenthèse, crochet ou guillemet mal fermé (`SyntaxError`)
- mauvaise indentation (`IndentationError`)
  
Attention, le traceback indique la ligne où l'erreur a été *détectée* (qui n'est
pas forcément la ligne où l'erreur a été commise...).

:::{custom-style="NDA"}
Numéroter les 3 premières lignes du code suivant
:::

~~~python
for i in range(1, 10):
    print("Quel est le carré de {} ?".format(i)
    print("C'est : {}".format(i**2))
~~~

~~~python
File "tst.py", line 3
    print("C'est : {}".format(i**2))
        ^
SyntaxError: invalid syntax
~~~

L'erreur, signalée ligne 3, est en fait ligne 2 : *il manque une parenthèse
fermante*. Comme les instructions peuvent courir sur plusieurs lignes, elle
n'est détectée que ligne 3 !

:::{custom-style="postit"}
[Espace ou Tabulation?]{custom-style="titrepostit"}
Mélanger des espaces et des tabulations pour indenter le code est souvent
incorrect, et toujours déconseillé. Ce type d'erreur est difficile à repérer car
la largeur d'une tabulation peut correspondre à celle de quelques espaces. Il est conseillé de
configurer son éditeur pour que l'appui sur la touche `tab` provoque l'insertion
de 4 espaces (qui est le niveau d'indentation conseillé en Python)
:::

## Erreurs à l'exécution

Les exceptions levées à l'exécution sont plus difficiles à trouver, car elles
nécessitent de comprendre (à différents degrés) l'exécution du code. Elles sont
aussi plus variées :

- `NameError` : un nom de variable a-t-il été mal orthographié ?
- `IndexError` : l'indice utilisé est-il en dehors de la liste ?
- `TypeError` : a-t-on essayé d'ajouter un nombre et une chaîne de caractères ?
  
Outre le type d'exception et la ligne l'ayant levée, le traceback contient un
*historique* des appels de fonctions (la pile des appels) permettant de
connaître le contexte d'exécution. La recherche d'une erreur s'apparente alors à
une enquête : depuis l'endroit où l'erreur s'est déclarée, on remonte le fil
d'exécution pour en déterminer la cause, qui peut être à un tout autre endroit
(le traceback se lit pour cette raison de bas en haut) :

~~~python
Traceback (most recent call last):
  File "error.py", line 10, in <module>
    f2()
  File "error.py", line 7, in f2
    f1()
  File "error.py", line 3, in f1
    a = a / (b + c)
ZeroDivisionError: division by zero
~~~

Lors de l'exécution du fichier `error.py`, une exception de type
`ZeroDivisionError` a été levée, ligne 3 (`a = a / (b + c)`), dans la fonction
`f1`. La fonction en question a été exécutée suite à un appel sur la ligne 7,
dans le fonction `f2`, qui elle même a été appelée par la ligne 10 du programme
principal.


### Bug célèbre
*Grace Hopper (NSI 1ere fiche 25) rapporte en 1947 un cas de bug devenu célèbre : il s'agissait d'un
insecte ayant provoqué des erreurs de calcul dans un ordinateur Mark II.*


## Débugueur

Le débugueur permet de dérouler un programme pas à pas et de contrôler l'état de
chaque variable. C'est un outil parfois indispensable pour trouver des bugs complexes.

Le débugueur post-mortem permet d'inspecter l'état du programme
(contenu des variables par exemple), à partir du lieu où l'exception a été
levée. C'est souvent suffisant pour comprendre une erreur.

La plupart des environnements de développement proposent un débugueur.

<!---
Les principales sources d'erreurs, et comment les tracker.

Lister les NameError IndexError etc... et les expliquer.

Expliquer qu'en Python, les bugs à l'exécution sont difficiles à tracer et
nécessitent parfois une vraie enquête pour remonter au bug.

Montrer le débugger post mortem.
--->


