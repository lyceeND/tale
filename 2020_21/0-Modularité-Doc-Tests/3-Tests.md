# Tests

En bref :

> Écrire un programme dont on est capable de prouver mathématiquement la
> justesse est une tâche complexe. La plupart du temps, le programmeur doit se
> contenter de tester le code qu'il a écrit
> en vérifiant qu'il se comporte correctement sur des entrées variées pour
> lesquelles le résultat attendu est connu à l'avance.

## Développement logiciel

De l'écriture des spécifications à la validation d'un logiciel, chaque étape du
développement est idéalement accompagnée d'une phase de tests. Nous nous
intéressons ici aux **tests unitaires**
dont l'objectif est de tester indépendamment chaque fonction.

Ces tests peuvent être conçus avant ou après la fonction à tester,
éventuellement par une personne différente. Ils sont généralement exécutés aussi
souvent que possible pour éviter les problèmes de régression (ajouts de bugs
lors de modification du code).


Le développement d'un projet logiciel passe par plusieurs étapes clés dont :

- les spécifications : description des fonctionnalités
- la conception : choix d'une solution effective qui réalise les spécifications
- la programmation : implémentation des solutions
- la validation  : vérification de l'adéquation du produit avec les spécifications

Chacune de ces étapes est idéalement accompagnée de phases de tests qui, en
simplifiant, peuvent être de plusieurs natures :

### Types de tests

L'écriture de tests peut intervenir dans plusieurs des étapes précédentes. En
particulier, l'écriture de tests peut être réalisée **avant** la programmation
elle-même, et par une autre personne que le programmeur : l'objectif du
programmeur est d'écrire du code qui passe les tests.
Les tests peuvent être utilisés pour vérifier la non-régression du code : suite
à une modification, on repasse tous les tests pour être sûr de na pas avoir
introduit une nouvelle faute.

En simplifiant un peu, on peut distinguer les types de tests suivant :

* test unitaire : chaque petite fonction testée indépendamment pour toutes les
  entrées imaginables et éventuellement problématiques
* test d'intégration : on commence à faire interagir des morceaux de logiciels
  en supposant que chacun d'un est correct (puisque les tests unitaires sont
  passés)
* test système : en supposant que les tests unitaires et d'intégration sont passés,
  on teste les fonctionnalités globales du logiciel

Dans la suite, nous nous intéressons aux tests unitaires, qui visent à vérifier
qu'une fonction rend le bon résultat. Ces tests peuvent être écrits avant la
fonction elle-même, ou non, par la même personne ou non.


## Tests unitaires

Il existe plusieurs outils permettant de réaliser des tests unitaires :
les assertions, les doctests, le module tiers `pytest`...

Les tests unitaires doivent être le plus *couvrants* possible, c'est-à-dire
envisager le plus de cas (simples ou à problème) possibles. Chaque
branche du code doit idéalement être testée.

<!--Lorsqu'on démarre une session de programmation sur un projet, il est habituel de
lancer les tests avant de commencer, puis lorsqu'on a finit, pour mieux cerner
l'impact du travail effectué sur le code écrit.
-->

Dans la suite, nous écrivons les tests de la fonction `fibo(n)` qui doit
calculer le terme `n` de la suite de Fibonacci (rappel : $`F_0=0`$, $`F_1=1`$,
$`F_n = F_{n-1}+ F_{n-2}`$ si $`n > 1`$).

### Assertions

Le moyen le plus simple d'écrire de tests est l'utilisation d'assertion. C'est
aussi le moins complet.

Une assertion *passe* si l'expression booléenne qui suit
le mot clé `assert` est vraie. Une assertion permet donc de vérifier, par exemple
le retour d'une fonction. Dès qu'une assertion est fausse, l'exécution s'arrête
(exception `AssertionError`).

~~~python
assert fibo(0) == 0
assert fibo(3) == 2
assert fibo(7) == 13
~~~

<!-- On pourrait ajouter d'autres tests, ou imposer que la fonction renvoie une
valeur particulière si le paramètre est strictement négatif.-->

### Doctests

Le module `doctest` permet d'intégrer des tests dans la *docstring* des fonctions
(fiche 1). Les doctests sont repérées par la chaîne `>>>`. Écrire des
doctests permet à la fois de réaliser des tests unitaires, mais aussi de
documenter efficacement la fonction :

~~~python
# Fichier  fibo.py
def fibo(n: int) -> int:
    """
    fibo(n) : nieme terme de la suite de Fibonacci
    >>> fibo(3)
    2
    >>> fibo(7)
    13
    """
    # Texte de la fonction
~~~

<!--En supposant que la fonction est écrire dans le fichier `fibo.py`, les tests
peuvent être validés depuis l'invite de commande du système :

~~~
$ python -m doctest fibo.py
~~~
ou depuis l'invite de commande de l'interpréteur Python :
-->

Les tests peuvent être validés depuis l'interpréteur Python (ici, un
des tests a échoué) :

~~~python
>>> import fibo
>>> import doctest
>>> doctest.testmod(fibo)
Failed example:
    fibo(7)
Expected: 13
Got: 15
~~~

<!-- Si la fonction ne renvoie pas la valeur escomptée, on obtiendra par exemple ce
qui suite indiquant que pour le paramètre 7, la valeur retournée a été 15 au
lieu du 13 attendu :

**********************************************************
File "fibo.py", line 9, in fibo.fibo
Failed example:
    fibo(7)
Expected: 13
Got: 15
**********************************************************
1 items had failures:
   1 of   3 in fibo.fibo
***Test Failed*** 1 failures.
-->

### Module `pytest`

On peut démarrer l'utilisation de `pytest` par l'écriture de fonctions
préfixées par `test_` contenant chacune une assertion. L'exemple suivant montre
qu'on peut aussi tester des choses plus complexes, comme la levée d'une
exception.

<!--
Lorsqu'on désire écrire des tests plus complets, on peut se tourner vers le
module standard `unittest` ou le module tiers `pytest`. Leur objectif est
identique, mais le second est un peu plus simple à utiliser : il suffit d'écrire
un fichier incluant des fonctions dont le nom commence par `test_`.
Les rapports de `py.test` sont détaillés, et il est même
possible de vérifier que l'exécution provoque bien une levée d'exception
particulière (dans le cas où le paramètre est négatif par exemple) :
-->

~~~python
# Fichier test_fibo.py
import pytest
from fibo import fibo
def test_0():
    assert fibo(0) == 0
def test_3():
    assert fibo(3) == 2
def test_7():
    assert fibo(7) == 13
def test_neg():
    with pytest.raises(ValueError):
        fibo(-1)
~~~

L'exécution des tests réalisés avec `pytest` montre un diagnostic plus précis
des problèmes (non reproduit ici).

~~~python
>>> import pytest
>>> pytest.main(["test_fibo.py"])
~~~


Exécution des tests
~~~Python
>>> import pytest
>>> pytest.main("test_fibo.py")
test_fibo.py ..FF                                      [100%]
======================== FAILURES ===========================
_________________________ test_7 ____________________________
    def test_7():
>       assert fibo(7) == 13
E       assert 15 == 13
E        +  where 15 = fibo(7)
test_fibo.py:11: AssertionError
________________________ test_neg ________________________
    def test_neg():
>       with raises(ValueError):
E       NameError: name 'raises' is not defined
test_fibo.py:14: NameError
=========== 2 failed, 2 passed in 0.05 seconds ===========
~~~
