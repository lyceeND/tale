# Guide de style

En bref :

> Si un programme est finalement destiné à être exécuté par une machine, 
> on oublie souvent qu'il sera très souvent relu par un humain : son auteur, qui
> voudra le modifier, ou un autre développeur.
> Avoir un style d'écriture standard facilite cette relecture.

## PEP 8

PEP est l'acronyme de Python Enhancement Proposal (Propositions d'amélioration
de Python). Une Pep est un document souvent technique décrivant un aspect du
langage ou une proposition d'amélioration. Toutes les Pep (il y en a
actuellement plus de 500) sont disponible à cette [adresse](https://www.python.org/dev/peps/)

La Pep 8 propose un guide de style d'écriture du code Python, largement suivi
par la communauté.

Les règles suivantes sont extraites du guide de style [PEP 8](https://www.python.org/dev/peps/pep-0008/)

## Nommage des variables

Syntaxiquement, les noms de variables, fonctions, classes, méthodes, attributs
(termes définis fiche 10) peuvent comporter des lettres, chiffres, caractères `_` et ne
doivent pas commencer par un chiffre.

Quel que soit le langage, on choisira un nom d'autant plus évocateur qu'il est
utilisé dans une grande portion de code.

La Pep 8 ajoute :

- noms de modules courts, en minuscule, et de préférence sans `_` : `crypto`
- noms de classe ou de types en *CamelCase*, c'est à dire en minuscule, sans `_` avec
  une majuscule au début de chaque mot : `class
  NombrePremier`
- noms de fonctions, méthodes, attributs ou variables en minuscules, les mots séparés par des
  `_` : `def decomposition_facteurs_premiers(...)`
- variables utilisées comme constantes sont majuscules, les mots séparés
  par des _ : `TOTAL_MAX = 10`

## Espaces, indentations, lignes blanches

### Indentation

Les bloc Python sont délimités par l'indentation.
<!-- Syntaxiquement, ce qui importe
est que l'indentation soit constante au sein d'un bloc. Pour faciliter la
lecture, -->
La PEP 8 propose d'indenter les blocs à l'aide de 4 espaces (la plupart
des éditeurs Python utilisent ce réglage).

Une ligne ne devrait pas excéder 79 caractères (cette règle est parfois
transgressée). Lorsqu'une instruction court sur plusieurs lignes, on facilite la
lecture en indentant (exemple tiré de la Pep8 ) :

~~~python
# Aligner avec la parenthèse ouvrante
foo = nom_de_fonction_long(var_one, var_two,
                           var_three, var_four)
~~~

Enfin, on écrit généralement une seule instruction par ligne.

<!--
~~~python
if a % 2 == 0:
    print(a, "est pair")
~~~

et non :

:::{custom-style="NDA"}
Barrer le bloc de code ?
:::

~~~python
if a % 2 == 0: print(a, "est pair")
~~~
-->

### Espaces

Les règles suivantes permettent de bien distribuer les espaces :

- pas d'espace avant `:`
- espace après (mais pas avant) les `,` dans les appels ou définitions de fonction
- espaces autour de `=` et des opérateurs arithmétiques, sauf s'il y en a beaucoup sur la
  ligne (dans ce cas, ne pas mettre d'espace autour des plus prioritaires)
- pas d'espace après `([{`, ni avant `)]}`
- pas d'espaces autour de `:` dans les *slices*

~~~python
# espaces autour de =
a = 2
# espace après , et pas d'espace avant : #
for i in range(1, 43):
    a = a * 2 # espaces autour de = et *
lst = [1, 1, 2, 3, 5, 8] # espaces après ,
print(lst[2:6]) # pas d'espace autour de :
# espaces après :, mais pas avant
dico = {"6": 8, "7": 13, "8": 21}
~~~

### Lignes blanches

On laisse deux lignes vides entre les différentes fonctions
ou classe (voir fiche 10) à l'intérieur d'un module. Au sein d'une classe, les
méthodes sont séparées par une seule ligne blanche.

## Outils de validation

Il existe de nombreux outils permettant de vérifier la conformité du code
produit avec la Pep 8. Certains de ces outils sont directement intégrés dans les
environnements de développement. La Pep 8 laissant parfois un peu de liberté,
ces outils contiennent de nombreuses options pour choisi ces différents réglages.


Des outils sont souvent intégrés dans les environnement de développement, mais
sont aussi disponibles séparément :


- l'outil `pep8` vérifie la conformité du code avec la Pep 8
- `flake8` vérifie la conformité avec la Pep 8 ainsi que d'autres points :
  imports non utilisés, fonctions trop longues...
- `autopep8` reformate le code de manière à le rendre conforme à la Pep 8
- `black` reformate le code pour le rende conforme, mais ne propose
  aucune option : un code formaté avec `black` sera tout le temps formaté de la
  même manière

