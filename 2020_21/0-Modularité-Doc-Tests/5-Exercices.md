# Exercices


## 1) Modularité

1) Parmi les lignes qui suivent, trouver celle qui ne permet pas d'importer et
   d'utiliser la totalité du module `itertools` :

a. `import itertools`

b. `import itertools as itt`

c. `from itertools import cycle`

2) On souhaite écrire une portion de code qui permettra de savoir si une année
   est bissextile ou non.  Qu'est-ce qui est le plus approprié ?

a. Écrire un programme principal qui demande à l'utilisateur de taper une année,
et indique si elle est bissextile.

b. Écrire une fonction qui indique si une année passée en paramètre est
bissextile ou non en renvoyant un booléen.

c. Écrire un module `bissextile.py` qui contiendra tout ce qu'il faut pour
tester le caractère bissextile d'une année.

## 2) Débugage

1) Quel message d'exception s'affiche si on tente d'exécuter ce code ?


~~~python
a = 3 * k + 1
for i in range(3):
    print("i = {}, a = {}".format(i, a)
    a = 2 * a
~~~

a. NameError

b. SyntaxError

c. IndexError

d. IndentationError

2) L'exception qui précède est-elle levée avant ou pendant l'exécution ?
  
a. avant l'exécution

b. pendant l'exécution

3) Quel message d'exception s'affiche si on tente d'exécuter ce code :


~~~python
f = [0, 1]
for i in rang(2, 100):
    f.append(f[-1] + f[-2])
print(f)
~~~

a. NameError

b. SyntaxError

c. IndexError

d. IndentationError

4) Cette exception est-elle levée avant ou pendant l'exécution ?
  
a. avant l'exécution

b. pendant l'exécution


5) Quel message d'exception s'affiche si on tente d'exécuter ce code ?
  
~~~python
v = 1
while v < 100:
    if v % 7 == 0:
        print(v, "est un multiple de 7")
      else:
        print(v, "n'est pas un multiple de 7")
~~~

a. NameError

b. SyntaxError

c. IndexError

d. IndentationError

6) Cette exception est-elle levée avant ou pendant l'exécution ?
  
a. avant l'exécution
b. pendant l'exécution

## 3) Tests

1) Pour tester une fonction ayant un seul paramètre, choisir une
  valeur du paramètre et vérifier que le résultat est correct pour cette valeur 
  est toujours suffisant
  
a. Vrai

b. Faux

2) On considère le code suivant qui, étant donné un terme $u_n$ de la suite de
   Syracuse, renvoie le terme suivant $u_{n+1}$.

~~~python
def syracuse(un: int) -> int:
    if un % 2 == 0:
        return un // 2
    else:
        return 3 * un + 1
~~~

Le test suivant est proposé :

~~~python
assert syracuse(32) == 16
~~~

Parmi ces autres tests, lequel vous semble le plus urgent à ajouter ?

a. assert syracuse(16) == 8

b. assert syracuse(0) == 1

c. assert syracuse(3) == 10

## 4) Guide de style

1) Quel nom est plus approprié pour une fonction qui calcule la suite de
   fibonacci :

a. f

b. fibonacci

c. Fibonacci

d. SuiteFibonacci







## 5) Racines d'un polynôme

On souhaite écrire des tests couvrants pour la fonction
`racines` du module `polynomes.py`,
qui renvoie la liste des racines réelles d'un polynôme du second degré
dans l'ordre croissant ou lève une exception si le polynôme n'est
pas de degré 2. Attention aux tests d'égalité sur les nombres flottants...

~~~python
import math
def racines(a: float, b: float, c: float) -> list:
    """
    Renvoie la liste des racines du polynôme
    ax^2 + bx + c
    """
    if a == 0 :
        raise ValueError("Le polynôme n'est pas de degré 2")
    delta = b ** 2 - 4 * a  * c
    if delta < 0:
        return []
    if delta == 0:
        return [-b / (2 * a)]
    x1 = (-b - math.sqrt(delta)) / (2 * a)
    x2 = (-b + math.sqrt(delta)) / (2 * a)
    if a > 0:
        return [x1, x2]
    else:
        return [x2, x1]
~~~

1) Écrire des tests à l'aide d'assertions 
2) Écrire des tests à l'aide du module `pytest` (on pourra lire la documentation sur [pytest.approx](https://docs.pytest.org/en/latest/reference/reference.html?highlight=approx#pytest.approx))


## 6) Valeurs uniques 

On dispose d'une fonction qui indique en renvoyant un booléen si les éléménts
d'une liste sont tous différents (dans ce cas la fonction renvoie `True`.
La première ligne de la fonction (écrite dans le module `outils_listes.py` est :

~~~python
def tous_differents(lst: list) -> bool:
~~~

Écrire des tests couvrants à l'aide d'assertions.
<!-- 2) Écrire des tests couvrants à l'aide du module `pytest` -->

## 7) Arithmancie 

On souhaite associer une valeur numérique à une chaîne de caractères. La valeur
associée à la chaîne est la somme des valeurs associées à chacun des caractères
qui la composent. Aux lettres non accentuées de A à Z, qu'elles soient en
minuscules ou en majuscules, on associe leur numéro d'ordre dans l'alphabet, de
1 à 26. À tous les autres symboles (lettres accentuées, chiffres, espaces,
ponctuations), on associe la valeur 0.

On rappelle que `ord(x)` est le code numérique associé à la lettre passée en
paramètre.

La chaîne `string.ascii_uppercase` est définie dans le module `string` et vaut
`"ABCDEFGHIJKLMNOPQRSTUVWXYZ"`.

Pour chaque proposition suivante, proposez un test qui montre que la fonction
est erronée.

1).

~~~python
def valeur1(chaine):
    s = 0
    for c in chaine:
        if c >= "A" and c <= "Z":
            s = s + ord(c) - ord('A') + 1
    return s
~~~

2).

~~~python
def valeur2(chaine):
    s = 0
    for c in chaine.upper():
        if c >= "A" and c < "Z":
            s = s + ord(c) - ord('A') + 1
    return s
~~~

3).

~~~python
import string
def valeur3(chaine):
    s = 0
    for c in chaine.upper():
        if c in string.ascii_uppercase:
            s = s + string.ascii_uppercase.index(c)
    return s
~~~

4).
Proposer un ensemble complet de tests pour la fonction `valeur` (non encore écrite), pour vérifier
qu'elle est correcte.

5). Proposer le code de cette fonction (il est possible de s'inspirer d'une des versions
précédentes, en la corrigeant). Ne pas oublier de tester que la fonction écrite
passe bien les tests de la question précédente.

<!--
>>> 0.1 + 0.2 == 0.3
False

__ https://docs.python.org/3/tutorial/floatingpoint.html

This problem is commonly encountered when writing tests, e.g. when making
sure that floating-point values are what you expect them to be.  One way to
deal with this problem is to assert that two floating-point numbers are
equal to within some appropriate tolerance::

>>> abs((0.1 + 0.2) - 0.3) < 1e-6
True
>>> from pytest import approx
>>> 0.1 + 0.2 == approx(0.3)
True
>>> 1.0001 == approx(1)
False
>>> 1.0001 == approx(1, rel=1e-3)
True
-->

## 8) Enquête sur une erreur : calcul de $`\pi`$

La somme les inverses des carrés des nombres entiers converge vers $`\frac{\pi^2}{6}`$ :

$`\displaystyle\sum_{k=1}^{k=\infty} \frac{1}{k^2} = \frac{\pi^2}{6}`$

On utilise cette formule pour trouver une approximation de $`\pi`$ :

~~~python
import math
def terme(k: int) -> float:
    return 1 / (k ** 2)

def approxpi(n: int) -> float:
    s = 0
    for k in range(n):
        s = s + terme(k)
    return math.sqrt(s * 6)
~~~

1) Lors de l'exécution de `approxpi(1000)` une exception est levée à l'exécution de la ligne 3.
Quel est le type d'exception ? `ValueError`, `SyntaxError`, `TypeError`,
`ZeroDivisionError`, `IndexError` ou `NameError` ?

2) Bien que l'exécution soit interrompue ligne 3, la source de l'erreur est
ailleurs. Expliquer d'où provient l'erreur et proposer un correctif.

## 8) Enquête sur une erreur : Produit Scalaire

Le produit scalaire de deux vecteurs $`u`$, $`v`$ de $`\mathbb{R}^3`$, de coordonnées
$`(u_1, u_2, u_3)`$ et $`(v_1, v_2, v_3)`$ est le réel : $`u_1v_1 + u_2v_2 + u_3v_3`$.

Le code suivant permet de calculer des produits scalaires. Il est testé sur des
vecteurs de $`\mathbb{R}^3`$ choisis au hasard :

~~~python
import random

def scalaire(v1: tuple, v2: tuple) -> float:
    """ Calcule le produite scalaire des deux
        vecteurs v1 et v2
    """
    s = 0
    for k in range(len(v1)):
        s = s + v1[k] * v2[k]
    return s

def random_vect(n: int) -> tuple:
    """ Choisit un vecteur de R^n contenant des entiers
        non nuls entre -10 et 10
    """
    v = []
    for i in range(n):
        val = random.randint(-10, 10)
        if val != 0:
            v.append(val)
    return tuple(v)

def main():
    for i in range(20):
        print(scalaire(random_vect(3), random_vect(3))

~~~

Prises séparément, les deux fonctions semblent opérationnelles :

~~~python
>>> scalaire((1, 2, 1), (-1, 3, -2))
3
>>> random_vect(3)
(-6, 2, 8)
~~~

Pourtant, lorsqu'on exécute la fonction `main()`, le programme n'affiche que
quelques produits scalaires (ici 12, 118 et -104) et *plante* :

~~~python
>>> main()
12
118
-104
Traceback (most recent call last):
  File "scal.py", line 25, in <module>
    main()
  File "scal.py", line 22, in main
    print(scalaire(random_vect(3), random_vect(3)))
  File "scal.py", line 6, in scalaire
    s = s + v1[k] * v2[k]
IndexError: tuple index out of range
~~~

*Le traceback obtenu avec le shell IPython peut être légèrement différent, et
afficher les lignes dans leur contexte. Les informations présentées sont
cependant globalement les mêmes.*

1) En analysant le traceback donné plus haut, indiquer quel est le type
d'exception qui a été levée, et sur quelle ligne de code ?

2) Sur la ligne en question, qu'est-ce qui pourrait avoir provoqué l'erreur ?

3) Proposer de rajouter un affichage `print(...)` à un endroit du programme pour
essayer de mettre en évidence la nature du problème, puis réexécuter le code.

4) Expliquer l'erreur. Pourquoi ne se produit-elle pas toujours au même moment.

5) Proposer un correctif.




## Objectif BAC : Module de chiffrement (45 minutes)

Le petit module suivant (fichier `macrypto.py`) propose quelques outils de chiffrement :

- [la méthode de César](https://fr.wikipedia.org/wiki/Chiffrement_par_décalage)
- [la méthode de Vigénère](https://fr.wikipedia.org/wiki/Chiffre_de_Vigenère)

<!--
>:::{custom-style="NDA"}
Numéroter les lignes (y compris les lignes vides)
:::
-->

~~~python
import string
import itertools

def decale(lettre,dec):
    alph=string.ascii_uppercase
    new_index =(alph.index(lettre)+ dec)%26
    return alph[new_index]
def cesar(message, dec) :
    res = []
    for l in message: res.append(decale(l, dec))
    return "".join(res)
def Vigenere(message,passwd):
    res = []
    for lettre,decl in zip(message,
        itertools.cycle(passwd)):
        dec = string.ascii_uppercase.index(decl)
        res.append(decale(lettre , dec))
    return "".join(res)
~~~

Le développeur a malheureusement oublié les docstrings, les tests et n'a pas
suivi les recommandations d'écriture de la PEP 8.

a) Corrigez le code afin qu'il suive les recommandations de la PEP 8.

b) En utilisant la fonction `help`, faire afficher la docstring du module
`string`. Que vaut `string.ascii_uppercase` ?

c) En utilisant la fonction `help`, faire afficher la docstring de la fonction
`cycle` du module itertools, et de la fonction `zip` si vous ne la connaissez
pas. Expliquer ce que fait la ligne suivante (fonction `vigenere`):
`for lettre, decl in zip(message, itertools.cycle(passwd)):`.

d) Écrire la dosctring du module de chiffrement et de chacune de ses trois
fonctions. Inclure un test représentatif, au format `doctest`,
pour chacune des trois fonctions.

e) Proposer des tests couvrants à l'aide d'assertions, pour les trois fonctions.

f) Le chiffrement Atbash consiste à remplacer A par Z, B par Y, C par X... (on
inverse l'alphabet). Proposer une fonction qui permet de chiffrer avec cette
méthode, en suivant la PEP 8, en intégrant une docstring (contenant un test),
des annotations de fonction, ainsi qu'une batterie de quelques assertions pour
vérifier votre code.


## Computer Science : création d'un projet git avec fichiers de tests

En               vous               inspirant               de               [ce
document](https://jeremykun.com/2020/09/11/searching-for-rh-counterexamples-setting-up-pytest/)
vous créerez  un projet plus simple  (regardez quand même ce  qu'est l'hypothèse
de  Riemann et  ce qu'elle  pourrait vous  rapporter). Il  s'agira de  créer une
fonction qui fabrique une liste de nombres premiers (voir crible d'Ératosthène) puis une fonction qui renvoie
les diviseurs premiers d'un nombre donné. Quel jeu de tests envisager?



