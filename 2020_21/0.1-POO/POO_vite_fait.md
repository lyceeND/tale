# Programmation Orientée Objet

## Architecture Objet

La logique interne de la conception objet possède un aspect intuitif très fort dans la mesure où l'on s'efforce de calquer la représentation informatique sur des entités physiques ou conceptuelles apparaissant dans le processus à modéliser. Ce processus s'appelle la **réification** et consiste souvent simplement à reproduire des objets du monde réel comme par exemple concevoir un objet *Produit* et un autre *Catalogue* sur un site de vente en ligne.
Les termes d'**objet** et d'**architecture objet** évoquent une volonté de se rapprocher du monde physique de la *construction* et de l'*assemblage*. Ce rapprochement est même plus marqué encore à travers l'usage des **design patterns** qu'utilisent couramment les programmeurs objet expérimentés. L'**héritage** et la **composition par affinage** y jouent un rôle important.

### Design Patterns
Le terme tire son origine des théories de Christopher Alexander, un architecte américain d'origine autrichienne, qui les développe notamment dans son ouvrage *A Pattern Language*. Ses conceptions de l'architecture et du design urbain ont trouvé des résonances et des applications dans divers domaines comme la structure des organisations, l'étude des motifs des tapis d'Orient ou encore l'informatique où tout le domaine des **design patterns** (patrons de conception) s'en inspire très largement. Ce domaine dépasse le cadre du programme.
:::

## Principes et Terminologie Objet

### Objets et encapsulation

Un objet comprend une partie figée qui représente son *état* et les *liens* qui l'unissent à d'autres objets et une partie dynamique qui décrit son comportement, c'est à dire toutes les opérations qu'on peut lui appliquer ainsi que sa manière de *réagir* aux événements de l'environnement.
Sa partie fixe est constituée d'un ensemble de **champs** ou **attributs** et sa partie dynamique d'opérations appelées **méthodes**.
Certaines de ces méthodes constituent la partie visible de l'objet.
C'est par elles que l'on s'adresse à lui. Elles constituent autant de *Services* qu'on peut demander à l'objet de fournir. D'autres champs peuvent quand à eux être partiellement ou totalement inaccessibles à d'autres objets. C'est alors que l'on parle d'**encapsulation**.

### exemple en Python

On se propose de modéliser des `Boites` qui peuvent contenir des objets et être ouvertes ou fermées.
On y place donc deux champs : `contenu` et `ouvert` qui sont en outre signalés **privés** par la présence du "_" devant leur nom.
Des méthodes pour ouvrir ou fermer la boite et tester si elle est ouverte y sont adjointes. Le paramètre **self** représente l'instance courante de l'objet, on le retrouve dans toutes les méthodes de la classe.
On trouve des méthodes spéciales comme `__init__` pour initialiser l'objet ou `__str__` pour obtenir sa représentation sous forme de chaîne de caractères.
On trouve aussi des méthodes statiques, qui ne sont pas attachée à une instance particulière.

```python
class Boite:
    def __init__(self, ouvert=False):
        self._ouvert = ouvert

    def ouvre(self):
        self._ouvert = True

    def ferme(self):
        self._ouvert = False

    def __str__(self):
        res = "Une boîte "
        res += "ouverte " if self._ouvert else "fermée" 
        return res
    
    @staticmethod
    def nouvelle_boite_ouverte():
        return Boite(ouvert=True)
```

## Classes, instances et messages

### Classes et instances

Quand des objets possèdent une structure et des comportements en commun, on peut les regrouper sous forme de classe. Une classe est une sorte de moule à partir duquel sont produits les objets que l'on appelle **instances** de la classe. Les instances d'une même classe ne diffèrent les unes des autres que par les valeurs de leurs attributs.
La programmation objet consiste alors à définir les bonnes classes dotées de leurs champs et méthodes, puis à les instancier pour créer des objets et les faire interagir.

### Messages

On dit qu'il y a *interaction* entre objets lorsqu'il y a envoi d'un *message* de l'un vers l'autre. Ce message est constitué du nom d'une méthode applicable à l'objet destinataire avec éventuellement des *arguments*. Le destinataire et l'émetteur peuvent être le même objet. Dans ce cas le mot clé *this* (en Java ou C++) ou *self* (en Python ou Smalltalk) est utilisé comme nom de destinataire.
Ne confondez pas la notion de message avec celle de fonction. La réalisation d'une action dépend ici de celui qui reçoit le message.
Ainsi dans notre exemple précédent, nous pouvons instancier une boite, l'ouvrir, l'afficher, la fermer. Notez l'appel à une méthode statique par le nom de la classe.

```python
boite = Boite()
boite.ouvre()
print(boite)
boite.ferme()
print(boite)
print(Boite.nouvelle_boite_ouverte())
```
