class Cellule:

    def __init__(self) -> None:
        self._actuel = False
        self._futur = False
        self._voisins = []

    def naitre(self) -> None:
        self._futur = True

    def mourir(self) -> None:
        self._futur = False
    
    def est_vivant(self) -> bool:
        return self._actuel

    def set_voisins(self, v: list) -> None:
	    self._voisins = v
    
    def get_voisins(self) -> list:
	    return self._voisins
    
    def basculer(self) -> None:
        self._actuel = self._futur

    def __str__(self) -> str:
        res = ""
        if self.est_vivant():
            res = "X"
        else:
            res = "-"
        return res
    
    def calcule_etat_futur(self) -> None:
        nb_vivants = sum([c.est_vivant() for c in self._voisins])
        if nb_vivants != 2 and nb_vivants != 3:
            self.mourir()
        elif nb_vivants == 3:
            self.naitre()
        else:
            self._futur = self._actuel


