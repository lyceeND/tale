# Exercices



## Principes de la POO

1. À travers lequel de ces langages le paradigme objet a-t-il été historiquement
   introduit pour la première fois ?

```
☐ a. SmallTalk ☐ b. Simula 
☐ c. Java ☐ d. C++ 
```

<!--
b
--->

2. Lequel de ces mots-clés marque le début de la définition d'une classe:

```
☐ a. class ☐ b. def
☐ c. from ☐ d. __init__
```

<!--
a
--->

3. On suppose que l'on dispose de la définition d'une classe `Entier`. Laquelle de ces affirmations décrit le mieux la déclaration `x = Entier()`:


```
☐ a. x contient une valeur de type int 
☐ b. x contient un objet de type Entier
☐ c. x contient une référence à un objet Entier
☐ d. On peut affecter une valeur de type int à x
```

<!--
c
--->

4. Quelle va être la sortie en réponse à l'exécution du code suivant :

```python
class Alien:
    def __init__(self, id = 'Zorglub'):
        self.id = id

bul = Alien('Bulgroz')
print (bul.id)
```


```
☐ a. Une erreur de syntaxe va empécher le programme de tourner
☐ b. Zorglub
☐ c. Bulgroz
☐ d. bul
```

<!--
c
--->



5. Quelle sera l'issue de l'exécution de ce code :

```python
class test:
    def __init__(self, a):
        self.a = a

    def montre(self):
        print(self.a)

obj = test()
obj.display()
```



```
☐ a. Une erreur car la création de l'objet nécessite un argument
☐ b. Pas d'erreur mais n'affiche rien
☐ c. Affiche None 
☐ d. Une erreur car test est en minuscule
```

<!--
a
--->


# PROJET : Jeu de la Vie

Le but de cet exercice est de réaliser en Python une implémentation du **jeu de la vie** en utilisant la programmation objet.

Le jeu de la vie a été inventé par le mathématicien américain John
Conway. C'est un exemple de ce qu'on appelle un **automate
cellulaire**.
Il se déroule sur un tableau rectangulaire ($`L\times H`$) de cellules.
Une cellule est représentée par ses coordonnées matricielles i et j.
$`0\leq i<H, 0\leq j<L`$

Une cellule peut être dans deux états : **Vivant** ou **Mort**.
La dynamique du jeu s'exprime par les règles de transition suivantes :

- Une cellule vivante reste vivante si elle est entourée de 2 ou
  3 voisines vivantes et meurt sinon.
- Une cellule morte devient vivante si elle possède exactement 3
  voisines vivantes.

La notion de *voisinage* dans le jeu de la vie est celle des 8 cases qui
peuvent entourer une case donnée (on parle de voisinage de Moore).
Pour implémenter la simulation, on va tout d'abord donner une 
modélisation objet du problème, puis procéder à son implémentation.

1. Modélisation Objet

   a. Quelles classes pouvez-vous dégager de ce problème au premier abord ?

   b. Quelles sont quelques unes des méthodes qu'on pourrait leur donner ?

   c. Dans quelle classe pouvons nous représenter simplement la notion de voisinage d'une cellule ? Et le calculer ?

   d. Une cellule est au bord si i=0, i=H-1, j=0 ou y=L-1. Combien de voisins possède une cellule qui n'est pas au bord ?

   e. Combien de voisins possède une cellule qui est au bord ?

   f. Que pourrions-nous aussi considérer comme voisin de droite de la case en haut à droite de la grille ? Et comme voisin du haut ? 

2. Implémentation  des cellules

   a. Implémenter tout d'abord une classe Cellule avec comme attributs :

     - un booléen `actuel` initialisé à `False`
     - un booléen `futur` initialisé à `False`
     - une liste `voisins` initialisée à  `None`

   Ces attributs seront considérés comme "privés" ici.
   La valeur `False` signifie que la cellule est morte et `True` qu'elle est vivante.

   Ajouter les méthodes :

     - `est_vivant()` qui renvoie l'état actuel (vrai ou faux)
     - `set_voisins()` qui permet d'affecter comme voisins la liste passée en paramètre
     - `get_voisins()`
     - `naitre()` qui met l'état futur de la cellule à `True`
     - `mourir()` qui permet l'opération inverse
     - `basculer()` qui fait passer l'état futur de la cellule dans l'état actuel

  b. Ajouter à la classe Cellule une méthode `__str__()` qui affiche une croix (un `X`) si la cellule est vivante et un tiret (`-`) sinon.
   Expliquer brièvement l'utilité d'une telle méthode `__str__()` en Python.

   c. Ajouter une méthode `calcule_etat_futur()` dans la classe `Grille` qui permet d'implémenter les règles d'évolution du jeu de la vie en préparant l'état futur à sa nouvelle valeur.

 3. Implémenter la classe Grille.

   a. Créer la classe `Grille` et y placer les attributs suivants considérés comme "publics" :
     
     -`largeur`
     -`hauteur`
     -`matrix` : un tableau de cellules à 2 dimensions (implémenté en Python par une liste de listes)

   Fournir une méthode `__init__` permettant l'initialisation d'une Grille de Cellules avec une largeur et hauteur (une nouvelle Cellule sera créée par l'appel `Cellule()` )

  b. Ajouter les méthodes :
  
     -`dans_grille()` qui indique si un point de coordonnées i et j est bien dans la grille
     -setIJ() qui permet d'affecter une nouvelle valeur à la case (i,j) de la grille
     -getIJ() qui permet de récupérer la cellule située dans la case (i,j) de la grille
     -une méthode statique `est_voisin()` qui vérifie si les cases (i,j) et (ii,jj) sont voisines dans la grille.

  c. ajouter une méthode `get_voisins()` qui renvoie la liste des voisins d'une cellule.

  d. fournir une méthode `affecte_voisins()` qui affecte à chaque cellule de la grille la liste de ses voisins.

  e. donner une méthode `__str__()` qui permet d'afficher la grille sur un terminal.

  f. on voudrait remplir aléatoirement la Grille avec un certain taux de Cellule vivantes. Fournir à cet effet, une méthode `remplir_alea()` avec le taux (en pourcentage) en paramètre.

  g. On joue à présent ! Concevoir une méthode `jeu()` permettant de passer en revue toutes les Cellules de la Grille, de calculer leur état futur, puis une méthode `actualise()` qui bascule toutes les cellules de la Grille dans leur état futur.

4. Programme principal

 Définir enfin un `main` pour terminer l'implémentation du Jeu de la Vie avec un   affichage en console en utilisant les méthodes précédentes.
 On donne la méthode suivante qui permet d'effacer l'écran dans un terminal `ANSI` :

 ```python
 def effacer_ecran():
    print("\u001B[H\u001B[J")
 ```

## Feuille de route

1) Modélisation objet
 
 a. On peut proposer assez facilement 2 classes : Une classe Cellule permettant de  stocker l'etat d'une cellule et son évolution avec une classe Grille représentant le  plateau de jeu 

 b. Dans la classe Cellule, il faut pouvoir faire naitre ou mourir la cellule,  connaitre son etat actuel et futur et afficher la cellule sous la forme d'un  caractère. Dans la Grille, il faut pouvoir connaitre sa largeur et sa hauteur,  récupérer le contenu (la cellule)  qui se trouve dans une position (i,j) dans la  grille et l'affecter avec une nouvelle Cellule.

 c. La notion de voisinage se calcule bien dans la grille mais une cellule doit aussi connaitre ses voisins (ou leur nombre) pour calculer son état futur.

 d. Une cellule qui n'est pas au bord admet toutes les cases qui l'entourent comme voisins. Il suffit de les compter

 e. Distinguez les cellules dans les angles des autres cas.
 
 f. Imaginer une Grille qui se replie sur elle même (bord gauche - bord droit) et  (bord supérieur et inférieur)

2) Implémentation des cellules

 a. Les attributs sont donnés et les méthodes sont pour la plupart traitables en 1  seule ligne. Ne pas oublier de mettre `self` en premier argument des méthodes !

 b. Simple test et on renvoie le caractère demandé selon l'état actuel de la cellule.

 c. On implémente les règles d'évolution du jeu de la vie dans cette méthode en  mettant seulement à jour l'attribut `futur`. Si possible ne pas utiliser d'accès aux  attributs privés de la classe.

3) Implémentation de la grille

 a. et b. La méthode `__init__()`  permet de mettre en place les attributs largeur, hauteur et matrix et de les initialiser.
 Utiliser une compréhension pour initialiser aisément matrix qui est un tableau de   Cellules à deux dimensions.
 Pour `est_voisin()` qui prend en entrée 2 cases de la grille on souhaite exprimer que ces 2 cases diffèrent d'au plus une unité en abscisse ou en ordonnée. Penser au   décorateur `@staticmethod`.

 c.  On passe en revue toutes les cases contiguës (8 voisins potentiels) de la casse  (x,y) de la Grille et si elles sont bien dans la grille, on les accumule dans une  liste. On renvoie la liste obtenue.

 d.
 parcourir les cellules de la grille et affecter leurs voisins avec le résultat  fourni par la méthode `get_voisins` de Grille.

 e. Afficher ligne par ligne les Cellules de la Grille.

 f. Utiliser la méthode `random()` du module éponyme qui renvoie un nombre aléatoire  entre 0 et 1 puis faire naître et basculer les cellules tirées au sort.

 g. On passe en revue toutes les cellules de la Grille et on fait ce qui est demandé  dans les 2 cas.

4) Programme principal 

 Penser à faire dans l'ordre l'instanciation, le remplissage aléatoire, le calcul des  voisinages puis jouer en marquant une pause grâce à à la méthode `sleep()` du  module `time`.
