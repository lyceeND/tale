from random import random
import time, sys
from cellule import Cellule

class Grille:
    def __init__(self, largeur: int, hauteur: int) -> None:
        self._largeur = largeur
        self._hauteur = hauteur
        self.matrix = [
            [Cellule() for j in range(self._largeur)]
             for i in range(self._hauteur)
             ] 
    
    def dans_grille(self, i: int, j: int) -> bool:
	    return 0 <= i < self._hauteur and 0 <= j < self._largeur
    
    def get_IJ(self, i: int, j: int) -> Cellule:
        if self.dans_grille(i, j):
            return self.matrix[i][j]
        else:
            return None

    def get_largeur(self) -> int:
        return self._largeur

    def get_hauteur(self) -> int:
        return self._hauteur

    @staticmethod
    def est_voisin(i: int, j: int, ii: int, jj: int) -> bool:	
	    return max(abs(ii - i), abs(jj - j)) == 1

    def get_voisins(self, i: int, j: int) -> list:
        v = []
        for _i in range(i - 1, i + 2):
            for _j in range(j - 1, j + 2):
                if self.dans_grille(_i, _j) and Grille.est_voisin(i, j, _i, _j):
                    v.append(self.get_IJ(_i, _j))
        return v

    def affecte_voisins(self) -> None:
        for i in range(self._hauteur):
            for j in range(self._largeur):
                self.get_IJ(i, j).set_voisins(self.get_voisins(i, j))

    def __str__(self) -> str:
        res = ""
        for i in range(self._hauteur):
            for j in range(self._largeur):
                res += str(self.get_IJ(i, j))
            res += "\n"
        return res
	
    def remplir_alea(self, taux: float) -> None:
        for i in range(self._hauteur):
            for j in range(self._largeur):
                if random() <= (taux/100.0):
                    self.get_IJ(i, j).naitre()
                    self.get_IJ(i, j).basculer()
	
    def jeu(self) -> None:
        for i in range(self._hauteur):
            for j in range(self._largeur):
                c = self.get_IJ(i, j)
                c.calcule_etat_futur()

    def actualise(self) -> None:
        for i in range(self._hauteur):
            for j in range(self._largeur):
                self.get_IJ(i, j).basculer()
        

if __name__ == '__main__':
    vie = Grille(400, 80)
    vie.remplir_alea(45)
    vie.affecte_voisins()
    while True:
        # effacer terminal ANSI
        print("\u001B[H\u001B[J")
        print(vie)
        print("\n")
        time.sleep(0.15) 
        vie.jeu()
        vie.actualise()






