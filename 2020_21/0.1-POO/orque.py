class Orque:

    def __init__(self, nom:str, poids:int, maitre:str = "Sauron") -> None :
        assert type(poids) == int, "le poids est un entier"
        self.__nom    = nom
        self.__poids  = poids
        self.__maitre = maitre

    def salut(self)-> None:
        print(f"Ash nazg durbatulûk! Mon nom est {self.nom}")

    def nom(self) -> str:
        return self.__nom

    def poids(self) -> int:
        return self.__poids

    def maitre(self) -> str:
        return self.__maitre

    def nouveau_poids(self, new_poids: int) -> None:
        assert type(poids) == int, "le poids est un entier"
        self.__poids = new_poids

    def grossit(self, gain: int) -> None:
        assert type(gain) == int, "le poids est un entier"
        self.__poids += gain
