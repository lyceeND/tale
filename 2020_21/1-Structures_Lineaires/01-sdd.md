# Types abstraits

En bref :

> Les types abstraits sont définis par leurs caractéristiques (comment on s'en sert).
> On les retrouve dans les langages sous forme de structures de données. Ils
> constituent une base théorique solide qui permet d'étudier les
> algorithmes indépendamment du langage utilisé, et de passer plus efficacement
> d'un langage à un autre.

## Structures de données, types abstraits

Une structure de données est une manière de stocker, d'accéder à, et de manipuler
des données (comme les types `list` ou `dict` de Python)
<!--. Les dictionnaires Python sont une structure de données, qui
permettent de stocker des couples clés/valeurs et d'accéder à la valeur associée
à une clé en temps constant.-->

L'interface de la structure de données décrit de quelle manière on peut la
manipuler : utilisation de `append` pour le type `list` ou de `get` pour le type
`dict`.
<!--Dans le cas d'un dictionnaire, il s'agit par exemple des méthodes
d'accès à une valeur : `dict[key]`, ou `dict.get(key)`. -->

L'implémentation de la structure de données, au contraire, contient le code de ces méthodes (comment
fait Python). Il n'est pas nécessaire
de connaître l'implémentation pour manipuler la structure de données.

Un type abstrait décrit essentiellement une interface, indépendamment du langage
de programmation, avec éventuellement des précisions sur la complexité en
temps et en espace de ces opérations.

 Ces types abstraits peuvent être
considérés comme des cahiers des charges, dont la réalisation constitue des
structures de données effectives.


## Listes

Une liste  est composée  de données,  et offre un  moyen de  passer à  la donnée
suivante.


Les opérations généralement disponibles sont :

- savoir si la liste est vide (`is_empty`)
- insérer un élément en tête de liste (`insert`) en temps constant
- récupérer l'élément en tête de liste (`head`)  en temps constant
- récupérer la liste privée du premier élément (`tail`)

Il existe de multiples variantes de listes, mais le principe reste toujours plus
ou moins identique. Cette structure est à la base de nombreuses autres.



## Piles

Une pile (*stack* ou *lifo*, pour Last-In, First-Out) est une collection
d'objets munie des  opérations suivantes :

- savoir si la liste est vide (`is_empty`)
- empiler un nouvel élément au sommet de la pile (`empiler`, `push`) en temps constant
- dépiler l'élément au sommet de la pile et le renvoyer (`depiler`, `pop`) en
  temps constant (parfois, il y a deux opérations : une pour renvoyer le sommet
  de la pile sans le supprimer, et une pour simplement le supprimer)
  
  

## Files

Une file (*queue* ou *fifo*, pour First-In, First-Out) est une
collection d'objets munie des opérations :

- savoir si la file est vide (`is_empty`)
- ajouter un élément dans la file (`enfiler` ou `enqueue`) en temps constant
- retirer et renvoyer l'élément le plus ancien de la file (`defiler` ou `dequeue`)

## Tableaux associatifs

Un tableau associatif est un type abstrait qui associe à des valeurs à des clés
et muni des opérations suivantes :

- ajout d'une nouvelle valeur associée à une nouvelle clé
- modification de la valeur associée à une clé existante
- suppression d'une clé et de la valeur associée
- récupération de la valeur associée à une clé donnée
  
Un tableau associatif pourrait par exemple associer les valeurs «couples
prénom/nom» aux clés «numéro de sécurité sociale», la contrainte étant que chaque
clé est unique dans le tableau associatif.
