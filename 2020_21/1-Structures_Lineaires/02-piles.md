# Piles


## Un peu d'histoire

En   1920,  le   mathématicien-logicien-philosophe   polonais  Jan   ŁUKASIEWICZ
(1878-1956) invente la notation préfixée alors qu'il est ministre de l'éducation
(on a le droit de rêver...). 

![luka](./IMG/luka.jpg)

35 ans plus tard, le philosophe et informaticien (!) australien (!!!) Charles
HAMBLIN (1922 - 1985) s'en inspire.

Il a en effet en sa possession  l'un des deux ordinateurs présents à l'époque en
Australie. Il  se rend compte  que la saisie de  calculs avec les  opérateurs en
notation  infixée  induit   de  nombreuses  erreurs  de  saisie   par  oubli  de
parenthèses.  Il pense  aussi  à la  (très petite)  mémoire  des ordinateurs  de
l'époque.



Il trouve alors  son inspiration dans le travail de ŁUKASIEWICZ pour
éviter les parenthèses et il
 pense à mettre les opérateurs en position préfixée:
ainsi  il   introduit  la   notion  de   **pile**  (ou   *stack*  ou
*LIFO*) qui économise le nombre d'adresses mémoire nécessaires.


C'est  la  naissance de  la  Notation  Polonaise  Inversée  (NPI ou  RPN).  Elle
économise également la taille des composants électroniques des portes logiques.

Son seul inconvénient:  les mauvaises habitudes prises  d'utiliser des notations
infixées...

Un autre grand avantage: il faut comprendre le calcul avant de l'exécuter :-)


## NPI

```mermaid
graph TD;
DIV -->9;
DIV-->MUL;
MUL-->ADD;
MUL-->SUB;
ADD-->2;
ADD-->NEG;
NEG-->3;
SUB-->6;
SUB-->4;

```


```mermaid
graph TD;
DIV -->9;
DIV-->MUL;
MUL-->NEG;
MUL-->2;
NEG-->1;
```


```mermaid
graph TD;
DIV -->9;
DIV-->NEG;
NEG-->2;
```




```mermaid
graph TD;
NEG-->4.5;
```



```mermaid
graph TD;
-4.5;
```




* Pile
* Stack
* LIFO
* DEPS
* empiler/dépiler
* push/pop


![trainKnuth](./IMG/trainKnuth.png)
![shuntDijkstra](./IMG/shuntDijkstra.png)



## Classe Pile


Barrière d'abstraction : différence entre interface et implémentation.

Voici une première version sans typage (on verra cela avec la classe `Arbre` et
vous pourrez modifier a posteriori).

```python
class Pile:
    
    def __init__(self: Pile) -> None:
        self.__tete = None
        
    def est_vide(self):
        pass

    def empile(self, elt) -> None:
        """La tête a pour valeur la valeur entrée et pointe vers l'ancienne tête"""
        pass
		
    def depile(self: Pile) -> ...:
        """On enlève la tête de la pile et on retourne la valeur de la tete"""
            
    def __repr__(self: Pile) -> str:
        return f"{self.__tete.get_valeur()}|-"
```

Vous remarquerez  que sans typage, le  code est parfois difficile  à suivre. Une
idée d'activité serait de typer ces méthodes.



Voici une seconde version qui utilise les listes `python` (complétez les types):

```python
class Pile:

    p = []
	
	def est_vide(self):
        return self.p == []

    def dépile(self):
		assert not self.est_vide(), "La pile vide ne peut pas être dépilée"
        return self.p.pop()
    
    def empile(self, val):
        self.p.append(val)

	def __repr__(self):
        return f"{p[-1]}|-"

```


Mais **quelque soit l'implémentation, l'interface est la même**.

Ainsi par exemple, que ce soit l'une ou l'autre implémentation, on obtient:

```python
In [124]: p = Pile()

In [125]: p
Out[125]: None|-

In [126]: p.empile(1)

In [127]: p.empile(2)

In [128]: p
Out[128]: 2|-

In [129]: p.empile(3)

In [130]: p
Out[130]: 3|- 

In [131]: p.depile()
Out[131]: 3

In [132]: p
Out[132]: 2|-

In [133]: p.depile()
Out[133]: 2

In [134]: p
Out[134]: 1|-

In [135]: p.depile()
Out[135]: 1

In [136]: p
Out[136]: None|-

In [137]: p.depile()
---------------------------------------------------------------------------
ValueError: La pile vide ne peut pas être dépilée
```



## HP15C

![hp15c](./IMG/hp15c.png)

$`9/((2+\lnot 3)\times (4- \lnot 5))`$

est entré:

`9 2 3 NEG + 4 5 NEG - * /`



* On lit l'expression de gauche à droite;
* On empile les opérandes;
*   Dès qu'on
lit un  opérateur, on l'applique  aux opérandes présents  sur la pile  selon son
arité;
* On s'arrête quand on n'a plus rien à lire;
* on renvoie le dernier élément présent dans la pile.

Avantages:

* pas besoin de parenthèses, de règles d'associativité, de priorité;
* on voit au sommet de la pile les résultats intermédiaires;
* le calcul est directement implémentable sur machine.


## Piles, Files

Une  file est  une  structure similaire  à  une pile  mais  fonctionnant sur  le
principe du  "Premier Arrivé  Premier Sorti"  (PAPS) ou  plus internationalement
FIFO (*queue*).

**Créer plusieurs implémentations d'une classe `File`**

Vous devez obtenir quelque chose comme ça :

```python
In [18]: f = File()

In [19]: f.enfile(1)

In [22]: f.enfile(2)

In [23]: f.defile()
Out[25]: 1

In [26]: f.defile()
Out[26]: 2

In [27]: f.defile()
-------------------------------------------------
InvalidExpressionError La file vide ne peut être defilée
```



