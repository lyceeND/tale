# EXERCICES



## 1) Types abstraits 

■ 1) Quelle opération ne fait pas partie de l'interface d'une pile ?

a) ajouter un élément à la pile
b) retirer l'élément le plus récent de la pile
c) retirer l'élément le plus ancien de la pile

■ 2) Quelle opération ne fait pas partie de l'interface d'une file ?

a) ajouter un élément à la file
b) retirer l'élément le plus récent de la file
c) retirer l'élément le plus ancien de la file

■ 3) L'opération `dequeue` d'une file s'exécute en un temps qui est proportionnel
   au nombre de valeurs stockées dans la file.

a) Faux
b) Vrai

■ 4) Un tableau associatif permet de créer une association clé -> valeur. Pour
   stocker des numéros de téléphone à l'aide d'un tableau associatif, quelle
   solution semble préférable, dans la mesure où il peut y avoir des homonymes ?

a) La clé est le numéro de téléphone, et la valeur est le nom correspondant
b) La clé est le nom et la valeur est le numéro de téléphone correspondant
c) La clé est le nom et la valeur la collection des numéros de téléphones
correspondants
d) La clé est un simple numéro unique, et la valeur le couple nom/téléphone

## 2) Files 

■ 1) Pour que deux implémentations du même type abstrait soient interchangeables,
   il faut que :

a) la complexité en temps soit la même dans les deux cas
b) l'interface de la structure de données soit la même dans les deux cas
c) les deux impléméntations soient parfaitement identiques


## 3) Structures de données Python 

■ 1) Le type `list` utilisé dans Python correspond le mieux :

a) au type abstrait liste chaînée
b) au type abstrait file
c) au type abstrait tableau

■ 2) La récupération d'un élément d'une `list` Python, connaissant son indice :

a) nécessite un temps proportionnel au nombre d'éléménts de la liste
b) s'effectue en temps constant
c) est impossible

■ 3) Pour implémenter une pile avec Python, on peut se servir d'un type de données
   disponible dans le langage :

a) le type `list`
b) le type `dict`
c) le type `set`
c) le type `tuple`

■ 4) On dispose d'un fichier contenant un grand nombre de numéros de série
   (quelques millions), et on doit vérifier si certains numéros (une dizaine)
   sont dans cette liste ou non. Une fois le fichier lu, quelle est la structure
de données la plus adaptée pour stocker ces numéros :

a) le type `list`
b) le type `set`
c) une file
d) une pile
e) le type `list`, puis on triera la liste pour pouvoir faire une recherche
dichotomique


## Entretien Google


Dans cet exercice on considère des chaînes de caractères avec 3 types  de parenthèses () [] {} (par exemple `s = {([[]]))}[)`.


1.   Parmi  les   chaînes  de   caractères  suivantes,   lesquelles  sont   bien
   parenthésées ?
   
* `( ) [ ] { ( ) }`
* `( [ ( ] ) )`
* `( [ ] [ ] { ( ) }` 
* `( [ ] [ ] { ( ) } )`


2. Écrire une fonction  qui prend en argument une telle  chaîne de caractères et
   vérifie qu'elle est bien parenthésée, c'est-à-dire qui renvoie `True` si elle
   est bien parenthésée et `False` sinon. On pourra utiliser une pile. 
3. Les assertions suivantes doivent passer :

   ~~~python
   assert parenthesage("()")
   assert parenthesage("(()())()")
   assert not parenthesage("(()()))()")
   assert not parenthesage("(()())(")
   assert not parenthesage("(()(b))()")
   ~~~
3. Indiquer votre complexité en temps et en espace.
4. Comment simplifier l'algorithme dans le cas  où il n'y aurait qu'un seul type
   de parenthèses ? Quelle est alors la complexité en temps et en espace ? 
   

## Nouvelle implémentation

```python
class Noeud:
    """Un noeud a une valeur et pointe vers un autre noeud"""

    def __init__(self,elt = None, sui = None):
        self.__valeur  = elt
        self.__suivant = sui

    def get_valeur(self):
        """accesseur"""
        return self.__valeur
    
    def set_valeur(self,v):
        """mutateur"""
        self.__valeur = v

    def get_suivant(self):
        return self.__suivant

    def set_suivant(self,s):
        self.__suivant = s

    def  __repr__(self):
        """ on affiche seulement la valeur du noeud """
        return str(self.get_valeur())
		
		
class Pile:
    
    def __init__(self):
        self.__tete = None
        
    def est_vide(self) -> bool:
        return self.__tete is None

    def empile(self, elt) -> None:
        """La tête a pour valeur la valeur entrée et pointe vers l'ancienne tête"""
        n = Noeud(elt)
        n.set_suivant(self.__tete)
        self.__tete = n
        
    def dépile(self):
      """
      On enlève la tête de la pile et on retourne la valeur de la tete
      """
      pass
            
    def __repr__(self) -> str:
        return f"{self.__tete.get_valeur()}|-"
```
1. Compléter la méthode `dépile` 
2. Vérifier que votre fonction de bon parenthésage fonctionne aussi avec cette nouvelle implémentation

## Création de nouvelles méthodes

On dispose  d'une classe `Pile`  avec les 3 méthodes  vues plus haut.  Il s'agit
d'en créer de nouvelles. 

Dans chaque cas:

* Vous indiquerez pour chaque fonction la complexité en temps et en espace.
* Certaines  fonctions supposent que la  pile est non-vide. On  ne vérifiera pas
  que la pile est bien non-vide et  on laissera l'erreur survenir si la pile est
  en fait vide.
* Vous veillerez à bien faire la différence entre modifier une pile existante et
  créer une nouvelle pile. 
*  Vous  n'utiliserez aucune  autre  structure  de  données  que les  piles.  En
  particulier, vous n'utiliserez pas de listes. 
  
Implémenter les fonctions suivantes sur les piles:
  
1. `p.nonepop()` qui  a le même effet  que `p.dépile()`, sauf si  la pile est
   vide : dans ce cas, la pile reste inchangée et `p.nonepop()` renvoie `None`. 
2. `p.duplique()` qui duplique l'élément au sommet de la pile.
3. `p.echange()` qui échange les deux premiers éléments de la pile `p`.
4. `p.ecrase()` qui efface tous les éléments de `p`.
5. `p.inverse ()` qui inverse l'ordre des éléments de `p`.
6. `p.copy ()` qui renvoie une nouvelle pile, qui est une copie de `p`




## Créer sa HP15C

Créez une calculatrice  qui effectue les calculs  arithmétiques à la
  manière d'une HP15C.

  On  pourra se  contenter d'une  version simplifiée:  on donne  comme
  argument  la chaîne  correspondant  à l'opération  à effectuer  sous
  forme postfixée. La fonction renvoie le résultat de l'opération. Par
  exemple:
  
  ```python
In [1]: seq = "2 3 4 + *"

In [2]: npi(seq)
Out[2]: 14
  ```
  

On pourra utiliser les fonctions du module `operator`.

Par exemple, `operator.add(2,3)` renvoie 5.

On peut alors créer un dictionnaire des opérations:


```python
operations = {
    '+':  operator.add, '-':  operator.sub,
    '*':  operator.mul, '/':  operator.truediv,
    '%':  operator.mod, '**': operator.pow,
    '//': operator.floordiv
}
```


Vous   pouvez    ensuite   proposer   une   calculatrice    plus 
interactive:

```python
In [1]: hp_inter()
->  |
rentrer un nombre ou un opérateur: 1
-> 1 |
rentrer un nombre ou un opérateur: 2
-> 2, 1 |
rentrer un nombre ou un opérateur: 3
-> 3, 2, 1 |
rentrer un nombre ou un opérateur: 4
-> 4, 3, 2, 1 |
rentrer un nombre ou un opérateur: '+'
-> 7, 2, 1 |
rentrer un nombre ou un opérateur: '-'
-> -5, 1 |
rentrer un nombre ou un opérateur: '+'
-> -4 |
rentrer un nombre ou un opérateur: 3
-> 3, -4 |
rentrer un nombre ou un opérateur: '*'
-> -12 |
rentrer un nombre ou un opérateur: 'fin'
```
