class Pile:
    """
    Implémentation de piles à l'aide des listes Python
    et de leurs méthodes pop et append.
    Une pile est alors une liste qui s'opére uniquement
    par la droite
    """

    def __init__(self) -> None:
        """
        Une pile est implémentée à l'aide d'une liste Python
        """
        self._p = []

    def est_vide(self) -> bool:
        return self._p == []
    
    def dépile(self):
        """
        On vérifie que la liste n'est pas vide
        et on utilise la méthode pop qui enlève de la 
        liste l'élément le plus à droite et le renvoie
        """
        assert not self.est_vide(), "La pile vide ne peut pas être dépilée"
        return self._p.pop()
    
    def empile(self, val):
        """
        Rajoute un élément à droite de la liste
        """
        self._p.append(val)
    
    def __repr__(self):
        return f"{self._p[-1]}|-"
