class Noeud:
    """
    Un noeud a une valeur et pointe vers un autre noeud
    """

    def __init__(self, elt = None, sui = None):
        self.__valeur  = elt # C'est une valeur, pas un noeud 
        self.__suivant = sui # C'est un noeud ou None

    def get_valeur(self):
        """accesseur"""
        return self.__valeur
    
    def set_valeur(self, v):
        """mutateur"""
        self.__valeur = v

    def get_suivant(self):
        return self.__suivant

    def set_suivant(self, s):
        self.__suivant = s

    def  __repr__(self):
        """
        on affiche seulement la valeur du noeud 
        """
        return str(self.get_valeur())
		
		
class Pile:
    
    def __init__(self):
        self.__tete = None # Noeud ou None
        
    def est_vide(self) -> bool:
        return self.__tete is None

    def empile(self, elt):
        """
        La tête a pour valeur la valeur entrée et pointe vers l'ancienne tête
        """
        n = Noeud(elt) # On rentre la valeur elmt dans un noeud
        n.set_suivant(self.__tete) # On le relie à l'ancienne tête
        self.__tete = n # On fait de ce noeud la nouvelle tête
        
    def dépile(self):
        """
        On enlève la tête de la pile et on renvoie la valeur de la tête
        """
        if self.est_vide():
            raise ValueError("La pile vide ne peut pas être dépilée")
        old_tete = self.__tete # tête avant dépilement
        self.__tete = old_tete.get_suivant() # nouvelle tête est le 2e noeud
        return old_tete.get_valeur() # renvoie la valeur de l'ancienne tête
            
    def __repr__(self):
        return f"{self.__tete.get_valeur()}|-"
