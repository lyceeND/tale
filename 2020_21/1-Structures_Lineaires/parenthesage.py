#from PilePy import Pile
from myPile import Pile

def fermant(paren: str) -> str:
    """
    Renvoie le fermant correspondant à l'argument paren
    """
    if paren == '(':
        return ')'
    elif paren == '[':
        return ']'
    elif paren == '{':
        return '}'
    else:
        raise SyntaxError

def parenthesage(chaine: str) -> bool:
    ps = Pile()
    for symb in chaine:
        if symb in "([{":
            ps.empile(symb)
        else:
            if ps.est_vide():
                return False
            elif symb != fermant(ps.dépile()):
                return False
    return ps.est_vide()

assert parenthesage("()")
assert parenthesage("(()())()")
assert parenthesage("(()[]){[[()()]]}")
assert not parenthesage("(()()))()")
assert not parenthesage("(()[])(")
assert not parenthesage("(()(b))()")

