# Paradigmes de programmation

## Qu'est-ce ?

Le mot **paradigme** vient du mot grec ancien παράδειγμα qui signifie « modèle ». En informatique, ce mot a été adopté pour distinguer les différentes façons d'envisager l'écriture d'un programme.

## One to rule them all

Cependant, quel que soit le programme et son mode d'écriture, il devra finir sous la  forme  de code  machine **impératif**  :  donner  à la  machine  des instructions élémentaires pour lui dire comment
effectuer ses allers-retours entre le processeur  et la mémoire (Voir la machine de Von Neumann - fiche 25, NSI 1ère).
Ce style (paradigme) de programmation impératif étant  au coeur de la machine, la majorité des langages l'a adopté.

## Pourquoi d'autres paradigmes ?

En  1954,  John Von  Neumann,  lorsqu'on  lui  a  présenté FORTRAN,  le  premier véritable langage de programmation, aurait déclaré:

*Pourquoi voudrait-on autre chose que le langage machine ?*

C'est  parce qu'il  est préférable  de  rapprocher le  langage de  la pensée  du programmeur plutôt que de lui demander de penser en fonction de la machine.
Or l'être humain a différentes façons de penser : différents types de langages ont donc été proposés. 

La tendance actuelle  est de ne pas  attacher un langage à un  paradigme mais de permettre l'adoption de plusieurs styles  afin de  pouvoir s'adapter  à différentes situations.

## Des exemples

Des dizaines de paradigmes de programmation ont été décrits, outre le paradigme impératif.

Son direct  "opposé" est le  paradigne **déclaratif** où  l'on décrit ce  que l'on
veut faire sans demander au programmeur de donner les instructions précises à la
machine. On  trouve notamment  dans cette  catégorie la  **programmation logique**
(voir Exercice Prolog), **fonctionnelle**
(voir fiche 9), le SQL (voir chapitre 8).

À  la fin  des années  50,  les concepts  de la  **Programmation Orientée  Objet**
apparaissent.  Ils auront un impact très important sur l'informatique d'aujourd'hui et sont  présentés  plus  en détail  dans  la suite.

La **Programmation  Orientée Acteurs**  et  plus généralement  la
programmation   permettant  de   gérer  la   **concurrence**  a   pris  énormément d'importance depuis une dizaine d'années pour tenir compte du changement drastique dans l'architecture des machines  qui disposent de multiples  coeurs ou processeurs. Des langages  comme Erlang,
Elixir, Scala, Go en sont l'illustration.

La liste ne saurait être exhaustive mais il faut évoquer la programmation qui révolutionne le monde depuis les années 2014 : l'**apprentissage automatique**. On programme des machines pour qu'elles pensent et programment par elles-mêmes...

## Programmation modulaire

Dans le cadre de la programmation classique de type impératif en langage Python, C, FORTRAN, une notion fondamentale s'est dégagée : celle de **modularité** à travers l'usage de *fonctions*. Un programme est alors constitué de deux types d'entités distinctes : les *données* et les *fonctions*. Les fonctions peuvent être utilisées hors de leur contexte initial mais il faut alors redéfinir les données à manipuler (à moins d'utiliser des variables globales, ce qui est connu comme étant une source intarissable de déboires). Cette conception donne la primauté aux *traitements*. Les briques logicielles sont essentiellement des morceaux de code réutilisables n'intégrant pas de données. Cette dissociation des données et des traitements restreint l'autonomie de ces briques logicielles et par la même leur *réutilisabilité*. La notion d'Objet va essayer de remedier à cela.

## Le paradigme objet

### Historique

Le paradigme Objet a été élaboré par les Norvégiens Ole-Johan Dahl et Kristen Nygaard au début des années 1960 à travers le langage Simula et repris et approfondi par l'Américain Alan Kay dans les années 1970 dans le dévelopement du langage SmallTalk qui est devenu une référence dans le domaine.
La Programmation Orientée Objet se retrouve dans nombre de langages aujourd'hui comme C++, C#, Java, Python, Eiffel, Ruby ou OCaml qui sont des langages **à classes**. JavaScript est aussi un langage Objet mais doté de caractéristiques fonctionnelles et il se range dans la catégorie plus rare des langages objets **à prototypes**.

### Programmation Orientée Objet

Pour remedier aux difficultés posées par la programmation modulaire, on considère un nouveau type d'entité : l'*Objet* qui intègre en lui-même les données et les traitements. Dans cette perspective, les données trouvent leur place au sein de ces briques logicielles. L'objet prend ainsi une certaine *autonomie* et est en mesure de dialoguer avec les autres objets de son univers. La notion d'objet nécessite l'adoption d'une nouvelle approche de conception logicielle. On répond d'abord à la question: *De quoi parle-t-on ?* et non pas à *Que veut-on faire ?*. Nous le retrouverons fiche 10.
