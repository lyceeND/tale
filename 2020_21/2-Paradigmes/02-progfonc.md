# Programmation fonctionnelle


## La programmation fonctionnelle hier, aujourd'hui et demain

La  programmation  fonctionnelle est  née  en  même  temps que  la  programmation impérative (LISP dans les années 1950) mais est longtemps restée marginale, plus académique qu'industrielle (langages ML et dérivés, Haskell). 

Cependant,  la  plupart des  langages  apparus  au  XXIe siècle  sont  fortement influencés par un *style fonctionnel*.





Au moment d'être racheté en 2014 par Facebook, WhatsApp comptait 32 informaticiens pour
gérer 1 milliard  d'utilisateurs par mois !  Le secret ? Whatsapp  est écrit en
`Erlang`, langage concurrent et fonctionnel.

Facebook a recruté de nombreux programmeurs (français) `Ocaml` et utilise aussi des outils développés en `Haskell` (`haxl`)

Twitter a commencé en `Ruby` mais en grandissant, Twitter est passé à `Scala` pour son aspect fonctionnel.

De nombreuses banques (Deutsche Bank, Barclays, Crédit Suisse, BNP, JPMorgan...) utilisent Haskell.

Google utilise `Haskell`

`Kotlin` est le langage officiel d'Android  depuis mai dernier. Il est largement
fonctionnel.

`Rust` est le langage phare de Mozilla et est largement fonctionnel.



Le langage `Go` lancé par Google a beaucoup d'aspects fonctionnels comme le typage statique fondé sur l'inférence de type, les fonctions d'ordre supérieur, l'évaluation paresseuse.

Le New York Times pour gérer les grands flux d'information


De manière générale, le style fonctionnel est de plus en plus influent depuis quelques années.

Dans les 10 langages associés aux salaires les plus élevés aux États-Unis, 7 ont
de  fortes connotations  fonctionnelles  : `Scala`,  `Clojure`,  `Erlang`,
`Kotlin`, `Rust`, `F#`, `Elixir` et on pourrait même ajouter `Go`. Voir la [page stackoverflow](https://insights.stackoverflow.com/survey/2019#technology-_-what-languages-are-associated-with-the-highest-salaries-worldwide)








## Non pas *Comment ?* mais *Quoi ?*

### Une programmation déclarative


**Quelle est la somme des 100 premiers entiers naturels pairs tels que le carré de la
somme d'un de ces nombres et de son successeur est
divisible par 13793 ?**

Première idée avec Python, mais...

```python
a = 0
b = 0
c = 0
while b < 100:
	if (c + c + 1)**2 % 13793 == 0:
		a += c
		b +=1
	c += 2
print(a)
```

Avec Haskell :

```haskell
sum $ take 100 $ filter (\x -> (x + x + 1)^2 `mod` 13793 == 0) [0,2..]
```

Avec Python influencé par Haskell, mais...

```python
gen = (x for x in range(10000000000000000) if (x + x + 1)**2 % 13793 == 0 and x % 2 == 0)
sum([next(gen) for _ in range(100)])
```

ou en utilisant la fonction `count` de la bibliothèque `itertools`, mais...

```python
gen = (x for x in __import__("itertools").count(0,2) if (x + x + 1)**2 % 13793 == 0)
sum([next(gen) for _ in range(100)])
```




On y compose des fonctions dont le nom permet de comprendre le rôle.


### Transparence référentielle

En programmation fonctionnelle, il n'y a pas d'affectation. Lorsqu'on écrit `a =
1`, `a` peut être remplacé par `1` tout au long du programme.






```python
n: int = 1

def inc(k: int) -> int:
    global n
    n = n + k
    return n


for appel in range(1,5):
    print(f"\n Appel numéro  {appel} : inc(1) + inc(2) = {inc(1) + inc(2)}")
```

```console
 Appel numéro  1 : inc(1) + inc(2) = 6

 Appel numéro  2 : inc(1) + inc(2) = 12

 Appel numéro  3 : inc(1) + inc(2) = 18

 Appel numéro  4 : inc(1) + inc(2) = 24
```

Ça peut  être pratique  pour moins  se casser  la tête  au moment  de programmer
mais...



>Insanity Is  Doing the Same Thing  Over and Over Again  and Expecting Different
>Results




Avoir de la transparence référentielle offre de nombreux avantages :

* **modularisation** : pas de notion  d'état des variables donc on peut découper
  en  petits   modules  selon  l'architecture  "botom-up".   Cela  simplifie  la
  maintenance, permet la  réutilisation. On construit les briques du  lego et on
  les assemble.
  
* **idempotence** : vous obtenez toujours  le même résultat pour chaque appel de
  la fonction.
  
* **parallélisation** : du fait de  l'indépendance des appels d'une fonction, on
  peut les effectuer indépendemment. Par exemple si 
  `res = f1(a,b) + f2(a,c)`, on peut  paralléliser les appels à `f1` et `f2` car
  `a` ne sera pas modifié.
  
* **mémoïsation** :  on peut garder en  cache les appels à une  fonction dans un
  cache. Cela facilite la programmation dynamique que nous verrons plus tard.



### Récursion

Sans affectations ni  boucles, la programmation fonctionnelle  passe souvent par des  définitions  **récursives** de  fonctions,  ie.  des fonctions  qui  s'appellent elles-mêmes.




## Récursion ne signifie pas Pile

En Haskell :

```haskell
fib :: Int -> Integer
fib = \n -> fib' n 0 1
       where fib' 0 prems _    = prems
             fib' n prems deuz = fib' (n-1) deuz (prems + deuz)
```

En JavaScript :

```javascript
function fib(n) {
	function fib_aux(n, prems, deuz) {
		if (n > 0) {
			return fib_aux(n - 1, deuz, prems + deuz);
			}
		else {
			return prems;
			} 
	}
	return fib_aux(n, 0, 1);
}
```

En PHP :

```php
function fib($n, $prems = 0, $deuz = 1) 
{ 
    if ($n == 0) 
        return $a; 
    return fib($n - 1, $deuz, $prems + $deuz); 
} 
```


En Python, la possibilité  de donner des arguments par défaut  nous permet de ne
pas avoir à créer une fonction imbriquée :

```python
def fib(n: int, prems:int = 0, deuz:int = 1, nb_appels:int = 0) -> int:
    if n == 0:
        print(f"Il y a eu {nb_appels} appels")
        return prems
    else:
        return fib(n-1, deuz, prems + deuz, nb_appels + 1)
```




### idempotence

Vous obtenez toujours le même résultat pour chaque appel d'une fonction. Il faut donc  éviter  au maximum  les  variables  globales  et  les fonctions  à  effets secondaires.

### concurrence et/ou parallélisation

L'idempotence et la transparence  référentielle permettent aux langages modernes de gérer  efficacement la  concurrence si importante  actuellement.  On  peut en effet se libérer de contraintes de chronologie.
Par exemple si 
  `res =  f1(a,b) +  f2(a,c)`, on  peut effectuer les  appels à  `f1` et  `f2` à n'importe uel moment car `a` ne sera pas modifié.
  
### Pas d'affectation : des liens !

...ou encore : **ne modifiez pas ! Créez !** 

C'est une conséquence de la transparence référentielle.
Une expression est liée à une valeur. 
Si on veut changer cette valeur, il faut créer une autre expression.

On peut s'en inspirer en Python aussi. Ainsi au lieu de :

```python
nom = 'James'
nom = nom + 'Bond'
```

on préférera :

```python
prenom = 'James'
nom = 'Bond'
appellation = prenom + nom
```

### Les fonctions sont des objets comme les autres

Les fonctions peuvent prendre d'autres fonctions en argument :








```python
def appeler(saluer:Callable[[str], str]) -> str:
	return saluer('James Bond')
	
def saluer_eng(name: str) -> str:
	return f'Hello, my name is {name}'
	
def saluer_fr(nom: str) -> str:
	return f'Bonjour, mon nom est {nom}'
```

`appeler`,  `saluer_eng` et `saluer_fr` sont des fonctions et pourtant :

```python
In [1]: appeler(saluer_eng)
Out[1]: 'Hello, my name is James Bond'

In [2]: appeler(saluer_fr)
Out[2]: 'Bonjour, mon nom est James Bond'
```







Supposons maintenant que nous voulions filtrer des listes selon des critères changeants. On peut utiliser l'opérateur `lambda` qui permet de créer des fonctions selon la syntaxe `lambda arguments : image`. Par exemple, pour créer "à la volée" la
fonction $x\mapsto 2x+1$:

```
     lambda              x          :           2*x + 1
"Une fonction qui    prend x   et retourne      2*x + 1"
```

Dans le cas d'un filtre :

```python
def filtre(critère):
	return lambda liste: [élément for élément in liste if critère(élément)]
```

Par exemple pour obtenir les nombres pairs et postifis d'une liste d'entiers :

```python
>>> pair_pos = filtre(lambda n: n % 2 == 0 and n >= 0)

>>> pair_pos([-4, -3, -2, -1, 0, 1, 2, 3, 4])
[0, 2, 4]
```


