# Exercices



## Principes de la POO

1. À travers lequel de ces langages le paradigme objet a-t-il été historiquement
   introduit pour la première fois ?

```
☐ a. SmallTalk ☐ b. Simula 
☐ c. Java ☐ d. C++ 
```

<!--
b
--->

2. Lequel de ces mots-clés marque le début de la définition d'une classe:

```
☐ a. class ☐ b. def
☐ c. from ☐ d. __init__
```

<!--
a
--->

3. On suppose que l'on dispose de la définition d'une classe `Entier`. Laquelle de ces affirmations décrit le mieux la déclaration `x = Entier()`:


```
☐ a. x contient une valeur de type int 
☐ b. x contient un objet de type Entier
☐ c. x contient une référence à un objet Entier
☐ d. On peut affecter une valeur de type int à x
```

<!--
c
--->

4. Quelle va être la sortie en réponse à l'exécution du code suivant :

```python
class Alien:
    def __init__(self, id = 'Zorglub'):
        self.id = id

bul = Alien('Bulgroz')
print (bul.id)
```


```
☐ a. Une erreur de syntaxe va empécher le programme de tourner
☐ b. Zorglub
☐ c. Bulgroz
☐ d. bul
```

<!--
c
--->



5. Quelle sera l'issue de l'exécution de ce code :

```python
class test:
    def __init__(self, a):
        self.a = a

    def montre(self):
        print(self.a)

obj = test()
obj.display()
```



```
☐ a. Une erreur car la création de l'objet nécessite un argument
☐ b. Pas d'erreur mais n'affiche rien
☐ c. Affiche None 
☐ d. Une erreur car test est en minuscule
```

<!--
a
--->

## Programmation fonctionnelle


6. Quelle est l'issue du code suivant :


```python
mesFonctions = [lambda x: x ** 2,
                lambda x: x ** 3,
                lambda x: x ** 4]

for f in mesFonctions:
    print(f(3))
```

```
☐ a. 27 81 343
☐ b. 9 27 81
☐ c. f(3) f(3) f(3)
☐ d. Rien car on ne peut pas créer des listes de fonctions
```


<!--
b
--->


7. Quelle est l'issue du code suivant:

```python
from functools import reduce
f = lambda a, b: a if (a > b) else b
num = reduce(f, [47,11,42,102,13])
print(num)
```

Voici la documentation de `reduce` : *Applique la fonction de deux arguments cumulativement aux éléments de l'itérable, de gauche à droite, afin de réduire l'itérable à une seule valeur. Par exemple, réduire `(lambda x, y: x + y, [1, 2, 3, 4, 5])` effectue le calcul `((((1 + 2) +3) +4) +5)`. L'argument de gauche, `x`, est la valeur accumulée et l'argument de droite, `y`, est la valeur de mise à jour de l'itérable. Si la valeur initiale facultative est présente, elle est placée avant les éléments de l'itérable dans le calcul et sert de valeur par défaut lorsque l'itérable est vide. Si la valeur initiale n'est pas donnée et que l'itérable ne contient qu'un seul élément, le premier élément est renvoyé.*

```
☐ a. num
☐ b. 47114210213
☐ c. 11
☐ d. 102
```




## Manipuler des fonctions d'ordre supérieur 

Voici une fonction mystérieuse:

```python
def f(n):
    def g(m):
        return m ** n
    return g
```

Analyser cette fonction, déterminez sa signature, comment l'utiliser, donnez des exemples.

<!--
In [223]: f(3)(2)
Out[223]: 8

In [224]: cube = f(3)

In [225]: cube(2)
Out[225]: 8

In [226]: cube(3)
Out[226]: 27

In [227]: carre = f(2)

In [228]: carre(5)
Out[228]: 25
--->



## Créer une fonction "Prend tant que" 

Créer une fonction  `prend_tantque` qui prend en argument une  liste d'objets de même type  et une  fonction booléenne  et qui  renvoie les  éléments de  la liste jusqu'à tomber sur un  élément qui renvoie Faux lorsqu'il est  en argument de la fonction booléenne.

Par exemple, sachant que `est_pair(n)` renvoie vrai si, et seulement si l'entier `n` est pair, on obtiendra :

```python
>>> prend_tantque([0, 2, 4, 5, 6, 8, 10], est_pair)
[0, 2, 4]
```

<!--
def est_pair(n: int) -> int:
    return n%2 == 0

def prend_tantque(xs, f):
    res = []
    n = len(xs)
    if n > 0:
        i = 0
        while i < n and f(xs[i]): # ordre !
            res.append(xs[i])
            i += 1
    return res
--->


## Manipuler les fonctions `map` et `reduce` 

Les  fonctions  `map` et  `reduce`  sont  les  fondements (avec `filter`) <!-- LS: Non ? --> de  la  programmation fonctionnelle.  Nous allons  les  traduire  par `applique`  et  `reduit` et  les fabriquer puis les utiliser pour résoudre quelques problèmes.

1. La fonction  `applique` a deux paramètres  : une fonction et  une liste. Elle renvoie une  nouvelle liste  contenant les  images des  éléments de  la liste initiale.
   
   Par exemple, on peut obtenir la liste des longueurs d'une liste de noms :
   
   ```python
   >>> applique(len, ['Joe', 'Max-Bill', 'Alexandra'])
   [3, 8, 9]
   ```
   
   ou les carrés des éléments d'une liste:
   
   ```python
   >>> applique(lambda x: x * x, [2, 4, 5])
   [4, 16, 25]

   ```
   a. Créer la fonction `applique(fonction, liste)` qui remplit ce rôle.
   b. Utiliser `applique`  pour mettre  les éléments  d'une liste  de chaînes  de caractères en majuscule. On pourra utiliser la méthode `upper`.
   c. Que désignera la variable `mystere` définie par :
      ```python
	  mystere = lambda liste: applique(lambda nb: nb**2, liste)
	  ```
	  Comment l'utiliser ? Donner un exemple.
   d.  En une seule ligne, et un utilisant `applique`, construire une fonction `majuscule` qui permet par exemple d'obtenir :
	  ```python
	  >>> majuscule(['Joe', 'Max-Bill', 'Alexandra'])
	  ['JOE', 'MAX-BILL', 'ALEXANDRA']
	  ```
	  
2. La fonction `reduit`  prend une fonction, une valeur de  départ, une liste et renvoie une combinaison des éléments de la liste. 
   
   Par exemple, voici comment obtenir  la concaténation des éléments d'une liste de caractères :
   
   ```python
    >>> reduit(lambda x, y: x + y , '', ['a', 'b', 'c', 'd'])
	'abcd'
	```
	
	On peut alternativement créer une fonction spécifique :
	
	```python
	>>> joint = lambda xs: reduit(lambda acc, x: acc + x, '', xs)
	
	>>> joint(['a', 'b', 'c', 'd'])
	'abcd'
	```

<!--LS: Pb Markdown dans ce qui suit, parfois, ```python apparaît dans le
document final. C'est pour ça que j'ai réaligné.
-->

a. Créer la fonction `reduit(fonction, depart, liste)` qui remplit ce rôle.

b. Utiliser  `reduit` créer une fonction qui vérifie qu'une liste de booléens  est constituée uniquement de `True`:

```python
>>> tous_vrais([3 > 2, len('Papa') == 4, 4 % 2 == 0])
True
```

c. Utiliser `reduit` pour calculer le nombre d'éléments d'une liste :

```python
>>> taille([1, 2, 3, 4, 3, 2, 1])
7
```

d.  Utiliser  `reduit` pour  filtrer  les  éléments  d'une liste  selon  une
       fonction booléenne.

```python
>>> filtre(lambda x: x%2 == 0, [1, 2, 3, 4, 5, 6])
   [2, 4, 6]
```

e. Utilisez `reduit` pour fabriquer `applique` !

<!--

def applique(fonction, liste):
    res = []
    for element in liste:
        res.append(fonction(element))
    return res

def reduit(fonction, depart, liste):
    acc = depart
    for element in liste:
        acc = fonction(acc, element)
    return acc


mystere = lambda liste: applique(lambda x: x*x, liste)

majuscule = lambda xs : applique(lambda mot: mot.upper(), xs)

tous_vrais = lambda xs : reduit(lambda b1, b2: b1 and b2, True, xs)

taille = lambda xs: reduit(lambda acc, x: acc + 1, 0, xs)

filtre = lambda f, xs: reduit(lambda acc, x: acc + [x] if f(x) else acc, [], xs)

applique2 = lambda f, xs: reduit(lambda acc, x: acc [f(x)], [], xs)


concatene = lambda xs: reduit(lambda acc, x: acc + x, '', xs)

--->

## Découvrir la programmation logique avec Prolog 

### Présentation succincte

Prolog est né à Marseille en 1972 avec pour but de laisser le compilateur faire
le  gros du  travail, l'utilisateur  se  contentant de  donner des  instructions
logiques (PROgrammation  LOGique). C'est un langage  de programmation déclaratif
que doivent pouvoir utiliser des non-informaticiens.


Programmer en Prolog nécessite de :

* spécifier des faits avérés sur des "individus" et leurs relations ;
* définir des règles sur ces individus et leurs relations ;
* poser des questions sur ces individus et leurs relations.


On peut installer la distribution `swi-prolog`
(https://www.swi-prolog.org/download/stable) qui est multiplate-forme.

Sur des distributions Debian/Ubuntu il suffit de taper dans un terminal:

```console
$ sudo apt-get install swi-prolog
```

On  enregistre  un  fichier  Prolog  avec l'extension  `pl`  dans  un  éditeur quelconque. On lance  ensuite `prolog` dans un terminal et  on charge le fichier créé.

Par exemple,  on crée le  fichier `famille.pl` avec  un certain nombre  de faits avérés (clauses) portant sur des individus  et des  villes (attention à  n'employer que des minuscules et à terminer par un point):

```prolog
habite(joe, london).
habite(claire, nantes).
habite(bill, london).
habite(anne, avonlea).
```

On lance `prolog` dans le répertoire de travail :

```console
$ prolog
```

puis on charge le fichier et on vérifie quelques *clauses* :

```prolog
?- [famille].
true.

?- habite(joe, london).
true.

?- habite(joe, nantes).
false.
```

On  peut ensuite  demander qui  habite  à Nantes.  Attention à  bien mettre  des majuscules pour les inconnues et des minuscules pour les constantes.

```prolog
?- habite(X, nantes).
X = claire.
```

Où habite Joe ?

```prolog
?- habite(joe, V).
V = london.
```

Qui habite dans la même ville que Joe ?

```prolog
?- habite(joe, V), habite(Y,V).
V = london,
Y = joe ;
V = london,
Y = bill.
```
La  virgule `,`  signifie  *ET* tandis  que le  point-virgule  `;` signifie  *OU inclusif*.

Lorsqu'une réponse est affichée, on provoque l'affichage des autres réponses en appuyant sur `;`, Tab ou sur la barre d'espace.
(dans l'exemple qui précède, il faut le faire après l'affichage de `Y = joe`)

On peut créer une règle qui vérifie si deux personnes différentes habitent la même ville. L'opérateur de différence est `\==`. Pour cela on ajoute dans le fichier `pl`:

```prolog
concitoyen(X, Y):- habite(X,V), habite(Y,V), X\==Y.
```

puis 

```prolog
?-[famille].
true.

?- concitoyen(joe, X).
X = bill.
```

### Arbre généalogique

On dispose  d'un certain nombre de  renseignements sur une famille  : leurs noms, leur  genre et  nous connaissons  les  parents  d'un certain  nombre d'entre eux. Ainsi `parent(X, Y)` signifie que `X` est le père ou la mère de Y.
On regroupe ces renseignements dans un fichier `famille.pl`:


```prolog
femme(svetlana).
femme(macha).
femme(olga).
femme(ania).
femme(anastasia).
femme(galina).
femme(oxana).
femme(nadia).
femme(natalia).
homme(piotr).
homme(ivan).
homme(pavel).
homme(igor).
homme(dmitri).
homme(mitia).
homme(ossip).
homme(modeste).
homme(nikolai).
parent(svetlana, oxana).
parent(piotr, oxana).
parent(svetlana, ivan).
parent(macha, pavel).
parent(nadia, dmitri).
parent(macha, anastasia).
parent(olga, ania).
parent(svetlana, mitia).
parent(piotr, mitia).
parent(modeste, macha).
parent(nikolai, dmitri).
parent(olga, igor).
parent(anastasia, galina).
parent(piotr, ivan).
parent(macha, ossip).
parent(natalia, macha).
parent(ivan, pavel).
parent(pavel, ania).
parent(ivan, anastasia).
parent(ivan, ossip).
parent(dmitri, galina).
parent(pavel, igor).
```

1.  Définir une  règle `pere(P,  E)` qui  est  vraie si  `P` est  le père  de `E`. Définir une règle similaire pour `mere(M, E)`
2. Quel est le  père d'Ivan ? La mère de Dmitri ?  Quels sont les enfants
   d'Ivan ? 
3.  Définir une  règle `fils(E,  P)` qui  est  vraie si  `E` est  le fils  de `P`. Faire de même pour `fille(E, P)`. 
4. Créer  alors une règle  `frere(F, X)` qui  est vraie si  `F` est le  frère de `X`. Faites de même pour `soeur(S, X)`.
5. Créer  de la même manière  `oncle(O, X)`, `tante(T, X)`,  `grand-pere(G, X)`,
   `grand-mere(G, X)`.
6. Quels sont les oncles d'Igor ?
7. Quels sont les frères d'Oxana ?
8. Quels sont les grand-pères de Pavel ?
9. Quels sont les petits-enfants de Svetlana ?
10. Quelles sont les nièces d'oxana ?







# PROJET : Jeu de la Vie

Le but de cet exercice est de réaliser en Python une implémentation du **jeu de la vie** en utilisant la programmation objet.

Le jeu de la vie a été inventé par le mathématicien américain John
Conway. C'est un exemple de ce qu'on appelle un **automate
cellulaire**.
Il se déroule sur un tableau rectangulaire ($`L\times H`$) de cellules.
Une cellule est représentée par ses coordonnées x et y.
$`0\leq x<L, 0\leq y<H`$

Une cellule peut être dans deux états : **Vivant** ou **Mort**.
La dynamique du jeu s'exprime par les règles de transition suivantes :

- Une cellule vivante reste vivante si elle est entourée de 2 ou
  3 voisines vivantes et meurt sinon.
- Une cellule morte devient vivante si elle possède exactement 3
  voisines vivantes.

La notion de *voisinage* dans le jeu de la vie est celle des 8 cases qui
peuvent entourer une case donnée (on parle de voisinage de Moore).
Pour implémenter la simulation, on va tout d'abord donner une 
modélisation objet du problème, puis procéder à son implémentation.

1. Modélisation Objet

a. Quelles classes pouvez-vous dégager de ce problème au premier abord ?
b. Quelles sont quelques unes des méthodes qu'on pourrait leur donner ?
c. Dans quelle classe pouvons nous représenter simplement la notion de voisinage d'une cellule ? Et le calculer ?
d. Une cellule est au bord si x=0, x=L-1, y=0 ou y=H-1. Combien de voisins possède une cellule qui n'est pas au bord ?
e. Combien de voisins possède une cellule qui est au bord ?
f. Que pourrions-nous aussi considérer comme voisin de droite de la case en haut à droite de la grille ? Et comme voisin du haut ? 

2. Implémentation  des cellules

a. Implémenter tout d'abord une classe Cellule avec comme attributs :

- un booléen `actuel` initialisé à `False`
- un booléen `futur` initialisé à `False`
- une liste `voisins` initialisée à  `None`

Ces attributs seront considérés comme "privés" ici.
La valeur `False` signifie que la cellule est morte et `True` qu'elle est vivante.

Ajouter les méthodes :

- `est_vivant()` qui renvoie l'état actuel (vrai ou faux)
- `set_voisins()` qui permet d'affecter comme voisins la liste passée en paramètre
- `get_voisins()`
- `naitre()` qui met l'état futur de la cellule à `True`
- `mourir()` qui permet l'opération inverse
- `basculer()` qui fait passer l'état futur de la cellule dans l'état actuel

b. Ajouter à la classe Cellule une méthode `__str__()` qui affiche une croix (un `X`) si la cellule est vivante et un tiret (`-`) sinon.
Expliquer brièvement l'utilité d'une telle méthode `__str__()` en Python.

c. Ajouter une méthode `calcule_etat_futur()` dans la classe `Grille` qui permet d'implémenter les règles d'évolution du jeu de la vie en préparant l'état futur à sa nouvelle valeur.

3. Implémenter la classe Grille.

a. Créer la classe `Grille` et y placer les attributs suivants considérés comme "publics" :

- `largeur`
- `hauteur`
- `matrix` : un tableau de cellules à 2 dimensions (implémenté en Python par une liste de listes)

Fournir une méthode `__init__` permettant l'initialisation d'une Grille de Cellules avec une largeur et hauteur (une nouvelle Cellule sera créée par l'appel `Cellule()` )

b. Ajouter les méthodes :

- `dans_grille()` qui indique si un point de coordonnées i et j est bien dans la grille
- setXY() qui permet d'affecter une nouvelle valeur à la case (i,j) de la grille
- getXY() qui permet de récupérer la cellule située dans la case (i,j) de la grille
- une méthode statique `est_voisin()` qui vérifie si les cases (i,j) et (x,y) sont voisines dans la grille.

c. ajouter une méthode `get_voisins()` qui renvoie la liste des voisins d'une cellule.

d. fournir une méthode `affecte_voisins()` qui affecte à chaque cellule de la grille la liste de ses voisins.

e. donner une méthode `__str__()` qui permet d'afficher la grille sur un terminal.

f. on voudrait remplir aléatoirement la Grille avec un certain taux de Cellule vivantes. Fournir à cet effet, une méthode `remplir_alea()` avec le taux (en pourcentage) en paramètre.

g. On joue à présent ! Concevoir une méthode `jeu()` permettant de passer en revue toutes les Cellules de la Grille, de calculer leur état futur, puis une méthode `actualise()` qui bascule toutes les cellules de la Grille dans leur état futur.

4. Programme principal

Définir enfin un `main` pour terminer l'implémentation du Jeu de la Vie avec un affichage en console en utilisant les méthodes précédentes.
On donne la méthode suivante qui permet d'effacer l'écran dans un terminal `ANSI` :

```python
def effacer_ecran():
    print("\u001B[H\u001B[J")
```

## Feuille de route

1) Modélisation objet
 
a. On peut proposer assez facilement 2 classes : Une classe Cellule permettant de stocker l'etat d'une cellule et son évolution avec une classe Grille représentant le plateau de jeu
b. Dans la classe Cellule, il faut pouvoir faire naitre ou mourir la cellule, connaitre son etat actuel et futur et afficher la cellule sous la forme d'un caractère. Dans la Grille, il faut pouvoir connaitre sa largeur et sa hauteur, récupérer le contenu (la cellule)  qui se trouve dans une position (i,j) dans la grille et l'affecter avec une nouvelle Cellule.
c. La notion de voisinage se calcule bien dans la grille mais une cellule doit aussi connaitre ses voisins (ou leur nombre) pour calculer son état futur.
d. Une cellule qui n'est pas au bord admet toutes les cases qui l'entourent comme voisins. Il suffit de les compter
e. Distinguez les cellules dans les angles des autres cas.
f. Imaginer une Grille qui se replie sur elle même (bord gauche - bord droit) et (bord supérieur et inférieur)

2) Implémentation des cellules

a. Les attributs sont donnés et les méthodes sont pour la plupart traitables en 1 seule ligne. Ne pas oublier de mettre `self` en premier argument des méthodes !

b. Simple test et on renvoie le caractère demandé selon l'état actuel de la cellule.

c. On implémente les règles d'évolution du jeu de la vie dans cette méthode en mettant seulement à jour l'attribut `futur`. Si possible ne pas utiliser d'accès aux attributs privés de la classe.

3) Implémentation de la grille

a. et b. La méthode `__init__()`  permet de mettre en place les attributs largeur, hauteur et matrix et de les initialiser.
Utiliser une compréhension pour initialiser aisément matrix qui est un tableau de Cellules à deux dimensions.
Pour `est_voisin()` qui prend en entrée 2 cases de la grille on souhaite exprimer que ces 2 cases diffèrent d'au plus une unité en abscisse ou en ordonnée. Penser au décorateur `@staticmethod`.

c.  On passe en revue toutes les cases contiguës (8 voisins potentiels) de la casse (x,y) de la Grille et si elles sont bien dans la grille, on les accumule dans une liste. On renvoie la liste obtenue.

d.
parcourir les cellules de la grille et affecter leurs voisins avec le résultat fourni par la méthode `get_voisins` de Grille.

e. Afficher ligne par ligne les Cellules de la Grille.

f. Utiliser la méthode `random()` du module éponyme qui renvoie un nombre aléatoire entre 0 et 1 puis faire naître et basculer les cellules tirées au sort.

g. On passe en revue toutes les cellules de la Grille et on fait ce qui est demandé dans les 2 cas.

4) Programme principal 

Penser à faire dans l'ordre l'instanciation, le remplissage aléatoire, le calcul des voisinages puis jouer en marquant une pause grâce à à la méthode `sleep()` du module `time`.
