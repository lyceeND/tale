# Arbres : des exercices 


## Préliminaire : les tas binaires


Un tas binaire est un arbre binaire complet à gauche, tel que le noeud racine
porte une donnée supérieure à celles de tous les autres noeuds et tel que ses
deux sous-arbres soient aussi des tas. L'arbre est alors un tas-max.

Un tas-min a la même structure, mais la racine porte une information plus petite
que tous les autres noeuds.

La structure de tas binaire est utilisé :

- dans un algorithme de tri efficace : le tri par tas.
- dans la gestion des files de priorité



## QUIZZ


## Mesures sur les arbres 

On considère l'arbre non étiqueté suivant :


```mermaid
graph TD;
  A( )-->C( );
  A-->B( );
  C-->D( );
  C-->E( );
  C-->F( );
  B-->G( );
  G-->H( );
  G-->I( );
```

1. Quelle est sa hauteur ?

~~~
☐ a. 9 ☐ b. 3 
☐ c. 2 ☐ d. 8 
~~~

2. Quelle est sa taille ?

~~~
☐ a. 9  ☐ b. 8  ☐ c. 4
~~~

3. Quelle est le degré du noeud de plus haut degré ?

~~~
☐ a. 1 ☐ b. 2
☐ c. 3 ☐ d. 4
~~~

<!-- 4. Parmi les arbres suivants, lequel n'est **pas** équilibré ?-->

## Parcours d'un arbre binaire 

On considère l'arbre binaire suivant étiqueté par des entiers :

```mermaid
graph TD;
   9-->8;
   9-->7;
   8-->6;
   8-->2;
   2-->1;
   7-->5;
   5-->4;
   5-->3;
```



1. Dans quels ordre seront examinés les noeuds lors d'un parcours en largeur ?

~~~
☐ a. 6 1 2 8 4 3 5 7 9
☐ b. 9 8 7 6 2 5 1 4 3
☐ c. 6 8 1 2 9 7 4 5 3
☐ d. 9 8 6 2 1 7 5 4 3
~~~


2. Dans quels ordre seront examinés les noeuds lors d'un parcours préfixe ?

~~~
☐ a. 6 1 2 8 4 3 5 7 9
☐ b. 9 8 7 6 2 5 1 4 3
☐ c. 6 8 1 2 9 7 4 5 3
☐ d. 9 8 6 2 1 7 5 4 3
~~~

3. Dans quels ordre seront examinés les noeuds lors d'un parcours infixe ?

~~~
☐ a. 6 1 2 8 4 3 5 7 9
☐ b. 9 8 7 6 2 5 1 4 3
☐ c. 6 8 1 2 9 7 4 5 3
☐ d. 9 8 6 2 1 7 5 4 3
~~~

4. Dans quels ordre seront examinés les noeuds lors d'un parcours postfixe ?

~~~
☐ a. 6 1 2 8 4 3 5 7 9
☐ b. 9 8 7 6 2 5 1 4 3
☐ c. 6 8 1 2 9 7 4 5 3
☐ d. 9 8 6 2 1 7 5 4 3
~~~

## Arbres binaires et Tas 

1. Parmi les arbres binaires suivants, lequel est  un arbre binaire de recherche ?

![](IMGS/quizz_recherche.png)

~~~
☐ a. ☐ b. ☐ c.
~~~

2. Parmi les tableaux suivants, lequel représente un tas ?

![](IMGS/quizz_tas.png)

~~~
☐ a. ☐ b. ☐ c.
~~~


## Rappels sur les parcours

```mermaid
graph TD;
   T-->Y;
   Y-->P;
   T-->O;
   O-->H;
   O-->N;
```

- En *largeur d'abord* (étage par étage) :  T Y O P H N
- Dans le cas où il est parcouru en *profondeur d'abord*, on distingue trois types
  de parcours selon l'ordre dans lequel le fils gauche, le fils droit et la racine
  sont explorés :
  - Parcours *préfixe*, ou *préordre* (racine, puis fils gauche, puis fils droit) : T Y P O H N
  - Parcours *infixe*, ou *en ordre* (fils gauche, racine, fils droit) : P Y T H O N
  - Parcours *postfixe*, ou *postordre* (fils gauche, fils droit, racine) : P Y H N O T
  



## Exercices


### Reconstruction d'un arbre 

Un arbre est étiqueté avec des lettres. Un parcours préfixe de l'arbre donne : `ALORHGIMET`
Un parcours infixe donne : `OLHRAMIEGT`

Reconstruire l'arbre binaire qui a produit ces deux parcours.
Qu'obtenez-vous en faisant un parcours en largeur d'abord ? Et en faisant un
parcours postfixe ?

### Implémentation objet d'un arbre 

On suppose qu'on dispose d'une implémentation objet, telle que celle présentée
à la fiche 15.

Écrire des fonctions (et non des méthodes) qui prennent en paramètre un arbre
(ou `None` pour l'arbre vide) et calculent :

- la hauteur de l'arbre (un arbre réduit à sa racine a pour hauteur 0)
- le nombre de feuilles de l'arbre (le nombre de noeuds sans fils)
- le nombre de noeuds internes de l'arbre (le nombre de noeuds sans fils)

### Implémentation avec des tuples

On suppose qu'on dispose d'une implémentation d'un arbre par des tuples
imbriqués (fiche 15).

Écrire des fonctions qui prennent un arbre en paramètre et calculent :

- la hauteur de l'arbre (un arbre réduit à sa racine a pour hauteur 0)
- le nombre de feuilles de l'arbre (le nombre de noeuds sans fils)
- le nombre de noeuds internes de l'arbre (le nombre de noeuds sans fils)

### Parcours des feuilles 

On suppose qu'on dispose d'une implémentation objet, telle que celle présentée
à la fiche 15.

- Écrire une fonction qui prend un arbre en paramètre et indique (en renvoyant un
booléen) s'il est réduit à une feuille.
- Écrire une procédure qui affiche les étiquettes des feuilles (et uniquement
  des feuilles) d'un arbre donné.

### Parcours préfixe, postfixe et infixe 

On suppose qu'on dispose d'une implémentation d'un arbre par des tuples
imbriqués (fiche 15).

1. Écrire une fonction `est_vide` qui prend un tel arbre et renvoie un booléen
indiquant si l'arbre passé en paramètre est vide (fiche 15)

2. Écrire des fonctions réalisant les parcours préfixe, postfixe et infixe
   (fiche 14), dans le cas où l'arbre est représenté par un tuple.

### Recherche dans un arbre binaire quelconque 

En supposant qu'un arbre binaire est implémenté par des tuples imbriqués (fiche 15).

Écrire une fonction qui indique si une valeur donnée est présente dans un tel arbre
binaire quelconque.

### Arbres binaires de recherche 

On reprend l'implémentation objets de la fiche 15.

1. Écrire une fonction qui indique si un arbre binaire donné est un arbre binaire
  de recherche.

Dans les questions suivantes, vous devez tenir compte du fait que l'arbre
est un arbre binaire de recherche :

2. Écrire une fonction qui renvoie le plus petit élément d'un arbre binaire de
  recherche
3. Écrire une fonction qui renvoie le plus grand élément d'un arbre binaire de
  recherche
4. Écrire une fonction qui indique si une valeur donnée est présente dans un
   arbre binaire de recherche

### Parcours en largeur 

Le parcours en largeur consiste à parcourir l'arbre étage par étage : d'abord la
racine, puis tous les noeuds de profondeur 2, puis tous les noeuds de profondeur
3...

Ce parcours nécessite l'utilisation d'une file d'attente (fiche 12), qu'on
supposera ici à disposition.
Au départ, on place la racine dans la file, puis, tant que la file contient des
éléments, on défile un élément, on affiche son étiquette, on ajoute les deux
fils dans la file et on recommence.

Écrire un algorithme de parcours en largeur utilisant une implémentation objet
d'un arbre binaire (fiche 15).


## Codage de Huffman



Nous allons étudier un algorithme de compression basé sur les arbres binaires :
la compression de Huffman.

On appelle *alphabet* l'ensemble des symboles (caractères) composant la donnée
de départ à compresser. Dans la suite, nous utiliserons un alphabet composé seulement des 8
lettres `ABCDEFGH`.

1. On cherche à coder chaque lettre de l'alphabet par une séquence de chiffres binaires.
     a. Combien de bits sont  nécessaires pour coder chacune des 8 lettres de
        l'alphabet ?
     b. Quelle sera la longueur en octets d'un message de 1000 caractères construit sur
        cet alphabet ?

2. Proposer un code de taille fixe pour chaque caractère de l'alphabet de 8 lettres.

3. On considère maintenant le codage suivant, la longueur du code de chaque
   caractère étant variable :

| lettre | A    | B     | C     | D      | E    | F      | G      | H      |
|--------|------|-------|-------|--------|------|--------|--------|--------|
| code   | $10$ | $001$ | $000$ | $1100$ | $01$ | $1101$ | $1110$ | $1111$ |

   Ce type de code est dit *préfxe*, ce qui signifie qu'aucun code n'est le préfixe d'un autre (le
   code de `A` est `10`, et aucun code ne commence par `10`, le code de `B` est
   `001`, et aucun code ne commence par `001`). Cette propriété permet de
   séparer les caractères de manière non ambiguë.

   a) Donner le code du message : `CACHE`
   b) Quel est le message correspondant au code `001101100111001`

4. Dans un texte, chacun des 8 caractères a un nombre d'apparitions différent.
   Ceci est résumé dans le tableau suivant, construit à partir d'un texte de 1000
   caractères :

| lettre | A   | B  | C   | D  | E   | F   | G  | H  |
|--------|-----|----|-----|----|-----|-----|----|----|
| nb     | 240 | 140| 160 | 51 | 280 | 49  | 45 | 35 |

   a) En utilisant le code de taille fixe proposé à la question 2, quelle sera
   la longueur en bits du message contenant les 1000 caractères énumérés dans le tableau
   précédent ?

   b) En utilisant le code de la question 3, quelle sera la longueur du même
   message en bits ?

5. L'objectif du codage de Huffman est de trouver le codage proposé en 3 qui
   minimise la taille en nombre de bits du message codé, en se basant sur le
   nombre d'apparition de chaque caractère (un caractère qui
   apparaît souvent aura un code plutôt court).

   Pour déterminer le code optimal, on considère 8 arbres, réduits à une racine,
   contenant le symbole et son nombre d'apparitions.

   ![](IMGS/huffman01.png)

   Puis on fusionne les 2 arbres contenant 
   **les plus petits nombres d'apparitions,**
   et on affecte à ce nouvel arbre la somme des
   nombres d'apparitions de ses deux sous-arbres. 
   Lors de la fusion des deux arbres, le
   choix de mettre l'un ou l'autre à gauche n'a pas d'importance. Nous
   choisirons ici de mettre le plus fréquent à gauche (s'il y a un cas
   d'égalité, nous ferons un choix arbitraire).

   ![](IMGS/huffman02.png)

   On recommence jusqu'à ce qu'il n'y ait plus qu'un seul arbre.

   a) Combien d'étapes (combien de fusions d'arbres) seront nécessaires pour que
   l'algorithme précédent se termine ?

   b) En suivant l'algorithme précédent, construire l'arbre de Huffman.

6. Le code à affecter à chaque lettre est déterminé par sa position dans
   l'arbre. Précisément, le code d'un symbole de l'alphabet décrit le chemin
   de la racine à la feuille qui le contient : un 0 indique qu'on descent par le
   fils gauche, et un 1 indique qu'on descent par le fils droit.

   Dans le cas de cet arbre :

   ![](IMGS/huffman09.png)

   le code de `X` serait `00` (deux fois à gauche), le code de `Y` serait `01`,
   et celui de `Z` serait `1`.

   a) Sur chaque arête de l'arbre construit à la question 5, inscrire `0` ou `1`
   selon que l'arête joint un fils gauche ou un fils droit.

   b) Quel est le code de `F` ? Vérifier qu'il s'agit bien du code proposé à la
   question 3.

7. Programme à trous

Le code suivant permet à partir d'un fichier nommé `texte.txt`, de construire
l'arbre de Huffman, puis un dictionnaire, qui associe à chaque caractère du fichier
d'entrée son code sous forme d'une séquence de bits (liste de 0 et
de 1).

Compléter le code en indiquant ce qui manque dans les zones rectangulaires.


~~~python
import bisect

class ArbreHuffman:

    def __init__(self, lettre, nbocc, g=None, d=None):
        self.lettre = lettre
        self.nbocc = nbocc
        self.gauche = g
        self.droite = d

    def est_feuille(self) -> bool:
        return [XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX]

    def __lt__(self, other):
        # Un arbre A est strictement inférieur à un arbre B
        # si le nombre d'occurrences indiqué dans A est
        # strictement inférieur à celui de B
        return -self.nbocc < -other.nbocc


def parcours(arbre, chemin_en_cours, dico):
    if arbre is None:
        return
    if arbre.est_feuille():
        dico[arbre.lettre] = chemin_en_cours
    else:
        parcours(arbre.gauche, chemin_en_cours + [0], dico)
        [XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX]

def fusionne(gauche, droite) -> ArbreHuffman:
    nbocc_total = [XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX]
    return ArbreHuffman(None, nbocc_total, gauche, droite)


def compte_occurrences(texte: str) -> dict:
    """
    Renvoie un dictionnaire avec chaque caractère du texte
    comme clé, et le nombre d'apparition de ce caractère
    dans le texte en valeur

    >>> compte_occurrences("AABCECA")
    {"A": 3, "B": 1, "C": 2, "E": 1}
    """
    occ =  dict()
    for car in texte:
        if car not in occ:
            [XXXXXXXXXXXXXX]
        occ[car] = occ[car] + 1
    return [XXXXXXX]

def construit_liste_arbres(texte: str) -> list:
    """
    Renvoie une liste d'arbres de Huffman, chacun réduit
    à une feuille
    """
    dic_occurrences = compte_occurrences(texte)
    liste_arbres = []
    for lettre, occ in dic_occurrences.items():
        liste_arbres.append(ArbreHuffman(lettre, occ))
    return liste_arbres


def codage_huffman(texte: str) -> dict:
    """
    Codage de Huffman optimal à partir d'un texte

    >>> codage_huffman("AAAABBBBBCCD")
    {'A': [0, 0], 'C': [0, 1, 0], 'D': [0, 1, 1], 'B': [1]}
    """

    liste_arbres = construit_liste_arbres(texte)
    # Tri par nombre d'occurrences décroissant
    liste_arbres.sort()
    # Tant que tous les arbres n'ont pas été fusionnés
    while len(liste_arbres) > 1:
        # Les deux plus petits nombres d'occurrences
        # sont à la fin de la liste
        droite = liste_arbres.pop()
        gauche = liste_arbres.pop()
        new_arbre = fusionne(gauche, droite)
        # Le module bisect permet d'insérer le nouvel
        # arbre dans la liste, de manière à ce que la
        # list reste triée
        bisect.insort(liste_arbres, new_arbre)
    # Il ne reste plus qu'un arbre dans la liste,
    # c'est notre arbre de Huffman
    arbre_huffman = liste_arbres.pop()
    # Parcours de l'arbre pour relever les codes
    dico = {}
    parcours(arbre_huffman, [], dico)
    return dico

# Script principal
with open("texte.txt") as f:
    texte = f.read()
print(codage_huffman(texte))
~~~


## Aide

1. a) Avec 1 chiffre binaire, on obtient 2 codes, (0 et 1), avec 2 chiffres
   binaires, on obtient 4 codes (00, 01, 10 et 11). Nous avons besoin de 8
   codes 
   b) Chaque caractère a un code de même longueur et il y a 1000 caractères.
2. Il y a beaucoup de possibilités. La plus évidente est de mettre les
   caractères dans l'ordre alphabétique.
3. a) Il suffit de remplacer chaque lettre par son code proposé dans le tableau.
   b) Le code est préfixe, il ne doit pas y avoir d'ambiguïté sur la coupure de
   chaque lettre
4. a) Se souvenir que chaque lettre a un code de même longueur.  
   b) Cette fois, il faut tenir compte de la longueur du code de chaque lettre
   et du nombre d'apparitions de chaque lettre.
5. a) À chaque fusion, on a un arbre de moins. Au départ, il y a 8 arbres, et à
   la fin il n'y en a plus qu'un seul.
   b) Attention à bien sélectionner, à chaque étape, les 2 arbres dont les
   racines portent les plus petits nombres. De ces deux arbres, celui qui a le
   nombre le plus grand est placé à gauche. La racine du nouvel arbre doit
   porter la somme des nombres aux racines de ses deux fils. 
6. a) Il y a 14 arêtes à étiqueter 
   b) Suivre le chemin de la racine à la feuille `F` et relever les étiquettes
   des arêtes 
7. Bien contrôler le type de retour de chaque fonction/méthode. Il est utile de
   comprendre le sens général du code pour compléter les parties manquantes
   
