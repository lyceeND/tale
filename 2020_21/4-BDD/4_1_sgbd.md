# Systèmes de Gestion de Bases de Données


## En bref

Nous  présentons ici  les généralités  sur  les SGBD  et les  outils qui  seront
employés pour les mettre en oeuvre.


## SGBD 
Un **S**ystème de **G**estion de **B**ases de **D**onnées (SGBD ou DBMS en
anglais) regroupe des outils logiciels  permettant de gérer, manipuler, stocker,
sécuriser des informations organisées dans une base de données (cf chap 7).

Sous l'impulsion  d'informaticiens comme Charles  Bachman, des SGBD sont  mis au
point avant  même l'apparition du  modèle relationnel de  E.F. Codd en  1970. La
plupart suivent  ce modèle (MySQL,  PostgreSQL, MariaDB,...) mais il  existe des
alternatives (MongoDB, Cassandra,... de type NoSQL *Not Only SQL*).

La gestion d'une BDD est complexe du fait de sa taille, du nombre de ses
utilisateurs, de la nécessité d'assurer la cohérence, la sécurité des
informations.

Un SGBD doit être indépendant des BDD qu'il gère.


## SQL 

Le  langage   le  plus  communément   utilisé  pour  dialoguer   (effectuer  des
**requêtes**) avec le SGBD est le  SQL (*Structured Query Language* : langage de
requête structurée). C'est celui que nous allons utiliser pour créer des tables,
ajouter,  supprimer, mettre  à  jour des  données.  Il  a été  créé  en 1979  et
normalisé en  1986.Il colle le  plus possible à  de l'anglais ordinaire.  Il est
déclaratif (cf fiche 9). Nous l'étudierons plus avant dans les fiches suivantes.


## SQLite

La plupart  des SGBD  fonctionnent sur  des machines  distantes selon  le modèle
client-serveur bien  adapté à  un langage  de requêtes.   SQLite, comme  son nom
l'indique, a  un fonctionnement  plus simple et  s'apparente à  une bibliothèque
logicielle  (écrite   en  langage   C)  qui   sera  directement   intégrée  dans
l'application qui  l'utilise. Une  base de  données est  entièrement représentée
dans un  fichier. Cela peut  présenter des  inconvénients pour de  gros systèmes
mais suffira pour notre découverte. 

SQLite est de plus  sous licence publique ce qui en fait un  outil de choix pour
l'éducation et est largement utilisée notament par Mozilla, Google, Apple,...

SQLite est disponible sur tous les systèmes d'exploitation.

Il existe de plus une bibliothèque Python, `sqlite3`, qui permet de gérer la BDD
à partir de commandes écrites en Python.


## En pratique

On travaillera via le terminal, via des fichiers Python et on pourra installer `SQLiteBrowser` disponible au téléchargement pour tout
système d'exploitation pour avoir un outil graphique.

![](./IMG/sqliteBrowser.png)



