# SQL 

## En bref

Nous allons voir  les principales commandes SQL permettant  d'interagir avec une
base de  données. Nous travaillerons  avec `SQLite3`.  (Note : SQLite  n'est pas
sensible à la casse).

## Création d'une table

On  peut créer  un  fichier d'extension  `sql`  pour plus  de  confort. Il  doit
commencer  par `BEGIN  TRANSACTION` et  finit par  `COMMIT;`. Voici  par exemple
comment créer la table suivante:

```
Nom         Maths       Anglais     INFO      
----------  ----------  ----------  ----------
Joe         16          17          18        
Zoe         19          15          17        
```

On  commence par  céer la  table avec  `CREATE TABLE`  en indiquant  ici la  clé
primaire. On insère ensuite les valeurs avec 

`INSERT INTO ... VALUES ...`:

```sql
BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS `Base_notes` (
	`Nom`	TEXT PRIMARY KEY,
	`Maths`	INTEGER,
	`Anglais` INTEGER,
	`INFO` INTEGER );
INSERT INTO `Base_notes` (Nom,Maths,Anglais,INFO) VALUES
 ('Joe',14,17,18),('Zoe',19,15,17);
COMMIT;
```

On enregistre ce fichier par exemple sous le nom `notes.sql`.
On peut enregistrer une valeur à la fois:

```sql
INSERT INTO `Table_notes` VALUES ('Joe', 14, 17, 18);
```

On peut cependant vouloir insérer une valeur pour une clé déjà 
 existante  ce qui  va à  l'encontre des  contraintes d'intégrité.  On obtiendra
 alors une erreur de type `IntegrityError`.
 
 
 
## Terminal 

On rappelle  qu'avec `SQLite`,  les bases  de données sont  stockées en  un seul
fichier.  On   ouvre  un   terminal  pour   créer  une   BDD  dans   le  fichier
`les_notes.db`  à  partir   du  fichier  `notes.sql`  que   nous  venons  de
créer. (note : il faut bien diffférencier les rôles de ces deux fichiers)

```console
$ sqlite3 les_notes.db < notes.sql
```

On peut ensuite travailler avec `SQLite`:

```console
$ sqlite3 les_notes.db
sqlite> 
```


## SELECT

La  commande   `SELECT`  permet,   comme  son  nom   l'indique  en   partie,  de
**sélect**ionner  des   colonnes  d'une   ou  de   plusieurs  tables   données  en
paramètres. La syntaxe est `SELECT colonne(s) FROM nom table(s)`. Comme souvent,
l'étoile  `*` indique  que l'on  sélectionne  tout. Pour  obtenir l'en-tête  des
colonnes et un affichage plus lisible, on commence par indiquer en préambule :

```console
sqlite> .headers ON
sqlite> .mode column
```

Puis on demande toute la table `Table_notes`:

```sql
sqlite> SELECT * FROM Table_notes;
Nom         Maths       Anglais     INFO      
----------  ----------  ----------  ----------
Joe         14          17          18        
Zoe         19          15          17        
```



Si l'on ne veut que certaines colonnes :

```sql
sqlite> SELECT Maths, Nom FROM Table_notes;
Maths       Nom       
----------  ----------
14          Joe       
19          Zoe       
```

## NULL

Il se  peut que certains enregistrements  n'existent pas. Par exemple  AnnE peut
avoir été absente lors du devoir d'Anglais. On met alors `NULL`:

```sql
sqlite> INSERT INTO `Table_notes` VALUES ('AnnE',18,NULL,19);
```
