# SQL avancé

## En bref

Nous continuons à explorer les commandes SQL avec des requêtes plus élaborées.


## WHERE

`WHERE` permet de  spécifier des critères de sélection. Par  exemple pour savoir
quels sont les élèves qui ont plus de 14 en maths ou plus de 17 en Info:

```sql
sqlite> SELECT NOM FROM Table_notes WHERE Maths>14 OR INFO>18;
Zoe       
```


## Modifications

On peut modifier  une note dans le tableau précédent  avec `UPDATE`. Par exemple
pour donner 16 en maths à Joe :

```sql
sqlite> UPDATE Table_notes SET Maths = 14 WHERE Nom = 'Joe';
```

On peut supprimer  une ligne avec `DELETE`. Par exemple  pour enlever les lignes
dont les notes de maths sont inférieures à 14:

```sql
sqlite> DELETE FROM Tables_notes WHERE Maths <= 14;
```

On peut rajouter une colonne, ici la classe, avec `ALTER TABLE`:

```sql
sqlite> ALTER TABLE Table_notes ADD COLUMN Classe TEXT;
sqlite> SELECT * FROM Table_notes;
Nom         Maths       Anglais     INFO        Classe    
----------  ----------  ----------  ----------  ----------
Joe         14          17          18                    
Zoe         19          15          17                    
AnnE        18                      19                    
```


On  peut  supprimer  une  table  avec `DROP  TABLE`:

```sql
sqlite> DROP TABLE Tables_notes;
```


## Fonctions de groupes (agrégation)

Les fonctions de groupe permettent d'obtenir des informations sur un ensemble de
lignes en  travaillant sur  les colonnes et  non pas sur  les lignes  comme avec
`WHERE`. Par exemple :

* `AVG` : qui calcule la moyenne d'une colonne;
* `SUM` : calcule la somme d'une colonne;
* `MIN`, `MAX`: calcule le minimum (maximum) d'une colonne;
* `COUNT` : nombre de ligne d'une colonne ;

Comptons combien d'élèves ont plus de 15 en maths:

```sql
sqlite> SELECT COUNT(Nom) FROM Table_notes WHERE Maths > 15;
2
```

## Tri : ORDER BY

Trions les  lignes par ordre croissant  des notes de
maths et en cas d'égalité en fonction des notes d'informatique.

```sql
sqlite> SELECT Nom, Maths, INFO FROM Table_notes ORDER BY Maths, INFO ASC;
Nom         Maths       INFO      
----------  ----------  ----------
Joe         14          18        
AnnE        18          19        
Zoe         19          17   
```


## Les jointures

Une jointure permet d'associer plusieurs tables dans une même requête.
Voici un extrait d'une  table `Table_mentions` :

```sql
Note        mention   
----------  ----------
14          cool      
17          la star   
18          la folie  
19          trop ouf  
```

Nous voudrions avoir les noms et les  appréciations des notes de maths de chaque
élève:

```sql
sqlite> SELECT Nom, mention AS mention_maths FROM Table_notes AS n JOIN Table_mentions AS m ON n.Maths = m.Note;
Nom         mention_maths
----------  -------------
Joe         cool         
Zoe         trop ouf     
AnnE        la folie     
```

Si on veut créer une table à partir de cette sélection et la réutiliser, on emploie la syntaxe:

```sql
CREATE TABLE Appreciations AS SELECT Nom, Mention.... 
```

## Un exemple de faille de sécurité : injection SQL

Il est  très important  de sécuriser  ses BDD qui  peuvent contenir  des données
sensibles. Or il est courant de  demander aux utilisateurs des données (nom, mot
de passe,...)  qui seront introduites  dans la base. Un  utilisateur malveillant
pourrait alors entrer du code SQL au lieu de son nom et détruire la base ou bien
récupérer des données.

Par exemple, si l'on  demande d'entrer un pseudo sans précaution  via ce code en
Python avec la bibliothèque `bottle-sqlite` qui sera introduite en exercice :

```python
p = input('Quel est votre pseudo ?')
db.execute(f"SELECT * FROM inscrits WHERE pseudo={p}");
```

Si un utilisateur rentre le pseudo 
`\'Joe\'; DROP TABLE inscrits;` alors
la requête exécutée est :

```sql
SELECT * FROM inscrits WHERE pseudo = 'Joe'; DROP TABLE inscrits;
```

et la table est détruite !

L'idée   est   de   faire   vérifier    les   entrées   avant   de   lancer   la
requête, par exemple  à l'aide d'*expressions régulièreres* ou  en utilisant des
*paramètres* SQL comme ici :

```python
db.execute('SELECT * FROM inscrits WHERE pseudo=?', pseudo)
```

SQLite va  vérifier que le paramètre  ne contient aucun code  créant une requête
**avant** d'exécuter la requête `SELECT` (requête préparée).
