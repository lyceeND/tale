# MÉMO

Principales commandes SQL et leur syntaxe

```sql
SELECT colonne1 [AS alias1],
       colonne2 [AS alias2]
FROM table1 [AS t11], table2 [AS t2]
WHERE [critères de jointure et sélection]
GROUP BY colonnei
ORDER BY colonnej [ASC|DESC]
```

Jointure :

```sql
SELECT colonne1 [AS c1],
       colonne2 [AS c2]
FROM table1 AS t1 join table2 AS t2 ON t1.clea = t2.clea
                                   AND t1.cleb = t2.cleb
```


Insertion :

```sql
INSERT INTO table1 VALUES
(valeur1-1, sur toutes les colonnes...),
(valeur2-1, sur toutes les colonnes...),
...
```

Modifier des cellules :

```sql
UPDATE table SET col1 = val1
WHERE [sélection]
```

Supprimer des lignes :

```sql
DELETE FROM table1 WHERE [sélection]
```

Listes :

```sql
DEPT IN (44, 35, 22, 56, 29)
DEPT BETWEEN 03 AND 06
```

Filtres :

```sql
nom LIKE 'AN%' -- qui commence par AN
nom LIKE 'ANN_' -- qui commence par ANN suivi par un seul caractère
nom LIKE '%E' -- qui finit par un E
nom LIKE `%CHOC%' -- qui cntient le cloc de caractères CHOC
```



