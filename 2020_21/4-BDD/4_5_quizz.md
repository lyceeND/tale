# Se tester

## 1) SGBD (fiche 24)

■ 1) Le modèle relationnel est apparu dans les années

a) 1960
b) 1970
c) 1980
d) 1990

■ 2) Quel outil permet à un utilisateur de rechercher une information spécifique dans une base de données ?

a) Une recherche
b) Une requête
c) Une clause
d) Un filtre

■ 3) Qu'est-ce que SQL ?

a) Un langage permettant de construire des bases de données
b) Un langage permettant d'effectuer des recherches dans des bases de données
c) Un langage permettant de débuguer des bases de données
d) Un langage permettant de manipuler des fichiers Excel

■ 4) Que signifie SQL ?

a) Strong Question Language
b) Strong Query Langage
c) Selected Query Language
d) Structured Query Language

## 2) SQL (fiches 25 et 26)

■ 5) Quelle commande SQL permet d'ajouter une nouvelle ligne ?

a) INSERT
b) INSERT INTO
c) ADD
d) PUT

■ 6) Quelle commande SQL permet de trier des lignes 

a) TRY
b) SORT
c) ORDER
d) ASCENDING

■ 7) Quelle commande permet d'extraire des lignes d'une table 

a) GET 
b) OPEN 
c) EXTRACT 
d) SELECT

■ 8) Quelle commande permet d'extraire toutes les lignes de la table `Eleves` ?

a) SELECT [all] FROM Eleves
b) SLECT ALL Eleves
c) SELECT *.Eleves
d) SELECT * FROM Eleves


