# Objectif Bac - 30 minutes

On doit créer une base de données `baseHopital.db` qui contiendra les trois tables suivantes :

```mermaid
classDiagram
	class Patients{
		Int id 
		Text nom
		Text prenom
		Text genre
		Int annee_naissance
	}
	class Medecins{
		Int matricule
		Text nom_prenom
		Text specialite
		Text telephone
	}
	class Ordonnances{
		Int code
		Int id_patient
		Int matricule_medecin
		Text date_ord
		Text medicaments
	}
```
On supposera que les dates sont données sous la forme "11-05-2020" pour le 11 mai 2020 par exemple.

1. Donner les commandes SQLite permettant de créer ces tables.
2. On a oublié une colonne pour noter les codes postaux des patients. Donner la commande SQLite permettant cet ajout.
3. Mme Anne Wizeunid née en 2000 et demeurant 3, rue des Pignons Verts 12345 Avonelit doit être enregistrée comme patiente. Donner la commande SQLite correspondante.
4. Le patient numéro 100 a changé de genre et est maintenant une femme. Donner la commande SQLite modifiant en conséquence ses données.
5. Par souci d'écomomie, la direction décide de supprimer les médecins ayant spécialisés en épidémiologie. Donner la commande SQLite permettant de le faire.
6. Donner la requête permettant d'obtenir la liste des prénoms et noms des filles habitant le Finistère dont le prénom commence par une voyelle triées dans l'odre croissant des âges.
7. Donner la liste des patient(e)s ayant été examiné(e)s par un(e) psychiatre en avril 2020.
8. Que fait cette requête ?
```sql
SELECT m.nom_prenom, COUNT(m.matricule)
FROM Medecins AS m 
JOIN Ordonnances AS o
ON m.matricule = o.matricule_medecin 
WHERE o.date_ord LIKE "%12-2020" AND m.matricule LIKE '1_'
GROUP BY  m.matricule;
```
9. Donner le nombre de visites en 2020 par spécialité.
   




## Feuille de route

1. Pour l'identifiant, on peut utiliser `INTEGER PRIMARY KEY AUTOINCREMENT` comme introduit dans l'exercice 1.
On peut préciser les clés étrangères dans `Ordonnances` avec `FOREIGN KEY(clé_fille) REFERENCES Table_mere(cle_mere)`
2. cf Fiche 3 : `ALTER TABLE...ADD...`
3. Attention à la première colonne qui est l'autoincrément. Il faut mettre cette colonne à `NULL` dans le `INSERT INTO...`  
4. Il faut utiliser `UPDATE` : cf fiche 3.
5. Il faut utiliser `DELETE` : cf fiche 3.
6. On peut utiliser `substr`  et `IN...(...)` (cf exercice 2).
7. Il faut faire une jointure sur les trois tables.
8. Les *wildcards* `%` et `_` ont été introduites dans l'exercice 2 et `GROUP BY` dans l'exercice 4.
9. Cette requête réutilise la plupart des notions vues jusqu'à maintenant : `COUNT`, `GROUP BY`, `LIKE`, `JOIN`. 
