BEGIN TRANSACTION;
PRAGMA foreign_keys = ON;

-- 1

CREATE TABLE IF NOT EXISTS Patients(
    id INTEGER PRIMARY KEY AUTOINCREMENT,
    nom TEXT,
    prenom TEXT,
    genre TEXT,
    annee_naissance INTEGER
);

CREATE TABLE IF NOT EXISTS Medecins(
    matricule INTEGER PRIMARY KEY,
    nom_prenom TEXT,
    specialite TEXT,
    telephone TEXT
);

CREATE TABLE IF NOT EXISTS Ordonnances(
    code INTEGER PRIMARY KEY AUTOINCREMENT,
    id_patient INTEGER,
    matricule_medecin INTEGER,
    date_ord TEXT,
    medicaments TEXT,
    FOREIGN KEY(id_patient) REFERENCES Patients(id),
    FOREIGN KEY(matricule_medecin) REFERENCES Medecins(matricule)
);


-- 2
ALTER TABLE Patients ADD cp TEXT;

-- 3
INSERT INTO Patients VALUES (NULL, "Wizeunit", "Anne", "F", 2000, "29000");

INSERT INTO Patients VALUES (NULL, "Max", "Joe", "M", 1999, "45000");
INSERT INTO Patients VALUES (NULL, "Wizeunit", "Gee", "M", 2000, "29002");
INSERT INTO Patients VALUES (NULL, "Cuthbert", "Matthew", "M", 1959, "29001");
INSERT INTO Patients VALUES (NULL, "Cuthbert", "Elaine", "F", 1999, "29005");

INSERT INTO Medecins VALUES (10, "Joe_max", "épidémiologie", "1234567890");
INSERT INTO Medecins VALUES (11, "Joe_bill", "Psychiatrie", "0123456789");
INSERT INTO Medecins VALUES (12, "AnnE_Cuthbert", "pédiatrie", "0912345678");
INSERT INTO Medecins VALUES (13, "max_b", "épidémiologie", "1234567890");
INSERT INTO Medecins VALUES (14, "bill_c", "Psychiatrie", "0123456789");
INSERT INTO Medecins VALUES (15, "Gilbert_Blythe", "Gynécologie", "0912345678");
INSERT INTO Medecins VALUES (18, "Joe_ma", "épidémiologie", "1234567890");
INSERT INTO Medecins VALUES (24, "Joe_b", "Psychiatrie", "0123456789");
INSERT INTO Medecins VALUES (25, "AnnE_C", "pédiatrie", "0912345678");
INSERT INTO Medecins VALUES (26, "max_b", "épidémiologie", "1234567890");
INSERT INTO Medecins VALUES (27, "bill_coco", "Psychiatrie", "0123456789");
INSERT INTO Medecins VALUES (28, "Gilbert_B", "Gynécologie", "0912345678");

INSERT INTO Ordonnances VALUES (NULL, 1, 15, '01-12-2020', 'Truc');
INSERT INTO Ordonnances VALUES (NULL, 1, 11, '01-10-2020', 'Truc');
INSERT INTO Ordonnances VALUES (NULL, 1, 12, '01-12-2020', 'Truc');
INSERT INTO Ordonnances VALUES (NULL, 2, 12, '01-10-2020', 'Truc');
INSERT INTO Ordonnances VALUES (NULL, 2, 14, '21-10-2020', 'Truc');
INSERT INTO Ordonnances VALUES (NULL, 3, 14, '11-04-2020', 'Truc');
INSERT INTO Ordonnances VALUES (NULL, 1, 12, '11-10-2020', 'Truc');
INSERT INTO Ordonnances VALUES (NULL, 1, 24, '01-09-2020', 'Truc');
INSERT INTO Ordonnances VALUES (NULL, 4, 24, '21-04-2020', 'Truc');
INSERT INTO Ordonnances VALUES (NULL, 2, 25, '11-10-2020', 'Truc');
INSERT INTO Ordonnances VALUES (NULL, 5, 18, '14-12-2020', 'Truc');


-- 4
UPDATE Patients SET genre = "F" WHERE id = 2;


-- 5
DELETE FROM Medecins WHERE specialite = "épidémiologie";

-- 6, 7, 8 ,9


-- 6.
CREATE VIEW q_6 
AS
SELECT prenom, nom
FROM Patients
WHERE substr(cp,1,2) = "29" AND substr(prenom,1,1) IN ("A","E","I","O","U","Y")
ORDER BY annee_naissance DESC;

--7. 
CREATE VIEW q_7
AS
SELECT p.prenom, p.nom
FROM Patients AS p 
JOIN Ordonnances AS o
ON p.id = o.id_patient
JOIN Medecins AS m
ON o.matricule_medecin = m.matricule
WHERE o.date_ord LIKE "%04-2020" AND m.specialite LIKE '%sychiatr%';

--8.
CREATE VIEW q_8
AS
SELECT m.nom_prenom, COUNT(m.matricule)
FROM Medecins AS m 
JOIN Ordonnances AS o
ON m.matricule = o.matricule_medecin 
WHERE o.date_ord LIKE "%12-2020" AND m.matricule LIKE '1_'
GROUP BY  m.matricule;

--Nombre de consultations d'un médecin de matricule commençant par 1 en décembre 2020.


--9.
CREATE VIEW q_9
AS
SELECT m.specialite, COUNT(o.code) AS nb_consultations_2020
FROM Ordonnances AS o
JOIN Medecins AS m
ON o.matricule_medecin = m.matricule
WHERE o.date_ord LIKE '%2020' 
GROUP BY m.specialite;


COMMIT;