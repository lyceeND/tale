<!doctype html>
<html  lang="fr-FR">
   <head><meta charset="utf-8"></head>
   <body>
<h3>Centres d'examens par élèves </h3>

% titres = ["id", "Prénom", "Nom", "Centre d'examen"]

<table border = 1>
  <thead>
      %for titre in titres:
     <td>{{titre}}</td>
     %end
  </thead>
  

%for ligne in res:
    <tr>
    % for col in ligne:
        <td>{{col}}</td>
    % end
     </tr>
%end
</table>
<a href = "/">Retourner à la page d'accueil</a><br>
<a href = "/recherche">--> rechercher un élève</a>

</body>
</html>
