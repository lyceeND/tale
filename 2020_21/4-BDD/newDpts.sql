BEGIN TRANSACTION;

CREATE TABLE IF NOT EXISTS Region 
AS 
SELECT DISTINCT c.num_region,
                r.nom AS nom_region,
                c.ville AS capitale 
FROM Capitales AS c 
     JOIN 
     Regions AS r 
     ON r.num_region = c.num_region;

CREATE TABLE IF NOT EXISTS Departement
AS
SELECT d.num_departement,
       d.nom,
       r.num_region
FROM Departements AS d 
     JOIN
     Regions AS r
     ON
     d.num_old_region = r.num_old_region;

COMMIT;