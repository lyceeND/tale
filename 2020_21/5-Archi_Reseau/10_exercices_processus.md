# S'entraîner


## Interblocage (fiche 29)

Sept processus Pi sont dans la situation suivante par rapport aux ressources Ri :

* P1 a obtenu R1 et demande R2
* P2 demande R3 et n'a obtenu aucune ressource tout comme P3 qui demande R2
* P4 a obtenu R2 et R4 et demande R3
* P5 a obtenu R3  et demande R5
* P6 a obtenu R6 et demande R2
* P7 a obtenu R5 et demande R2

On voudrait savoir s'il y a interblocage. Dans cette situation où apparaisssent de nombreux processus, on pourra s'aider d'un graphe orienté où les sommets sont les processus et les ressources :

* La présence de l'arc Ri -> Pj signifie que le processus Pj a obtenu la ressource Ri 
* La présence de l'arc Pj -> Ri signifie que le processus Pj demande la ressource Ri




<!--
```mermaid
graph LR;
R4[R4]->P4((P4))->R3->P5((P5))->R5->P7((P7))->R2->P4;
R1->P1((P1))->R2;
R6->P6((P6))->R2;
P2((P2))->R3;
P3((P3))->R2;
R2->P4;
```
-->

## Multi-thread

Voici deux fonctions Python :

```python
# date-serveur.py
import socket
import datetime
BUFSIZE = 1024

def serveur(port):
	sock = socket.socket(type=socket.SOCK_DGRAM)
	sock.bind(("0.0.0.0", port))
	
	while True:
		data , addr = sock.recvfrom(BUFSIZE)
		data = datetime.datetime.now()
		data = "%s\n" % data
		sock.sendto(data.encode(), addr)


if __name__ == '__main__':
	serveur(5555)
```

et 

```python
# date-client.py
import socket
import datetime
import sys
BUFSIZE = 1024

def client(host, port):
	sock = socket.socket(type=socket.SOCK_DGRAM)
	addr = (host, port)
	sock.sendto(b"", addr)
	data = sock.recv(BUFSIZE)
	print(data.decode(), end="")


if __name__ == '__main__':
	if len(sys.argv) ==  2:
		host = sys.argv[1]
	else:
		host = '127.0.0.1'
	client(host, 5555)
```


1. Expliquer les rôles respectifs de ces deux programmes en consultant notamment la documentation de `socket`: https://docs.python.org/3/library/socket.html

a. Quelle est la taille du Buffer choisi pour les échanges ?

b. Quelles sont les 2 types principaux de socket et à quoi correspondent-ils ?

c. A quoi correspond la boucle `while True`{.python} du serveur ?

d. Que fait la méthode `recvfrom` de socket ?

e. Qu'envoie finalement le serveur lorsqu'il reçoit une connexion ?

f. Expliquer brièvement le code du client. Le client envoie des données vides et son adresse et reçoit en retour la date du serveur qu'il affiche avant de la décoder car tous les échanges se font en bytes

2.
a. En utilisant la classe `Thread`, comment rendre ce code multi-thread côté serveur ?

b. Ajoutez le lancement de plusieurs Threads clients nommés A, B, C, D dans le `__main___`{.python} du client


## Lancement de Processus 

Observez et commentez le code suivant.
Précisez en particulier le nombre de Processus lancés, ce qu'ils affichent et les rôles des méthodes `os.getppid` et `os.getpid()`.

```python
import multiprocessing
from multiprocessing import Process
import os

def info(title):
    print(title)
    print('Processus : ', multiprocessing.current_process().name)
    print('parent process:', os.getppid())
    print('process id:', os.getpid())
    print('*' *60)

def f(name):
    info('fonction f')
    print('Bonjour', name)

if __name__ == '__main__':
    info('Bonjour depuis le main !')
    p1 = Process(target=f, name='Jean-Claude P',args=('Jean-Claude',))
    p2 = Process(target=f, name='Bernadette P', args=('Bernadette',))
    p1.start()
    p2.start()
    p1.join()
    p2.join()
```


## Accès aux ressources

1. Explorer la bibliothèque `multiplrocessing` de Python : `https://docs.python.org/fr/3/library/multiprocessing.html`
 Chercher dans la documentation la fonction permettant d'afficher le nombre de coeurs disponibles. 

2. Lancer quelques processus en parallèle en utilisant un `Pool` du module `multiprocessing`




## Utilisation d'un verrou

Observez le code suivant :

```python
from multiprocessing import Process, Lock
import time

def f(l, i):
    l.acquire()
    try:
        print('hello world', i)
        time.sleep(1)
        print('Bonjour le Monde', i)
        print('*' * 60)
    finally:
        l.release()


if __name__ == '__main__':
    lock = Lock()
    for num in range(10):
        Process(target=f, args=(lock, num)).start()
```

1. a) Que représente la classe `Lock` ?

   b) A quoi servent les méthodes `acquire` et `release` de la class `Lock` ?

   c) Quel est l'affichage produit par l'exécution de ce programme ?
      Vous semble-t-il cohérent ?

2. 
a) Refaire une version du même code gardant la même structure mais en enlevant l'utilisation du verrou.

b) Qu'observez-vous cette fois ?

c) Expliquez ce comportement



## Stocker et compter les processus d'un utilisateur

Ecrire un script bash `proc.sh` qui  :

- teste s'il y a un seul parametre en entree
- verifie si ce parametre est un nom d'utilisateur
- cree un fichier de meme nom que l'utilisateur contenant la liste de ses  processus actif
- afffiche le nombre de processus actifs correspondants s'il y en a 
- traite les cas d'erreurs éventuels


## Traitements parallèles sur des images

1. Observer et commenter le code suivant qui utilise des filtres sur des images comme dans le projet NSI 1ère fiche 45. Notez bien que les tableaux `numpy` manipulés ont comme taille hauteur x largeur x 3 avec la dernière colonne représentant les 3 couleurs R, G et B.  

```python
def decoupe_quadrants(img):
    imgnp = np.array(img) # Transformation de l'image en tableau numpy
    h,w,_ = np.shape(imgnp)
    im_inv = inverse(imgnp)
    assert h % 2 == 0, "{} lignes pas divisble par 2".format(h)
    assert w % 2 == 0, "{} colonnes pas divisble par 2".format(w)
    un = imgnp[0:h//2 , 0:w//2,:]
    deux = imgnp[0:h//2 , w//2:w,:]
    trois = imgnp[h//2:h , 0:w//2,:]
    quatre = imgnp[h//2:h , w//2:w,:]
    return [un,deux,trois,quatre]

def reconstitue_image(quadrants):
    paq1 = np.concatenate((quadrants[0],quadrants[1]), axis=1)
    paq2 = np.concatenate((quadrants[2],quadrants[3]), axis=1)
    image_reconstituee = np.concatenate((paq1, paq2), axis=0)
    return image_reconstituee

def inverse_boucle(im):
    im2 = im.copy()
    # Double boucle pour parcourir tous les pixels
    for i in range(im.shape[0]):
      for j in range(im.shape[1]):
        im2[i, j] = 255 - im[i, j] 
    return im2

def filtre_parallel(img, filtre):
    pool = Pool(2)
    quadrants = decoupe_quadrants(img)
    return reconstitue_image(np.array(pool.map(filtre, quadrants)))

def inv_boucle_paralle(img):
    return filtre_parallel(img,inverse_boucle)

def inv_directe(img):
    img_inv = np.array(img)
    return inverse_boucle(img_inv)

if __name__ == '__main__':
    for filtre in inv_directe, inv_boucle_paralle:
        img = Image.open("ada.jpg")
        debut = time.time()
        neg = filtre(img)
        nom = "ada-{}.png".format(filtre.__name__)
        Image.fromarray(neg).save(nom)
        temps = time.time() - debut
        print(filtre.__name__, temps)
```

2. Est-ce que le traitement en parallèle fait gagner du temps ? Qu'observez-vous si on augmente la taille du `Pool` ?

3. En fait, on peut aussi effectuer avec numpy des opérations matricielles s'appliquant à tout le tabelau représentant l'image. Pour l'image en négatif, on aurait pu directement faire une opération matricielle unique  :

```python
def inverse(t):
    return 255 - t
```

Quelles sont les performances comparées de cette méthode ?
