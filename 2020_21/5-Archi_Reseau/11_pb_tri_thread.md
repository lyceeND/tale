# Objectif Bac : Tri Fusion en parallèle


On souhaite reprendre l'algorithme du Tri Fusion vu au Chapitre 2, fiche 6 et tirer parti de la méthode diviser pour régner qui a été employée pour effectuer ce travail en parallèle.


1. Rappelez l'algorithme du Tri Fusion avec les 2 fonctions `ìnterclassement` et `fusion` vues fiche 6.

2. Comment envisager la parallélisation de cet Algorithme ?

3. On souhaite maintenant effectuer cette parallélisation en prenant comme nombre de Processus deux fois le nombre de coeurs disponibles sur la plateforme et où l'on partitionne les données à trier équitablement en "paquets".

a. Comment calculer le nombre de processus ?

b. Si on appelle `data` le tableau des entiers à trier, comment calculer la taille `np` des paquets ?

c. Exprimer le tableau `tdata` contenant l'ensemble des paquets à trier (sous forme de tableaux)  

d. Lancer les tris fusion en parallèle sur tous les paquets avec un `Pool` du module `multiprocessing` et stocker le résultat dans une variable `pdata`.

e. Pour achever le tri, tant que `pdata` n'est pas vide, on veut fusionner 2 à 2 les tableaux en leur appliquant `interclassement`, jusqu'à obtention d'un résultat unique. Pour cela, utiliser la fonction `starmap` sur l'objet `Pool`. Dire quelle est la différence avec la fonction `map` en consultant la documentation Python.

4. Puis on lance le tout dans un main :

 ```python
import random
import time
if __name__ == "__main__":
    size = 2**20 
    a_trier = [random.randint(0, size//2) for _ in range(size)]
    for tri in fusion, tri_fusion_parallele, sorted:
        debut = time.time()
        res = tri(a_trier)
        temps = time.time() - debut
        print(tri.__name__, temps)
        assert res == sorted(a_trier)
```

Expliquer ce que fait ce `main`.

5. Tester et commenter les performaces respectives des différentes fonctions.


## Feuille de route

### 1) Connaître le tri fusion
 Relire la fiche 6 

### 2) Déterminer si un programme peut être parallélisé

 Paralléliser l'algorithme en pensant au principe de diviser pour régner. en partitionnant les données à trier dans un premier temps

### 3) a. Savoir trouver une fonction dans la documentation d'un module

Trouver une fonction dans le module `multiprocessing` qui renvoie le nombre de coeurs disponibles puis multiplier le résultat par 2 pour obtenir le nombre de processus qu'on veut lancer

### b. Savoir calculer la taille de sous-problèmes

 Diviser la taille de `data` par le nombre de processus pour obtenir la taille `np` des paquets

### c.  Savoir créer une liste par compréhension

Utiliser une compréhension où l'indice varie entre 0 et  le nombre de processus pour obtenir le  tableau `tdata` (sous la forme d'un tableau de tableaux)

### d. Savoir créer un `Pool` du module `multiprocessing`

 Utiliser un `Pool` et la fonction `map` pour paralélliser les calculs sur tous les sous-tableaux  et stocker le résultat `pdata`.

### e. Comprendre des fonctions d'un module

 Utiliser et Expliquer le code suivant :

```python
    while len(pdata) > 1:
        extra = pdata.pop() if len(pdata) % 2 == 1 else None
        paires = [ (pdata[i], pdata[i + 1]) for i in range(0, len(pdata), 2) ]
        pdata = pool.starmap(interclassement, paires) + ([extra] if extra else [])
```

expliquer en particulier quand la boucle va s'arrêter, que contient le tableau paires, quel est le rôle de la variable `extra` si le nombre de partitions restantes est impair.
La fonction `starmap` autorise l'utilisation d'arguments itérables (de type liste par exemple) contrairement à la fonction `map` qui n'autorise que des arguments "simples".

### 4) Conmprendre et expliquer un code fourni

 Expliquer ligne à ligne le `main` fourni en particulier comment est construit le tableau `a_trier` et le rôle de la boucle `for` 

### 5) Savoir mesurer expérimentalement les performances de fonctions

 Observer Les performances respectives des différentes fonctions de tri puis essayer de les expliquer.
