# Routage

## En bref
Comment transitent les  informations d'un point à l'autre d'un  réseau ? Comment
une machine  sait sur quel  chemin envoyer son information  ? C'est le  rôle des
protocales de routage que nous allons introduire dans cette fiche.


## Qu'est-ce que le routage

Un routeur peut être vu comme un  ordinateur ayant 2 (au moins) cartes réseau et
donc 2 dresses IP  (une par carte). Voici un réseau qui  nous servira de support
dans le cours et certains exercices:


![reseau1](./IMG/reseau1.png)


Chaque routeur  reçoit des données et  doit décider à qui  les transmettre. Pour
cela il dispose de tables de routage construites statiquement (par un humain) ou
dynamiquement (par un programme). Par exemple, comment le Routeur 2 peut-il
savoir  comment accéder  à internet  ? Soit  un administrateur  a mis  le chemin
statiquement dans les tables, soit le Routeur 3 va lui dire de venir le voir.

## Utilisation des tables de routage


Une trame arrive en  couche 1 (cf fiche 1 sur les réseaux),  est traduite logiquement dans
la couche 2 sous la forme :

| MAC dest | MAC source | ... | IP src | IP dest | Data | ...  |
|:--- |:---|:---|:---:|:---|:---|:---:|

Un routeur  lit l'adresse MAC  du destinatatire. Si ce  n'est pas la  sienne, il
rejette la trame, sinon,  il envoie le paquet IP à la couche  3. Le protocole IP
lit  l'adresse IP  du destinataire.  Si  elle est  dans son  réseau local,  il
l'envoie  à la  machine concernée,  sinon,  il regarde  dans sa  table à  quel
nouveau routeur il va l'adresser (passerelle - *gateway* en anglais).


Voici une table possible et simplifiée du routeur 1:

```console
Destination        Passerelle (Gateway)
192.168.0.0/25     192.168.0.1
192.168.1.0/25     192.168.1.1
0.0.0.0/0          192.168.1.3
100.10.42.0/25     192.168.1.2
```

Les deux premières lignes indiquent les passerelles vers les deux réseaux directement attachés au routeur.
La troisième ligne correspond au chemin par défaut vers internet : il doit se diriger vers le Routeur 3.
La dernière ligne correspond au réseau qui n'est pas accessible par les passerelles précédentes.




Dans notre réseau-exemple, voici le chemin proposé pour aller de la machine `192.168.0.102` vers la machine `100.10.42.100` en utilisant la commande `traceroute` (voir le fichier reseauPB1.fls que vous pourrez ouvrir après avoir installé le logiciel [`Filius`](https://www.lernsoftware-filius.de/Herunterladen):

```console
$ traceroute 100.10.42.100
Établissement de la connexion avec 100.10.42.100 (en 20 sauts max.).
  1     192.168.0.1
  2     192.168.1.2
  3     100.10.42.100
  
100.10.42.100 a été atteint en 3 sauts.
```

# Algorithmes de routage dynamique

## En bref

Il est impossible  au niveau d'internet ou même de  gros réseaux d'entreprise de
définir statiquement les  tables de routages de milliers voire  de milliards de
machines.  Des algorithmes  pour  automatiser cette  tâche  sont employés.  Nous
allons en découvrir deux d'entre eux dans le cas de réseaux de taille moyenne.



## Algorithmes de routage dynamique

Dans des  réseaux plus complexes (par  exemple internet !), il  est difficile de
maintenir voire de mettre en œuvre  un routage statique. Internet est lui-même
divisé en Systèmes Autonomes (AS *Autonomous  Systems*) à travers le globe. Il y
en avait 36 000 en 2011, 96 000 en mai 2020 (27 629 aux USA, 4 en Mauritanie...).

Les routeurs  mettent à jour  leurs tables en  fonction des tables  des routeurs
voisins. Il existe  deux grands groupes d'algorithmes au  programme de Terminale
parmi les routeurs appartenant à un même AS.

### Algorithmes à vecteur de distance RIP (*Routing Information Protocol*)

C'est historiquement  le premier  algorithme de routage.  L'idée est  que chaque
routeur a  chaque destination possible associée  à la plus courte  distance - en
terme de saut (*hop*) i.e. en nombre de routeurs traversés pour aller au réseau.


Chaque routeur a d'abord dans sa  table les réseaux directement accessibles sans
passer par un autre routeur (donc à une distance 0).

Ensuite, périodiquement (toutes  les 30s), chaque machine  *écoute* les annonces
des passereles  (*Gateaway* qui  permettent de sortir  du réseau  local) donnant
leurs tables :  elles mettent à jour  ainsi leurs propres tables  si des chemins
plus courts sont rendus possibles ou si de nouvelles destinations apparaissent :
les distances sont mises  à jour ainsi que le nom du  premier routeur qu'il faut
joindre pour accéder au réseau (distance + direction = vecteur...).

Si un  réseau n'apparaît plus  dans les annonces au  bout d'un certain  temps (3
minutes), il est supprimé des tables.

Les trois caractéristiques principales qui le distingueront de OSPF sont :

* la distance est mesurée en nombre de sauts;
* chaque routeur  n'a de renseignement que  sur ses voisins (en terme  de saut :
  *next hop*) donc n'a pas de vision globale du réseau (on parle de *routing by rumor*);
* il y  a une distance maximum  permise de 15 sauts (16  représente l'infini) et
  les tables ont 25 entrées au maximum.

Cet algorithme ne  peut s'appliquer qu'à de  petits réseaux et est  ouvert à des
attaques.

### Algorithmes à état de liaison OSPF (*Open Shortest Path First*)

OSPF a été mis au point pour pallier aux problèmes de RIP et son fonctionnement
est plus efficace mais plus compliqué.

Retenons seulement quelques grands principes:

*  les  routeurs ont  une  "vision"  globale du  réseau  car  ils reçoivent  des
  informations   de  tout   le   réseau  (mais   de   manière  intelligente   et
  efficace). Tous les routeurs ont donc une connaissance identique du réseau.
* les distances sont maintenant mesurées de  manière plus fine : on tient compte
  du nombre de sauts mais aussi du débit de chaque "câble" reliant deux routeurs
  par  exemple  (en  général  c'est  le rapport  entre  une  bande  passante  de
  référence  divisée par la bande passante du câble dans la
  même unité).  Les **débits binaires** sont souvent donnés en Kbps ou Kb/s 
  (kilobits par seconde) ou Mb/s (Megabit par seconde).
  
Chaque  réseau peut  être  schématisé  par un  graphe  (vision *topologique*  du
réseau). Dans notre exemple de la  fiche précédente, les routeurs et les switchs
sont les sommets, leurs liaisons sont les arêtes, les étiquettes des arêtes sont
les coûts.

On applique alors l'algorithme de Dijkstra (...que nous verrons qu 3e trimestre) pour  obtenir les routes les  moins coûteuses et sans cycle. Chaque  routeur devient alors la racine d'un  arbre qui contient les meilleures routes.



