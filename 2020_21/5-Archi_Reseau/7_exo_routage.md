# Exercices

## Construire des tables de routage 

### Statiquement

1. Reprendre le réseau proposé fiche 6 et donner la table de routage des routeurs 2 et 3 sur le modèle de la table du routeur 1 donnée dans la fiche de cours.

### Dynamiquement


2. Donner la table de routage de tous les  routeurs selon le protocole RIP. 
   On va simplifier la représentation du réseau.
   Voici par exemple la situation avant que les routeurs n'écoutent leurs voisins: 
   ![réseau départ](./IMG/RIP1.jpg)
 

3. On complique légèrement le réseau en rajoutant des connexions :

![reseau2](./IMG/reseau2.png)


On utilise cette fois le réseau OSPF. On dispose des données suivantes sur les débits:
- À l'intérieur du réseau 192.168.0.0/25 : 1,544 Mb/s
- Liaison Routeur 1 -> Switch 1 : 100 Mb/s
- Laison Routeur 1 -> Routeur 2 : 2,048 Mb/s
- Liaison Switch 1 -> Routeur 2 : 2,048 Mb/s
- Liaison Switch 1 -> Routeur 3 : 100 Mb/s
- Liaison Routeur 2 -> Routeur 3 : 1,544 Mb/s
- Liaison Routeur 2 -> Routeur 4 : 100 Mb/s
- Liaison Routeur 2 -> Switch 2 : 1,544 Mb/s
- Liaison Switch 3 -> Routeurs 3 et 4 : 10 Mb/s
- Liaison Routeur 4 -> Serveur Web : 100 Mb/s

Le débit de référence sera de 100 Mb/s

Déterminer la route pour aller de la machine 192.168.0.101 à internet selon le protocole OSPF. On commencera par représenter le réseau par un graphe dont les sommets sont les routeurs et les switchs, dont les arêtes sont les liaisons et dont les étiquettes sont les distances (rapport entre le débit de référence et leurs débits binaires). (Appliquez ensuite l'algorithme de Dijkstra : quand on l'aura vu au "e trimestre...).

## Comprendre les cycles et l'infini dans le cas du RIP 

Plaçons nous dans le cas de 4 réseaux et 3 routeurs placés en ligne et utilisant le protocole
RIP.

```mermaid
graph LR;
r1---|A|r2---|B|r3---|C|r4;
```

1. Donner les tables de routage initiales **simplifiées** des trois routeurs A, B et C. Vous ferez figurer trois colonnes : le nom de réseau atteignable, le nombre de sauts, le routeur voisin vers lequel se diriger. Par exemple, la table intiale du routeur A sera:

|Réseau|Distance|Passerelle|
|:---: |:---:   |:----:    |
|r1    | 0      | A        |
|r2    | 0      | A        |
|r3    |infini  |/         |
|r4    |infini  |/         |

2. Donner les tables successives après chaque période de 30s jusqu'à ce que les 4 réseaux apparaissent pour la première fois dans les tables des trois routeurs avec des distances finies.
3. Le routeur C tombe en panne. Expliquer l'anomalie constatée et comment le fait que la distance de 16 sauts soit l'infini permet de résoudre ce problème. Au bout de combien de temps le réseau `r4` est-il considéré comme inaccessible pour les routeurs A et B ?


