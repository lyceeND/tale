# Processus et Ressources

**Un programme est statique. Son exécution change l'état du processeur et de la mémoire. Après avoir appris comment écrire un programme, il s'agit de comprendre comment la machine organise son traitement alors que bien d'autres programmes sont aussi en cours d'exécution.**




## Notion de processus

Il faut distinguer un programme de son exécution : le lancement d'un programme entraîne des lecture/écriture de registres et d'une partie de la mémoire (cf fiches 26 et 27 1ère). D'ailleurs, un même programme peut être exécuté plusieurs fois sur une même machine au même moment mais occupera des espaces mémoire différents. Un processus représente une instance d'exécution d'un programme dans une machine donnée


## États d'un processus

- Un processus est créé et se trouve alors dans l'état  *prêt*: il attend de pouvoir avoir accès au processeur.
- Le processus obtient l'accès au processeur. Il passe alors dans l'état *élu*.
- Alors qu'il est élu, le processus peut avoir besoin d'attendre une ressource quelconque comme par exemple une ressource en mémoire. Il doit alors quitter momentanément le processeur pour que celui-ci puisse être utilisé à d'autres tâches (le processeur ne doit pas attendre !). Le processus passe donc dans l'état *bloqué*.
- Le processus a obtenu la ressource attendue mais s'est fait prendre sa place dans le processeur par un autre processus. Il se met donc en attente et repasse à l'état *pret*.
- Un processus ne pourra terminer que s'il est déjà dans l'état *élu* sauf anomalie. 

![procs](./IMGS/procs.png)

<!--
```mermaid
graph LR;
subgraph Attend le processeur
P{Prêt}
end
subgraph Attend des ressources
B{Bloqué}
end
subgraph En exécution
E{Élu}
end
P--Élection ->E;
E--Blocage ->B;
B--Déblocage ->P;

```
-->

## Commandes Linux

### `ps`

Pour connaître les processus appartenant à l'utilisateur `moi` :

```console
$ ps -lu moi

F S   UID   PID  PPID  C PRI  NI ADDR SZ WCHAN  TTY          TIME CMD
0 S  1000  9412  3048  0  80   0 -  1156 wait   ?        00:00:00 sh
0 S  1000  9413  9412  0  80   0 - 178754 poll_s ?       00:00:07 tilda
0 S  1000  9423  9413  0  80   0 -  7537 wait   pts/2    00:00:00 bash
4 R  1000 28867  9423  0  80   0 - 11140 -      pts/2    00:00:00 ps
```

* La commande `ps` (penser à *ProcessuS*) permet d'afficher des informations sur le processus. L'option `u` permet de préciser le propriétaire, l'option `l` permet un affichage avec plus d'attributs (*Long*);
* La colonne `S` indique l'état (*State*) du processus : `S` pour *stopped*, `R` pour *Running* et `Z` pour *Zombie*;
* `PID` est le *Processus IDentifier* : un identifiant sous forme d'entier donné par le système;
* `PPID` est le *Parent Processus IDentifier* qui donne l'identifiant du parent qui a engendré le processus.
* `CMD` est le nom de la commande.

### `kill`

Il n'est parfois pas possible de fermer un processus graphique en cliquant par exemple sur la croix prévue pourtant à cet effet.  On peut utiliser la commande `kill` si on connaît le PID du processus à *tuer* qui peut être obtenu avec l'option `C` de `ps`:

```console
$ ps -lC codium
F S   UID   PID  PPID  C PRI  NI ADDR SZ WCHAN  TTY          TIME CMD
0 S  1000  3091     1  3  80   0 - 218751 poll_s ?       00:00:01 codium
0 S  1000  3093  3091  0  80   0 - 94034 poll_s ?        00:00:00 codium
0 S  1000  3116  3091  2  80   0 - 134596 poll_s ?       00:00:01 codium
0 S  1000  3129  3091  0  80   0 - 111394 futex_ ?       00:00:00 codium
0 S  1000  3138  3091 11  80   0 - 276851 futex_ ?       00:00:05 codium
0 S  1000  3162  3138  3  80   0 - 173887 ep_pol ?       00:00:01 codium
0 S  1000  3192  3091  1  80   0 - 182402 futex_ ?       00:00:00 codium
```

On découvre une "généalogie" des processus. `3091` est le parent de tous les autres. C'est lui qu'il faut interrompre:

```console
$ kill 3091
$ ps -lC codium
F S   UID   PID  PPID  C PRI  NI ADDR SZ WCHAN  TTY          TIME CMD
```