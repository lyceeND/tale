# Notions d'interblocage
 
**En bref : le système dispose d'un ordonnanceur qui permet de gérer les accès concurrents aux ressources et prévenir certains problèmes évoqués dans cette fiche.**


## Ordonnancement

Plusieurs processus peuvent être dans l'état *Prêt* : comment choisir celui qui sera *élu* ? L'**ordonnanceur** (*scheduler*) classe les processus prêts dans une file, le **répartiteur** (*dispatcher*) alloue lui un processeur à l'élu dans le cas d'une architecture multiprocesseur.
Il existe plusieurs *politiques d'ordonnancement* dont le choix va dépendre des objectifs du système. Voici quelques exemples:

* *Premier arrivé, premier servi* : simple mais peu adapté à la plupart des situations.
* *Plus court d'abord* : très efficace mais il est la plupart du temps impossible de connaître à l'avance le temps d'exécution d'un processus;
* *Priorité* : le système alloue un niveau de priorité aux processus (`SCHED_FIFO` sur Linux). Cependant des processus de faible priorité peuvent ne jamais être élus;
* *Tourniquet* : un *quantum* de temps est alloué à chaque processus (`SCHED_RR` sous Linux). Si le processus n'est pas terminé au bout de ce temps, il est mis en bout de file en état *prêt*. 
* Un système hybride entre *Tourniquet* et *Priorité* qu'on retrouve dans les systèmes Unix.

## Problème de synchronisation

Considérons un programme de jeu contenant une variable globale `nb_pions` et un programme prenant un pion tant qu'il en reste. Imaginons deux joueurs utilisant ce programme. Leur exécution se traduit par la création de deux processus `P1` et `P2`. Supposons qu'il ne reste qu'un pion. `P1` est élu, lance la prise d'un pion mais est interrompu par l'ordonnanceur pour élire `P2` avant de décrémenter `nb_pions`. `P2` a le temps lui de décŕementer `nb_pions`. Quand `P1` est élu à nouveau, il reprend où il en était et ne vérifie pas s'il reste un pion et ne voit pas d'anomalie.  

Un système de *sémaphores* (qui ne sera pas détaillé ici) est le plus souvent mis en place pour éviter ce problème.

## Problème d'interblocage

<!--
```mermaid
graph TB;
P1((P1))->R1[R1]->P2((P2))->R2[R2]->P1;
P1-.P1 commuté car demande R2.->P2;
P2-.P2 commuté car demande R1.->P1;
subgraph R1 détenue par P1
R1
end
subgraph R2 détenue par P2
R2
end
```
-->

![interB](./IMGS/interB.png)



Imaginons un processus `P1` demandant et obtenant une ressource `R1` puis commuté par l'ordonnanceur. `P2` est élu et demande et obtient `R2` puis demande `R1` mais ne peut l'obtenir car détenue par `P1`. `P2` est donc commuté et `P1` est élu et demande `R2`. Il est à son tour commuté et `P2` est élu car `R2` est détenue par `P2`. Les processus sont alors bloqués dans l'attente l'un de l'autre dans un cycle sans fin.

On parle dans ce cas d'**interblocage**. Des solutions (détection/guérison ou prévention) sont mises en place qui ne seront pas étudiées ici.


## Threads - Processus légers

Un *thread* ( signifie originalement *fil* en anglais) désigne ce qui est appelé en français **processus léger**. Plutôt que de se voir allouer une partie de la mémoire, les processus légers forment des groupes qui vont se partager un même espace mémoire. Cela permet par exemple de faciliter le parallélisme sur des machines multi-processeurs. En contrepartie, les données étant partagées dans le même espace mémoire, cela impose une plus grande surveillance pour la synchronisation. (cf exercice ...)
