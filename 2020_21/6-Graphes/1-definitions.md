# Généralités et vocabulaire

En bref :

> Un graphe est une structure de données à la fois simple 
> et riche dans ses applications.
> Des sommets sont liés entre eux par des arcs ou des arêtes.
> Nous présentons ici le vocabulaire de base des graphes.

## Exemples de graphe et premières définitions

![](IMGS/image_reseau_internet.jpeg)

![](IMGS/img_reseau_social.png)

![](IMGS/img_reseau_routier.png)

On appelle graphe la donnée d'un ensemble fini V de points (ou sommets ou vertices en anglais) et d'un ensemble E de liens entre ces points.

Les 3 images au-dessus correspondent à des graphes, le premier représenant schématiquement le réseau internet ; le second un réseau social où des personnes suivent d'autres personnes, et le troisième un réseau routier.

Ces liens sont le choix d'un sommet $s_1$ de départ et d'un sommet $s_2$ d'arrivée. Lorsque les liens sont symétriques, c'est-à-dire
lorsque l'existence d'un lien de $s_1$ vers $s_2$ implique l'existence d'un lien de $s_2$ vers $s_1$ le graphe est dit non orienté (et les liens
sont représentés par de simples lignes), c'est le cas du premier exemple. Dans le cas contraire, on parle de graphe orienté (et les liens sont représentés par des flèches), c'est le cas du second exemple.

Des poids peuvent être associés aux liens d'un graphe (orienté ou non), par exemple pour représenter la distance, le temps ou le coût nécessaires
pour passer d'un état à un autre. On parle alors de graphe pondéré. C'est le cas du troisième exemple.

On appelle ordre d'un graphe son nombre de sommets.

## Vocabulaire des graphes non orientés

Les liens d'un graphe non orienté s'appellent des arêtes (edge en anglais).

![](IMGS/img_quizz2.png)

Une chaîne est une suite (finie) consécutive d'arêtes sur un graphe non orienté. Par exemple 0,3,1,2 est une chaîne sur ce graphe.

Lorsqu'un graphe non orienté est "un seul morceau", c'est-à-dire lorsque il existe pour tous sommets $s_1$ et $s_2$, une chaîne les reliant, le 
graphe est dit connexe. C'est le cas de ce graphe, mais par exemple pas du réseau routier mondial.

Lorsqu'il une chaîne mène d'un sommet $`s`$ à lui-même, on parle de cycle. Par exemple le chamin 0,4,1,2,0 est un cycle.

Les arbres, vus précédemment, sont des graphes connexes où il n'existe pas de cycle.

## Vocabulaire des graphes orientés

Les liens d'un graphe orienté s'appellent des arcs.

![](IMGS/img_graphe3.png)

Un chemin est une suite (finie) consécutive d'arcs sur un graphe orienté. Par exemple 0,2,3 est un chemin sur ce graphe.

Un graphe orienté est dit connexe si le graphe non orienté associés, obtenu en transformant les arcs de ce graphe en arêtes, est connexe. Le graphe présenté plus haut est connexe.

Lorsque il existe pour tous sommets $`s_1`$ et $`s_2`$, un chemin les reliant, le graphe est dit fortement connexe. Le graphe présenté plus haut n'est pas fortement connexe car il n'existe pas de chemin menant de 3 à 2.







# Représentations informatique des graphes

En bref :

> Plusieurs modes de représentations peuvent être choisies pour stocker
> des graphes : matrices d'adjacence, listes des voisins ; des successeurs
> ou des prédécesseurs.
> Nous allons étudier des implémentations de ces représentations et le passage
> d'un mode de représentation à un autre.

## Matrice d'adjacence

Une matrice est un tableau de nombres. De manière la plus simple, elle peut être 
représentée en machine par une liste de listes. Si on note `mat` une matrice, l'élément
qui est en $i$-ème ligne et en $j$-ème colonne est accessible en Python
à l'aide de `mat[i][j]`.

Un graphe peut être représenté par une matrice d'adjacence : si il y a un lien depuis le 
sommet $i$ vers le sommet $j$, on pose `mat[i][j] = 1` et si il n'y en a pas on pose `mat[i][j] = 0`.

Par exemple les graphes suivants sont représentés par les matrices suivantes :

![](IMGS/img_fiche2.png)

$`M_1 = \begin{pmatrix} 1 & 1 & 0 \\ 0 & 1 & 1 \\ 0 & 0 & 1 \end{pmatrix} \hspace{2cm}
 M_2=\begin{pmatrix} 0 & 1 & 1 & 0 \\ 1 & 0 & 1 & 1 \\ 1 & 1 & 0 & 1 \\ 0 & 1 & 1 & 0 \end{pmatrix}`$.
 
Lorsqu'un graphe est non orienté sa matrice d'adjacence est symétrique : on a toujours $m_{i,j}=m_{j,i}$.
Voici un exemple d'une implémentation d'une classe `GrapheM` s'appuyant sur la représentation par matrice d'adjacence :

~~~python
class GrapheM:
    def __init__(self, mat):
        self.m = mat

    def est_lie(self, i, j):
        return self.m[i][j] == 1

    def est_symetrique(self):
        for i in range(len(self.m)):
            for j in range(len(self.m[0])):
                if self.m[i][j] != self.m[j][i]:
                    return False
        return True

m1 = [[1, 1, 0], [0, 1, 1], [0, 0, 1]]
g1=GrapheM(m1)

m2=[[0, 1, 1, 0], [1, 0, 1, 1], [1, 1, 0, 1], [0, 1, 1, 0]]
g2=GrapheM(m2)

print(g1.est_lie(0, 0))
print(g1.est_lie(0, 2))
print(g1.est_symetrique())
print(g2.est_symetrique())
~~~

## Liste des voisins

Pour représenter un graphe on peut également, pour chacun de ses sommets, donner la liste des sommets 
auxquels il est relié. Lorsque le graphe est non orienté, on parlera de liste de voisins. Lorsqu'il est orienté,
on peut décider de représenter un graphe par la liste de ses successeurs, ou lorsque les problèmes
que l'on étudie rendent cette représentation plus adaptée par la liste de ses prédécesseurs.

Nous choisirons de coder ici les sommets avec des nombres (de 0 à $n-1$ où $n$ est l'ordre du graphe)
et les listes de successeurs avec des listes, ce qui facilite le passage d'une liste de voisins à la matrice d'adjacence (voir exercices).
Il est également possible de coder les listes de successeurs avec des dictionnaires, ce qui permettra d'étiquetter les graphes,
et qui sera proposé en exercice.

Voici un exemple de classe simple implémentant ceci, les graphes `G3` et `G4` étant ceux de l'image précédente.

~~~python
class GraphLS:
    def __init__(self, lst):
        self.lst = lst

    def est_lie(self, i, j):
        return j in self.lst[i]

    def graph_to_matrix(self):
        n = len(self.lst)
        mat = [] # Création de la matrice
        for i in range(n):
            lst = [0 for i in range(n)] # Création de la ième sous-liste ; n zéros
            for x in self.lst[i]:
                lst[x] = 1 # Il y a un arc de i vers x
            mat.append(lst)
        return mat

lst_som1=[[0, 1], [1, 2], [2]]
g3=GraphLS(lst_som1)

lst_som2=[[1, 2], [0, 2, 3], [0, 1, 3], [1, 2]]
g4=GraphLS(lst_som2)

print(g3.est_lie(0, 2))
print(g3.graph_to_matrix())
print(g4.graph_to_matrix())
~~~




# Parcours de graphes : Présentation

En bref :

> Nous présentons ici les parcours de graphes. Ceux-ci consistent à explorer les sommets atteignables 
d'un graphe à partir d'un sommet de départ  par un chemin ou par une chaîne.
> Le choix de la stratégie d'exploration d'un graphe non pondéré donnera les parcours en largeur et en profondeur.
> Nous présentons ensuite l'algorithme de Dijkstra qui permet de trouver les plus courts chemins depuis un sommet de départ dans un graphe pondéré non orienté.

## Parcours en largeur et en profondeur

Les parcours de graphes correspondent à des objectifs variés. L'idée commune est de partir d'un sommet du graphe, déterminé à l'avance (éventuellement, suivant les problèmes que l'on cherche  à résoudre cette procédure peut être réitérée depuis un autre sommet), puis d'explorer les sommets atteignables depuis celui-ci, en respectant un ordre de traitement des sommets imposés par le type  de parcours qu'on effectue. Par exemple :

![](IMGS/img_fiche3.png)

Considérons ce graphe orienté et A comme sommet de départ. Deux stratégies de parcours (on admet
que le voisin exploré en premier est celui qui a la première lettre dans l'ordre alphabétique) sont envisageables:

- le parcours en largeur (BFS en anglais pour Breadth First Search) où on explore en priorité tous les voisins  de A puis tous les voisins des voisins de A etc... Cela donne ici le parcours ABCDEFGH.

- la parcours en profondeur (DFS en anglais pour Depth First Search) où on explore en priorité les voisins du premier voisin de A puis récursivement ses voisins respectifs. Cela donne ici le parcours ABDGHEFC.

## Algorithme de Dijkstra

On considère un graphe non orienté pondéré avec des poids positifs. L'algorithme de Dijkstra permet de trouver à partir d'un sommet `i` de départ fixé à l'avance  les plus courts chemins menant à tout sommet accessible depuis `i`.

Le graphe suivant représente les distances entre plusieurs villes :

![](IMGS/img_dijkstra.png)

Initialement, on construit `lst_e = []` liste de sommets déjà explorés ; `lst_v = [i]` liste de sommets atteignables et non explorés et `lst_d` liste des
distances à `i` et des pères. Initialement `lst_d` est composée de $`n`$ fois `[float.inf, None]` sauf `lst_d[i] = [0, None]`.
Tant que `lst_v` n'est pas vide, on sélectionne le sommet `s` de `lst_v` qui a la plus courte distance à `i` parmi les sommets de `lst_v`.
On supprime ce sommet de `lst_v` et on l'ajoute à `lst_e`. Pour chaque voisin `v` de `s` on regarde si il est dans `lst_e`. Si ce n'est pas le cas, on l'y ajoute 
et on met à jour `lst_d`: `lst_d[v] = [lst_d[s][0] + distance(s, v), s]`. Si il est présent dans `lst_e` on regarde si la quantité `lst_d[s][0] + distance(s, v)`
est inférieure à `lst_d[v][0]` auquel cas on remet à jour `lst_d`.
Lorsque `lst_v`est vide l'algorithme s'arrête. On connaît alors la plus distance de `i` à tout sommet accessible, et en remontant dans l'ordre les pères on peut reconstituer 
le chemin qui a cette longueur.

Sur l'exemple de l'image cela donne (`None` est noté N et $`+\infty`$ est noté inf) :

| lst_v | lst_e | s | 0 | 1 | 2 | 3 | 4 | 5 | 6 |
| --- | --- | --- | --- | --- | --- | --- | --- | --- | --- |
| (init) | [0] | [] | 0,N | inf,N | inf,N | inf,N | inf,N | inf,N | inf,N |
| [0] | [] | 0 | 0,N | 2,0 | 3,0 | inf,N | inf,N | inf,N | inf,N |
| [1,2] | [0] | 1 | 0,N | 2,0 | 3,0 | 5,1 | 10,1 | inf,N | inf,N |
| [2,3,4] | [0,1] | 2 | 0,N | 2,0 | 3,0 | 5,1 | 10,1 | 9,2 | inf,N |
| [3,4,5] | [0,1,2] | 3 | 0,N | 2,0 | 3,0 | 5,1 | 8,3 | 7,3 | inf,N |
| [4,5] | [0,1,2,3] | 5 | 0,N | 2,0 | 3,0 | 5,1 | 8,3 | 7,3 | 11,4 |
| [5,6] | [0,1,2,3,5] | 4 | 0,N | 2,0 | 3,0 | 5,1 | 8,3 | 7,3 | 11,4|
| [6] | [0,1,2,3,5,4] | 6 | 0,N | 2,0 | 3,0 | 5,1 | 8,3 | 7,3 | 11,4 |

L'algorithme de Dijkstra est notamment utilisée pour le routage sur internet, voir le chapitre 10 exercice 2 pour plus de détails. 





# Parcours de graphes : Implémentation et Applications

En bref :

> Nous continuons ici l'étude du parcours des graphes, avec l'implémentation concrète des parcours en largeur et en profondeur et la présentation de leurs applications.

## Parcours en profondeur

Pour effectuer le parcours d'un graphe en profondeur nous allons utiliser une structure de Pile.

Cette pile contiendra initialement le sommet de départ. Tant qu'elle ne sera pas vide, nous dépilerons son sommet, et regarderons si il a déjà été exploré.
Si ce n'est pas le cas, nous le rajouterons à la liste des sommets déjà visités (initialement vide) et empilerons tous ses voisins.
Lorsque la pile sera vide, la liste des sommets visités donnera le parcours en profondeur du graphe.

Nous allons utiliser la classe `GrapheLS`. Une liste `vus` contenant les sommets déjà visités sera utilisée, ainsi qu'une liste `parc`
qui donnera le parcours effectué. La structure de Pile sera modélisée avec une liste `voisins`. Le sommet de la pile sera le dernier élément de la liste, de sorte à
pouvoir le supprimer en temps constant. Les voisins seront empilés à l'envers, de sorte à ce que le premier voisin soit en fin de la liste `voisins` ; ceci
sera réalisé techniquement en inversant la liste des voisins du sommet exploré à l'aide de la méthode `lst.reverse()`.


~~~python
def profondeur(self, dep):
    n = len(self.lst)
    vus = [False] * n
    vus[dep] = True
    voisins = self.lst[dep][:]
    voisins.reverse()
    parc = [dep]
    while voisins: #il reste des sommets
        explore = voisins.pop()
        if not vus[explore]:
            parc.append(explore)
            vus[explore] = True
            v = self.lst[explore]
            v.reverse()
            voisins.extend(v)
    return parc
~~~

Le parcours en profondeur permet de trouver la composante connexe d'un sommet (tous ses sommets accessibles). Il est également utilisé pour répondre au problème
du tri topologique d'un graphe orienté, qui permet de donner un ordre dans lequel peuvent être effectuées des tâches dont la réalisation dépend d'autres tâches.

## Parcours en largeur

Pour effectuer le parcours d'un graphe en largeur nous allons utiliser une structure de File.

Cette file contiendra initialement le sommet de départ. Tant qu'elle ne sera pas vide, nous traiterons le premier sommet, et regarderons si il a déjà été exploré.
Si ce n'est pas le cas, nous le rajouterons à la liste des sommets déjà visités (initialement vide) et mettrons dans la file d'attente tous ses voisins.
Lorsque la file sera vide, la liste des sommets visités donnera le parcours en largeur du graphe.


Nous allons utiliser la classe `GrapheLS`. Une liste `vus` contenant les sommets déjà visités sera utilisée, ainsi qu'une liste `parc`
qui donnera le parcours effectué. La structure de File sera modélisée avec une liste `voisins`. Le premier élément de la liste sera celui à traiter en priorité
dans la file. Il sera supprimé et récupéré à l'aide de la méthode `voisins.pop(0)`, ce qui n'est malheureusement pas optimal dans la mesure où cette opération
est en temps linéaire. Les voisins seront mis dans l'ordre à la fin de la liste `voisins`.

~~~python
def largeur(self, dep):
    n = len(self.lst)
    vus = [False] * n
    vus[dep] = True
    voisins = self.lst[dep][:]
    parc = [dep]
    while voisins:
        explore = voisins.pop(0)
        if not vus[explore]:
            parc.append(explore)
            vus[explore] = True
            voisins.extend(self.lst[explore])
    return parc
~~~

Le parcours en largeur d'un graphe permet également de trouver la composante connexe d'un sommet. Il permet également de trouver tous les plus courts chemins à partir d'un sommet,
ce qui permet par exemple de trouver de manière optimale la sortie d'un labyrinthe.






