# S'entraîner

## 4) Utiliser les graphes pour modéliser un problème 

On considère un graphe non orienté. On appelle degré d'un sommet $`s`$ le nombre d'arêtes partant de ce sommet.

1/ Montrer que la somme des degrés d'un graphe est égale au double du nombre d'arêtes de ce graphe.

2/ On désire organiser un tournoi de football durant un week-end. 9 équipes sont inscrites. Chaque équipe devra jouer exactement trois matchs. Est-ce possible ?

## 5) Utiliser un graphe étiqueté 

On considère dans cet exercice un graphe orienté étiqueté, représentant les personnes suivies par d'autres personnes sur un réseau social.

![](IMGS/img_reseau_social.png)

Ce graphe est codé par un dictionnaire, où les clés sont les chaînes de caractères correspondant aux noms des personnes inscrites, et les valeurs associées sont des listes de chaînes de caractères, représentant les personnes suivies.

1/ Définir le dictionnaire correspondant au graphe proposé dans l'image ci-dessus.

2/ Écrire une fonction `amis_d_amis(dico, pers)` qui prend en argument un dictionnaire représentant un tel graphe et une chaîne de caractères qui représente une personne et
qui renvoie la liste des amis des amis de `pers`, à l'exclusion d'elle-même et sans doublon.

## 6) Passer d'une représentation sous forme de matrice d'adjacence à une liste des successeurs 

On dispose de la classe `GrapheM` telle que proposée dans le cours.

Écrire une méthode `matrix_to_list` qui permet de passer de la représentation par matrice d'adjacence à la représentation par liste de successeurs.

Le graphe renvoyé sera codé sous forme de dictionnaire, les clés étant les sommets, numérotés de 0 à $`n-1`$, $`n`$ étant l'ordre du graphe ; et les valeurs associées sont les listes des successeurs du sommet.
 
## 7) Passer de la liste des successeurs à celle des prédécesseurs

On dispose de la classe `GrapheLS` telle que proposée dans le cours.

Écrire une méthode `predecesseurs` qui renvoie un dictionnaire, dont les clés sont les sommets, numérotés de 0 à $`n-1`$, $`n`$ étant l'ordre du graphe ; et les valeurs associées sont les listes des prédécesseurs du sommet.

<!-- ## Utiliser le parcours en profondeur pour résoudre un problème d'ordonnancement des tâches.

On considère dans cet exercice un graphe orienté sans cycle, représentant des tâches à effectuer. Celles-ci doivent dans certains cas être faites dans un ordre préétabli, un arc entre deux sommets $i$ et $j$ représentant le fait que la tâche $i$ doit être effectuée avant la tâche $j$. On considère par exemple la fabrication de lasagnes à la bolognaise, représentée par ce graphe :

On appelle ordre topologique la donnée d'une liste comprenant l'ensembles des tâches (sommets) dans un ordre tel que si il existe un chemin entre $i$ et $j$ (ce qui signifie que la tâche $i$ doit être réalisée avant la tâche $j$) alors $j$ apparaît après $i$ dans cette liste.

1/ Proposer un ordre topologique pour le graphe ci-dessus.

2/ Écrire un dictionnaire où les clés sont les étiquettes des sommets du graphe ci-dessus et les valeurs associées les listes de ces successeurs.

3/  -->
## 8) Utiliser un parcours pour tester la connexité d'un graphe 

On dispose de la classe `GrapheLS` telle que proposée dans le cours. On considère que l'on travaille sur un graphe non orienté.

1/ Le degré du sommet `s` d'un graphe est le nombre de voisins auquel il est connecté. Écrire une méthode `degre(self, s)` qui détermine le degré du sommet `s` d'un graphe.
 
2/ On rappelle qu'un graphe est connexe lorsque toute paire de sommets est reliée par une chaîne. Expliquer pourquoi un parcours en largeur ou en profondeur suffit à déterminer si un graphe non orienté est connexe. 
Écrire une méthode `connexe(self)` qui renvoie un booléen indiquant si un graphe est connexe ou non.

Le problème suivant est à l'origine de la création de la théorie des graphes, par Euler en 1736. Les habitants de Kaliningrad, dont un plan sommaire est donné dans l'image ci-dessous, désiraient savoir si il est possible lors d'une promenade de passer tous les ponts de la ville une fois et une seule :

![](IMGS/ponts.png)

On appelle chaîne eulérienne une chaîne qui parcourt toutes les arêtes d'un graphe une fois et une seule. On appelle graphe eulérien un graphe où il existe une chaîne eulérienne.

Le théorème d'Euler affirme : " Un graphe est eulérien si et seulement si il est connexe et possède zéro ou deux sommets de degré impair".

3/ Le problème des ponts de Kaliningrad admet-il une solution ?

4/ Écrire une méthode `eulerien(self)` qui renvoie un booléen indiquant si un graphe est eulérien.

## 9) Chercher un chemin et sa longueur dans un graphe pondéré

Les graphes considérés ici sont pondérés. On considère par exemple le graphe suivant :

![](IMGS/graphes07.png)

Ce graphe est représenté par une matrice de pondération : il s'agit d'un tableau de nombres, avec $`n`$ lignes et $`n`$ colonnes, ou $`n`$ est l'ordre du graphe,
tel que la $`i-`$ème ligne et la $`j-`$ème colonne contient 0 si aucune arête ne lie les sommets $`i`$ et $`j`$ et contient la longueur (ou poids) entre ces deux sommets si ils sont liés.

1/ Quelle est la matrice de pondération du graphe représenté ci-dessus ? Écrire en Python une telle matrice `mat` en codant ce tableau de nombres comme une liste de listes.

2/ On considère `lst` une liste de sommets. Écrire une fonction `existe(mat, lst)` qui renvoie un booléen indiquant si `lst` représente ou non une chaîne sur ce graphe.

Par exemple, `existe(mat,[0, 1, 4, 2])` renverra `True` alors que `existe(mat, [0, 4, 1, 2])`renverra `False`.

3/ Adapter la fonction précédente en une fonction `longueur(mat, lst)` pour qu'elle renvoie la longueur de la chaîne considérée si elle existe et 0 sinon.

Par exemple, `longueur(mat,[0, 1, 4, 2])` renverra 9 alors que `longueur(mat, [0, 4, 1, 2])`renverra 0.


## 10) Utiliser un parcours en profondeur pour détecter un cycle dans un graphe non orienté

On dispose de la classe `GrapheLS` telle que proposée dans le cours. On considère que l'on travaille sur un graphe non orienté.

1/ Expliquer comment un parcours en profondeur permet de détecter la présence d'un cycle dans ce graphe.

2/ Écrire en modifiant le parcours en profondeur de la classe `GrapheLS` une méthode `existe_cycle` qui renvoie un booléen indiquant si il y a un cycle dans ce graphe.


## 11) Dijkstra et OSPF

![reseau2](./IMGS/reseau2.png)

 On dispose des données suivantes sur les débits:
- À l'intérieur du réseau 192.168.0.0/25 : 1,544 Mb/s
- Liaison Routeur 1 -> Switch 1 : 100 Mb/s
- Laison Routeur 1 -> Routeur 2 : 2,048 Mb/s
- Liaison Switch 1 -> Routeur 2 : 2,048 Mb/s
- Liaison Switch 1 -> Routeur 3 : 100 Mb/s
- Liaison Routeur 2 -> Routeur 3 : 1,544 Mb/s
- Liaison Routeur 2 -> Routeur 4 : 100 Mb/s
- Liaison Routeur 2 -> Switch 2 : 1,544 Mb/s
- Liaison Switch 3 -> Routeurs 3 et 4 : 10 Mb/s
- Liaison Routeur 4 -> Serveur Web : 100 Mb/s

Le débit de référence sera de 100 Mb/s

Déterminer la route pour aller de la machine 192.168.0.101 à internet selon le protocole OSPF. On commencera par représenter le réseau par un graphe dont les sommets sont les routeurs et les switchs, dont les arêtes sont les liaisons et dont les étiquettes sont les distances (rapport entre le débit de référence et leurs débits binaires). Appliquez ensuite l'algorithme de Dijkstra.



