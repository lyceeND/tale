# Objectif Bac 

## 11) Adapter le parcours en largeur pour résoudre un problème de distance (durée : 1 heure 30)

### Partie 1 : Algorithmes sur les graphes

On désire modifier le parcours en largeur présenté dans le cours pour qu'il permette non seulement de connaître les sommets atteints durant le parcours 
mais également le nombre de sommets qu'il aura fallu explorer avant d'atteindre chaque sommet. Ce nombre de sommets représente la ditance au sommet de départ :
les voisins sont à une distance de 1 ; les voisins des voisins à une distance de 2 etc...

1. Écrire une méthode `largeur_dist(self, dep)` qui prend un graphe de la classe
`GrapheLS` fournie dans le cours et qui renvoie un dictionnaire où les clés sont les numéros des sommets et les valeurs associées les distances au sommet de départ. Si un sommet n'est pas atteignable depuis le départ la valeur associée sera de -1.

2. On désire adapter la méthode précédente pour trouver la distance à une case sur un échiquier avec des déplacements de cavalier.

Un cavalier se déplace, lorque c'est possible (il ne peut aller au-delà du bord) de 2 cases dans une direction verticale ou horizontale, et de 1 case dans l'autre direction.

![](IMGS/img_cavalier.png)

Les cases de l'échiquier sont représentées par des tuples. Le couple `(i,j)` désigne la case d'abscisse `i` et d'ordonnée `j`.
 On travaillera sur un échiquier de taille quelconque, comportant $`n`$ lignes et $`p`$ colonnes. Un échiquier standard possède $`n=8`$ lignes (numérotées de 0 à 7) et $`p=8`$ colonnes (également numérotées de 0 à 7).

Écrire une fonction `voisins(t, n,p )` qui prend en argument un tuple `t` représentant des coordonnées sur un échiquier et qui renvoie la liste de ses voisins, représentés par des tuples.

3. Créer un dictionnaire `dico_voisins` dont les clés sont les cases de notre échiquier et les valeurs associées sont la liste de ses voisins.

4. Adapter la méthode `largeur_dist(self, dep)` pour écrire une fonction `dist_cases(dico, dep, n, p)` qui prend en entrée un tuple `dep` représentant des coordonnées et les dimensions de l'échiquier
et qui renvoie un dictionnaire dont les clés sont les cases de notre échiquier et les valeurs associées les distances à la case `dep` en déplacement de cavalier.

5. Pour une meilleure présentation des résultats, utiliser la fonction précédente pour écrire une fonction `a_distance(dico, dep, n, p)` qui prend en entrée un tuple `dep` représentant des coordonnées et les dimensions de l'échiquier
et qui renvoie un dictionnaire où les clés sont la distance à la case `dep` et les valeurs associées sont des listes de tuple à cette distance de `dep`.

### Partie 2 : À la découvrte d'un module graphique

On travaille dans cette partie avec un échiquier classique ($`n=p=8`$). L'objectif est d'obtenir une image représentant la distance à une case donnée en déplacement de cavaliers, sur un échiquier que l'on aura tracé. Par exemple :

![](IMGS/distance.png)

Nous utiliserons dans cette partie le module `matplotlib.pyplot`, qui permet de tracer des graphiques.

La partie qui nous intéresse de ce module peut être importée à l'aide de :

~~~python
import matplotlib.pyplot as plt
~~~

Une nouvelle figure peut être obtenue à l'aide de :

~~~python
plt.figure(nom_ou_numero_optionnel)
~~~

Un titre peut être donné à l'aide de la commande suivante, `titre` étant une chaîne de caractères :

~~~python
plt.title(titre)
~~~

Les commandes suivantes contrôlent les axes :

~~~python
plt.axis("off") # rend les axes invisibles
plt.axis("equal") # rend les axes isométriques
~~~

Un segment entre le point de coordonnées `(x0, y0)` et le point de coordonnées `(x1, y1)` peut être tracé à l'aide de :

~~~python
plt.plot([x0, x1], [y0, y1], **kwargs)
~~~

`**kwargs` désigne des arguments optionnels ; plusieurs sont présentés plus bas. Pour de plus amples détails, on pourra se référer à la documentation en ligne de `plt.plot`.

Du texte, contenu dans une chaîne de caractères `s` peut être écrit dans la figure au point de coordonnées `(x,y)` à l'aide de :

~~~python
plt.text(x, y, s, **kwargs)
~~~

La figure peut être affichée à l'aide de `plt.show()`. Elle peut être fermée avec `plt.close()` et sauvegardée à l'aide de `plt.savefig(s)` où la chaîne de caractères `s` désigne le nom du fichier.

Les couleurs les plus simples sont codées à l'aide de :

| Couleur | Codage |
| --- | --- |
| noir | `'k'` |
| rouge | `'r'` |
| vert | `'g'` |
| bleu | `'b'` |
| jaune | `'y'` |

Si l'on souhaite colorer un segment, on ajoute dans l'appel à `plt.plot` l'argument optionnel `color = 'y'` pour un tracé en jaune par exemple 

Quelques exemples d'arguments optionnels pour le texte :

| Argument | Valeur | Signification |
| --- | --- | --- |
| fontsize | entier | écrit le texte en police de taille donnée |
| color  | chaîne | écrit le texte en couleur |
| horizontalalignment | "left", "center", "right" | positionne le texte horizontalement | 
| verticalalignment | "left", "center", "right" | positionne le texte verticalement | 

On écrira dans toute cette partie une procédure `dessine_echiquier(i, j)`, où `i` et `j` désignent le numéro de la ligne et le numéro de la colonne de la case de départ.

6. Créer une figure, rendre les axes invisbles et égaux.

7. Donner un titre à la figure. Celui-ci sera constitué de la chaîne de caractères "Distance à la case" à laquelle on aura concaténé les coordonnées de la case de départ `(i,j)`.

8. On dessinera l'échiquier en traçant 9 segments horizontaux noirs aux ordonnées espacées de 20 en 20 de 0 à 160 et avec des abscisses comprises entre 0 et 160
et 9 segments verticaux noirs aux abscisses espacées de 20 en 20 de 0 à 160 et avec des ordonnées comprises entre 0 et 160.

Tracer l'échiquier.

9. En utilisant un des dictionnaires construits dans la première partie, écrire sur la figure les nombres correspondant aux distances à la case de départ en déplacement de cavalier.
On utilisera du texte bleu, centré horizontalement et verticalement, une fonte de taille 11, et on déterminera les coordonnées du centre de la case de ligne `i` et de colonne `j`.

10. Placer les numéros de lignes et de colonnes. Ils seront positionnés au niveau du centre de leur colonne et de leur ligne, à des coordonnées d'abscisse -8 pour les numéros des lignes,
et à des coordonnées d'ordonnée -8 pour le numéro des colonnes. On utilisera du texte rouge, centré horizontalement et verticalement, une fonte de taille 8.

11. Afficher et enregistrer l'image. Tester votre procédure.

### Feuille de route

## Feuille de route 

1/ **Écrire un parcours en largeur** 

L'implémentation que nous avons proposée du parcours en largeur ne tient pas compte de la distance au sommet de départ. Plutôt que de travailler avec une simple liste de booléens, permettant de marquer si les sommets ont été visités ou non, 
il est plus pertinent d'utiliser une liste d'entiers, initialisée à -1 (sommet non visité) sauf pour le sommet de départ initialisé à 0. Cette liste contiendra la distance à l'origine pour les sommets déjà visités. 
Il n'est possible de la mettre à jour que si l'on stocke les sommets à explorer avec leurs pères (sommet d'où on les atteint) ; ceci est par exemple possible en stockant un tuple de la forme `(provenance, sommet)` dans la liste des voisins.

2/ **Créer une liste des déplacements et faire une boucle** 

Il est facile de créer à la main une liste des déplacements relatifs du cavalier : `lst = [(-2, 1), (2, -1), ...]`. Ensuite, il suffit de tester pour chaque case d'arrivée si elle sort ou non du cadre, et de stocker ceci dans une liste.

3/ **Construire un dictionnaire** 

On peut partir d'un dictionnaire vide, que l'on remplit avec tous les tuples possibles correspondant aux cases de l'échiquier comme clé et dont les valeurs associées sont les listes renvoyées par la fonction précédente.

4/ **Écrire un parcours en largeur** 

Il s'agit d'adapter ce qu'on a fait à la première question en tenant compte du fait que les clés sont désormais des tuples de coordonnées sur l'échiquier.

5/ **Manipuler des listes et des dictionnaires**

On peut commencer par récupérer la valeur associée maximale `m` dans le dictionnaire précédent, puis créer un dictionnaire dont les clés sont les valeurs de `0` à `m` et les valeurs associées des listes vides.
On parcourt ensuite le dictionnaire précédent, et on ajoute dans le nouveau dictionnaire les cases dans les bonnes listes.

6/ **Lire une documentation** 

Reprendre les instructions données en préambule.

7/ **Lire une documentation et l'appliquer**

L'instruction est donnée en préambule, attention au type des objets, on peut convertir un tuple `t ` en chaîne de caractères à l'aide de `srt(t)`.

8/ **Écrire des boucles et calculer des coordonnées** 

On utilise l'instruction `plt.plot` pour tracer des segments. Comme il y a 9 segments horizontaux à tracer, une boucle gagne du temps. Les extrémités des segments
horizontaux sont les points $`(0,20i)`$ et $`(160,20i)`$. On fait pareil pour les segments horizontaux. La couleur s'obtient en utilisant `plt.plot(lst1, lst2, color = 'k')`.

9/ **Se repérer dans l'espace ; écrire des boucles et lire une documentation** 

Les coordonnées du point à la ligne `i` et à la colonne `j` sont $`(10+20j,10+20i)`$ et non le contraire. On parcourt un dictionnaire (celui dont les clés sont des coordonnées est plus facile à utiliser) et on remplit les cases en n'oubliant pas qu'une chaîne de caractères doit être passée à `plt.text`.

10/ **Se repérer dans l'espace ; écrire des boucles** 

Deux boucles successives, ou une seule boucle plaçant les lignes et les colonnes simultanément suffit. En cas de difficultés, s'inspirer de ce qui a été fait précédemment pour déterminer les coordonnées.

11/ **Tester le code**

Finaliser la procédure en utilisant les méthodes `show()` et `savefig(fichier)` qui permettent d'afficher et sauvegarder l'image. Puis vérifiez vos résultats et débugguez votre programme le cas échéant.


