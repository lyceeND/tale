# Graphe eulerien


Lors de létude du  problème des ponts de Königsberg, Euler  a énoncé et démontré
le théorème suivant:

>Un graphe symétrique  G=(X,A) est eulérien si, et seulement  si, il est connexe
>et si chaque sommet est de degré pair.


Occupons-nous d'abord de la *condition  nécessaire*: il est évident que le
graphe doit être connexe. De plus, si un cycle arrive en un sommet, il
doit pouvoir en repartir sans emprunter une arête déjà utilisée: on peut
donc mettre  en bijection l'ensemble des arêtes  entrantes et l'ensemble
des arêtes sortantes, ce qui signifie que les sommets sont tous de degré pair.



La *condition* est aussi *suffisante*. Partons d'un sommet
quelconque. On avance en parcourant  des arêtes. À chaque fois qu'on 
"entre" en un sommet, on en "sort" par une autre arête car son degré est pair. On
continue  ainsi à  "traverser"  des sommets  sans être  bloqué en
empruntant des  arêtes différentes. Cependant, le nombre  de sommets est
fini et il reste un sommet  emprunté une seule fois, celui de départ: on
y retourne  donc forcément et  on a ainsi *construit*  un cycle
avec a priori une partie des arêtes empruntées une et une seule fois.

Considérons  maintenant une  arête  non empruntée,  s'il  en existe  une
(sinon, on a trouvé un cycle eulérien). Il y
a donc  au moins une de  ses extrémités qui n'appartient  pas au cycle
initial. Comme  le graphe est connexe,  il existe une chaîne  vers un des
sommets du cycle. Éliminons dans  le cycle de départ tous les sommets
sauf celui-là (qu'on notera s) et toutes les arêtes empruntées.  Il nous reste un
sous-graphe ayant  un sommet en commun  avec le cycle  initial. Tous ses
sommets sont de degré pair, certes, mais il peut ne pas être connexe. 
On peut cependant construire un cycle eulérien comme précédemment dans
ce sous-graphe et  on fusionne ces deux cycles  qui n'ont aucune arête
en commun.

On réitère ce procédé jusqu'à  ne laisser aucune arête non parcourue. Il
se termine car il y a un nombre fini d'arêtes.



Ce   théorème  est   lié   à  un   problème   classique,  celui   du
*postier chinois*, la nationalité du postier étant celle de Meigu
Guan qui le proposa en 1962:  est-ce qu'un postier peut faire sa tournée
en ne repassant jamais par la même rue ?


> Que se passe-t-il si tous les sommets sont de degré pair sauf deux ?


Voici maintenant un extrait de Wikipedia :

>Fleury's  algorithm is  an  elegant  but inefficient  algorithm  that dates  to
>1883. Consider a graph known to have  all edges in the same component and at
>most  two vertices  of odd  degree. The  algorithm starts  at a  vertex of  odd
>degree,  or, if  the  graph has  none,  it starts  with  an arbitrarily  chosen
>vertex. At  each step  it chooses the  next edge  in the path  to be  one whose
>deletion would not disconnect the graph, unless there is no such edge, in which
>case it picks the  remaining edge left at the current vertex.  It then moves to
>the  other endpoint  of that  edge and  deletes  the edge.  At the  end of  the
>algorithm there are no  edges left, and the sequence from  which the edges were
>chosen forms an Eulerian  cycle if the graph has no vertices  of odd degree, or
>an Eulerian trail if there are exactly two vertices of odd degree.



Un pont  dans un graphe  est une  arête qui, si  elle est supprimée,  modifie la
connexité du graphe.

Créez une méthode `est_un_pont(self,  a, b)` qui teste si l'arête  [a, b] est un
pont dans le graphe.

Étudier la méthode `randrange` du module `random`.

Il s'agit maintenant  de programmer l'algorithme de Fleury.  Vous aurez sûrement
besoin de:

- `nombre_aretes`
- `sommets`
- `degre_sommet`
- `random.randrange`
- `adjacents`
- `est_un_pont`
- `graphe_reduit`

Votre fonction renverra une liste donnant les sommets d'un cycle eulerien.
