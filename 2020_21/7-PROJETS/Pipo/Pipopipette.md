# Jouer à la pipopipette

## En bref

Du Tic-Tac-Toe au jeu de Go, les machines rivalisent avec les humains. Dans les
jeux où le nombre de coups est limité (Tic-Tac-Toe, morpion, puissance 4...)
l'algorithme du minimax permet de programmer un adversaire de taille. C'est ce
qui est proposé ici avec le jeu de la Pipopipette.

## Objectif du projet

L'objectif est de réaliser un programme informatique qui joue à la Pipopipette
contre un humain (voir règles plus loin). L'algorithme utilisé par le programme
sera l'algorithme du minimax, évoqué fiche 16. L'accent est mis ici sur la
programmation du joueur non humain, mais le projet de prête aussi à la
réalisation d'une interface graphique.

## Règles du jeu de la Pipopipette

La [Pipopipette](https://fr.wikipedia.org/wiki/La_Pipopipette)
(ou jeu des petits carrés)
a été inventé à la fin du 19e siècle par le mathématicien français 
[Édouard Lucas](https://fr.wikipedia.org/wiki/%C3%89douard_Lucas)
(inventeur des 
[tours de Hanoï](https://fr.wikipedia.org/wiki/Tours_de_Hano%C3%AF)).

Le jeu se joue à deux joueurs sur une grille de taille arbitraire.
Voici une grille de 2x2 cases, comportant 12 segments.

![](IMGS/img_2_ggggwggggwggggwggggw.png){height=60}

### Tours de jeu

Chaque joueur à son tour doit tracer un des segments de la grille.
N'importe quel segment qui n'a pas déjà été tracé est
éligible. Si ce nouveau tracé forme un petit carré (peu importe que les autres
segments du carré aient été faits par l'un ou l'autre des joueurs), alors le carré
est **fermé**, le joueur qui l'a fermé marque un point, et peut rejouer.

C'est ce qui se produit dans la situation suivante. Chaque joueur a déjà joué 2
coups :

![C'est maintenant au tour du joueur bleu](IMGS/img_2_gbggwrrgbwggggwgbggw.png){height=60}

<!--C'est au tour du joueur bleu, qui ferme un carré :-->

![Qui ferme un carré, marque un point](IMGS/img_2_gbggwrrbbbggggwbbggw.png){height=60}

<!-- Et peut donc rejouer : -->

![Et peut donc rejouer](IMGS/img_2_gbgbwrrbbbggggwbbggw.png){height=60}

### Fin de partie 

Sur cette grille 2x2, la partie se termine lorsque les 12 segments ont été
tracés, fermant ainsi
nécessairement les quatre carrés. La somme des scores des deux joueurs vaut
nécessairement 4. Celui qui a le plus gros score gagne. Il y a un cas d'égalité,
si chaque joueur a fermé deux carrés.

### Vers un algorithme

La pipopipette est un jeu à information complète (chaque joueur a connaissance de la
totalité du jeu) et à somme nulle (les gains d'un joueur sont synonymes de
pertes pour l'autre). C'est ce type de jeu qu'il est possible résoudre
complètement ou partiellement avec l'algorithme du **minimax**.

## Présentation du Minimax

Le principe est de calculer toutes les issues du jeu à partir de la position
actuelle. À chaque issue (fin de partie) sera attribué un score. Ici, on
pourra utiliser comme score le nombre de carrés fermés par le
joueur bleu moins le nombre de carrés fermés par le joueur rouge.

<!--La joueur bleu
aura donc pour objectif de maximiser ce score, alors que le joueur rouge aura
pour objectif de le minimiser.-->
<!--
coup adverse etc... et de supposer que l'adversaire jouera au mieux. On étudie
ainsi toutes les fins de parties possible auxquelles on affecte un score. Ici,
on pourrait prendre par exemple comme score le nombre de carrés fermés par le
joueur bleu moins le nombre de carrés fermés par le joueur rouge.
-->

Sur une grille 1x3, ce score sera compris entre +3 (victoire écrasante du
bleu) et -3 (victoire écrasante du rouge). L'objectif du joueur rouge sera de
minimiser ce score (ou au moins de le rendre strictement négatif). 
L'objectif du joueur bleu sera de le maximiser (ou au moins, de le rendre
strictement positif).

### Évaluation d'un état du jeu

Voici un exemple de position de jeu. Un carré a déjà été fermé par le joueur
rouge, et c'est au tour du joueur rouge de jouer :

![](IMGS/img_brrbrrrgrwggbrw.png){height=30}

Sur le graphe de la figure 1, la
couleur de l'arête (flèche) correspond à la couleur du joueur qui joue.

![Figure 1: Évaluation d'une position de jeu](IMGS/graphe5.png){width=17cm}

Nous voyons que le joueur rouge a trois possibilités. S'il joue dans la case la
plus à droite (deux segments possibles), le bleu termine la partie. L'évaluation
de la situation finale donne le score 1 (2 pour le bleu -1 pour le rouge).

En revanche, s'il joue dans la case du milieu, il ferme un carré et rejoue.
Quelle que soit la suite de la partie, l'évaluation finale sera alors -1 (1 pour le
bleu et 2 pour le rouge). Des trois choix, le rouge choisit donc celui qui
**minimise** l'évaluation finale : il joue dans la case du milieu pour atteindre
sûrement la fin de partie de score -1.

L'évaluation de l'état du jeu avant le coup du rouge est donc -1 car le rouge
pourra nécessairement atteindre ce score.

Dans la situation suivante, le même nombre de coup a été joué, aucun carré n'a été
fermé, et c'est au tour du joueur bleu, qui a trois choix :

![](IMGS/img_bgrbwrrrgwggbrw.png){height=30}

Les possibilités de fin de partie sont représentées sur la figure 2.

![Figure 2: Évaluation d'une autre position de jeu](IMGS/graphe6.png){width=17cm}

En jouant le segment entre les deux premières cases, le joueur bleu ferme deux
carrés, puis rejoue, ce qui amènera à une fin de partie évaluée à 1 (2 pour le
bleu et 1 pour le rouge).  

En revanche, jouer dans la case de droite (deux choix) conduira toujours à une fin de
partie évaluée à -3 (0 pour le bleu et 3 pour le rouge).

Des trois choix, le bleu choisit celui qui **maximise** l'évaluation finale : il
joue donc entre les deux premières cases.

L'évaluation de l'état du jeu avant le coup du bleu est donc 1, car le bleu
pourra nécessairement atteindre ce score.

### Un peu de recul

Regardons maintenant la situation un coup plus tôt. C'est au rouge de jouer et
il a quatre choix (figure 3).

![Figure 3: C'est au rouge de jouer](IMGS/img_bgrbwrrggwggbrw.png){height=30}

Les issues de cette partie sont représentées sur la figure 4.

![Figure 4: Évaluation de fin de partie s'il reste 4 coups à jouer, 2 situations
ont déjà été envisagées (fond jaune)](IMGS/graphe4_cadres.png){height=22cm}

Le premier choix (fermeture du carré de gauche) l'amènera à
une des situations examinée précédemment (figure 1) car ce sera encore son tour.
Nous savons que cette première situation est évaluée à -1.

Le quatrième choix dans l'arbre précédent consiste à jouer le segment du bas de
la case du milieu. Cette issue a déjà été évaluée à 1, car le bleu choisira
ensuite l'option qui maximise le score (figure 2).

Par un raisonnement similaire, les deux autres choix (jouer dans la case de
droite) sont évalués tous les deux à 3 (voir figure 4).

Des quarte choix, le rouge choisit donc le coup qui minimise l'évaluation du
jeu. 
C'est le premier coup examiné (score -1). En faisant ce choix, il sait qu'il
aboutira au pire au score -1 (si le bleu joue au mieux) et qu'il gagnera ainsi la partie.
L'évaluation de la situation de départ de la figure 3 est donc : -1.

## Représentation de l'état du jeu

### Classe `Board`

Pour examiner les positions de jeu, il faut un moyen d'en représenter l'état en
cours de partie. Plusieurs options sont possibles. Nous proposons ici d'écrire une classe
`Board` dont une instance représentera une position de jeu :

- représentation de la grille de jeu , stockant la position de chaque segment
  tracé (il est inutile de savoir qui l'a tracé, mais simplement si le segment
  est tracé ou non)
- numéro du prochain joueur qui va jouer. On propose de les numéroter : 0 (par
  exemple pour le bleu) et 1

<!-- - nombre de carrés déjà fermés par le joueur 0 et le joueur 1 (une liste de deux
 valeurs)-->

### Grille de jeu

 Pour représenter la position des segments tracés, une simple liste peut
 convenir. Chaque élément représentera une case du jeu et indiquera les segments
 tracés autour de cette case avec un code
 particulier. En affectant par exemple la valeur 1 au segment haut, 2 au segment droit, 4 au segment
 bas et 8 au segment gauche, un nombre entier entre 0 et 15 représente alors de
 manière biunivoque chacune des 16 configurations de segments possibles.
  
![](IMGS/img_ggggw.png){height=30} : pas de segment ⭢ 0

![](IMGS/img_bggrw.png){height=30} : segment gauche et haut ⭢ 9 (= 8 + 1)

![](IMGS/img_brbrr.png){height=30} : tous les segments ⭢ 15 (= 8 + 4 + 2 + 1)

 ![](IMGS/img_bggrwggbgwggggw.png){height=30} est représenté par la liste `[9, 4,
 0]`
 
 Il est ainsi très simple de tracer des segments ou de vérifier si des segment
 sont tracés avec des opérateurs booléens (fiches 5 et 6 NSI 1ere).
 L'inconvénient de cette représentation est
 que certains segments appartiennent à 2 cases à la fois. Il faut donc penser,
 lors d'un tracé de segment, à mettre à jour 2 éléments de la liste (à cet
 effet, une table précalculée des règles de voisinage pourra rendre le code
 plus rapide). Voici ce qu'on obtient en ajoutant un segment à la grille
 représentée par `[9, 4, 0]` :

 ![](IMGS/img_brgrwggbrwggggw.png){height=30} est représenté par la liste `[11, 12, 0]`
 
## Algorithme du Minimax

### Algorithme général

La fonction principale, qui calcule le meilleur coup, prend en paramètre 
un plateau de jeu et renvoie le score optimal et le
coup optimal (ou `None` à la place du coup optimal s'il n'y a plus de coup à
jouer en fin de partie). Le paramètre est de la classe `Board` (voir section
précédente), et contient le numéro du joueur qui va jouer.

~~~text
fonction calcule(board: Board) ⭢ (entier, coup):
    si board.partie_finie()
        renvoyer (board.score(), None)

    choix_coups ⭠ []
    pour chaque coup dans board.coups_possibles():
        new_board ⭠ board.joue_coup(coup)
        score, coupsuivant ⭠ calcule(new_board)
        choix_coups.append((score, coup))

    si board_numero_joueur == 0: # c'est le tour du bleu
        meilleur_choix ⭠ max(choix_coups)
    sinon:
        meilleur_choix ⭠ min(choix_coups)
    renvoyer meilleur_choix
~~~

On suppose ici que la fonction `min` (resp. `max`) est capable de sélectionner 
un couple dont le premier élément est minimum (rep. maximum).
<!--sont similaires à celles de
Python. Dans le cas de tuples `(score, coup)` elles minimiseront ou maximiseront
le premier élémént du tuple en premier (donc le score).-->

Voici les éléments utilisés dans l'algorithme :

- La méthode `board.partie_finie()` indique si la partie est terminée. Pour
  accélérer les calculs, il vaut mieux avoir un compteur qui indique le nombre
  de segments déjà tracés, puisqu'on peut en déduire facilement si la partie est
  finie.
- La méthode `board.score()` évalue la fin de partie : nombre de carrés fermés par le 
  bleu moins le nombre de carrés fermés par le rouge. Là aussi, il sera plus efficace
  de mettre à jour des compteurs dans la classe `Board` à chaque fois qu'un carré
  est fermé par un joueur.
- La méthode `board.coup_possible()` renvoie la liste des coups jouables
  actuellement sur la grille.
- La méthode `board.joue_coup(coup)` prend la description d'un coup en
  paramètre (par exemple un tuple contenant le numéro de la case à jouer et la valeur
  (1, 2, 4 ou 8) affectée au segment à tracer) et renvoie la nouvelle représentation du
  jeu.
  

### Méthode `Board.joue_coup`

La méthode `board.joue_coup(coup)` est probablement la plus délicate. Elle prend
en paramètre la description d'un coup et renvoie le nouvel
état du jeu tel qu'il sera une fois le coup joué. Il est important de noter que
cette méthode ne fait pas **muter** l'objet `board`, mais renvoie un nouvel
état (l'objet `board` ne doit pas muter entre deux tours de boucle de
l'algorithme du minimax).

La méthode devra entre autres :

- veiller à ce que la représentation de la grille reste
consistante (si on joue le segment de droite d'une case, le segment de gauche de
la case située juste à droite est joué aussi) ;
- mettre à jour le score si le coup joué ferme un carré (avec un segment joué,
  on peut parfois fermer 2 carrés !) ;
- alterner correctement le numéro du prochain joueur (si un carré est fermé, le
même joueur rejoue, sinon, on passe à l'autre joueur).

### Quelques conseils 

Lors de l'écriture de la méthode `board.joue_coup`, il faut être
vigilant sur les points suivants :

- génération du nouveau `board` à partir de l'ancien lorsqu'un coup est
  joué (il faudra probablement recopier tous les attributs de l'instance dans le
  nouvel objet)
- erreurs possibles dans la gestion du voisinage des segments (les segments internes sont
  communs à deux cases, les segment externes non, il faut y faire attention)
- problèmes de performances : il n'y a aucune étape très complexe, mais elles
  sont exécutées un nombre considérable de fois, et une mauvaise optimisation
  peut conduire à des résultats catastrophiques en terme de temps de calcul.
  Ce point n'est pas crucial dans
  un premier temps, mais il sera sûrement à prendre en compte à un moment.

Il est conseillé de tester le programme sur de toutes petites grilles (une
ou deux cases uniquement), avant de s'attaquer aux éventuels problèmes de
performance.

Lors de l'énumération des coups possibles, il faudra tenir compte du fait que
jouer le segment de gauche dans la case 1 est le même coup que jouer le segment
de droite de la case 0 (et ce coup a peut être déjà été évalué).

## Améliorations possibles

### Calcul partiel de l'arbre

Calculer un arbre de profondeur 10 peut être relativement long. C'est pourtant 
ce qu'il faut pour que la machines joue sur une petite grille 1x3 : l'arbre complet
d'une telle partie contient plus de 3 millions de noeuds. Aussi, il est
courant de ne pas mener le calcul de l'arbre jusqu'à la fin de la partie. Dans
ce cas, on utilise une évaluation de la situation avant la fin de partie, en
tant que score. Ici, très
simplement, le nombre de carrés déjà fermés par le joueur bleu moins le
nombre de carrés déjà fermés par le joueur rouge peut être une première
approche (pour certains jeux, il peut être beaucoup plus difficile de déterminer
qui a l'avantage en cours de partie, ici on dispose au moins de cet indicateur).

Pour aller encore plus loin, il faudrait tenir compte des opportunités
qui s'offrent au joueur, par exemple être dans la situation où on peut fermer
toutes les cases d'un couloir (en jouant plusieurs coups d'affilée) est très avantageux.

### Coupures $\alpha$ $\beta$

Supposons que sur une grille 2x2, chaque joueur ait déjà fermé un carré. 
C'est au tour du joueur bleu. Le score maximum qu'il peut espérer est 2 (3 pour
lui, 1 pour le rouge). Si l'évaluation d'une des possibilités qui s'offrent au
joueur bleu donne ce score, c'est l'option à prendre, et il est inutile
d'évaluer les autres branches.

La même chose se produit pour le joueur rouge.

Cet «élagage» au niveau des branches de l'arbre à évaluer effectivement
s'appelle élagage $\alpha\beta$ et il diminue considérablement la quantité de
calculs à réaliser.

### Interface graphique

Il sera naturellement plus agréable de jouer à la pipopipette si on dispose
d'une interface graphique (tkinter par exemple).

## Résultats attendus

En réalisant le projet avec soin, et avec quelques optimisations, le jeu
sur une petite grille peut être complètement résolu :

- sur une grille 1x2, le premier joueur ne peut espérer mieux qu'un match nul.
  <!-- en jouant le segment du milieu -->
  En revanche si son premier coup est le mauvais (il n'y a qu'un coup qui peut
  lui assurer le match nul), alors le second joueur est sûr de gagner.
- sur une grille 1x3, le second joueur dispose systématiquement d'une stratégie gagnante.
- sur une grille 2x2, le joueur qui commence dispose d'une stratégie gagnante.