
## Un exemple de faille de sécurité : injection SQL

Il est  très important  de sécuriser  ses BDD qui  peuvent contenir  des données
sensibles. Or il est courant de  demander aux utilisateurs des données (nom, mot
de passe,...)  qui seront introduites  dans la base. Un  utilisateur malveillant
pourrait alors entrer du code SQL au lieu de son nom et détruire la base ou bien
récupérer des données.

Par exemple, si l'on  demande d'entrer un pseudo sans précaution  via ce code en
Python avec la bibliothèque `bottle-sqlite` qui sera introduite en exercice :

```python
p = input('Quel est votre pseudo ?')
db.execute(f"SELECT * FROM inscrits WHERE pseudo={p}");
```

Si un utilisateur rentre le pseudo 
`\'Joe\'; DROP TABLE inscrits;` alors
la requête exécutée est :

```sql
SELECT * FROM inscrits WHERE pseudo = 'Joe'; DROP TABLE inscrits;
```

et la table est détruite !

L'idée   est   de   faire   vérifier    les   entrées   avant   de   lancer   la
requête, par exemple  à l'aide d'*expressions régulièreres* ou  en utilisant des
*paramètres* SQL comme ici :

```python
db.execute('SELECT * FROM inscrits WHERE pseudo=?', pseudo)
```

SQLite va  vérifier que le paramètre  ne contient aucun code  créant une requête
**avant** d'exécuter la requête `SELECT` (requête préparée).
