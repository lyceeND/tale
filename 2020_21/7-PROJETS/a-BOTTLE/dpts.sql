BEGIN TRANSACTION;



CREATE TABLE IF NOT EXISTS capitales(
  num_region TEXT NOT NULL,
  ville TEXT NOT NULL,
  PRIMARY KEY  (num_region)
);

INSERT INTO capitales VALUES ('1', 'Strasbourg');
INSERT INTO capitales VALUES ('2', 'Bordeaux');
INSERT INTO capitales VALUES ('3', 'Lyon');
INSERT INTO capitales VALUES ('4', 'Rouen');
INSERT INTO capitales VALUES ('5', 'Dijon');
INSERT INTO capitales VALUES ('6', 'Rennes');
INSERT INTO capitales VALUES ('7', 'Orléans');
INSERT INTO capitales VALUES ('8', 'Ajaccio');
INSERT INTO capitales VALUES ('10', 'Paris');
INSERT INTO capitales VALUES ('11', 'Toulouse');
INSERT INTO capitales VALUES ('12', 'Lille');
INSERT INTO capitales VALUES ('13', 'Nantes');
INSERT INTO capitales VALUES ('14', 'Marseille');


CREATE TABLE IF NOT EXISTS regions(
  num_old_region TEXT NOT NULL,
  nom TEXT NOT NULL,
  num_region TEXT NOT NULL,
  PRIMARY KEY  (num_old_region),
  FOREIGN KEY(num_old_region) REFERENCES capitales(num_region)
);

INSERT INTO regions VALUES ('1', 'Grand-Est', '1');
INSERT INTO regions VALUES ('2', 'Nouvelle-Aquitaine', '2');
INSERT INTO regions VALUES ('3', 'Auvergne-Rhône-Alpes', '3');
INSERT INTO regions VALUES ('4', 'Normandie', '4');
INSERT INTO regions VALUES ('5', 'Bourgogne-Franche-Comté', '5');
INSERT INTO regions VALUES ('6', 'Bretagne', '6');
INSERT INTO regions VALUES ('7', 'Centre-Val-de-Loire', '7');
INSERT INTO regions VALUES ('8', 'Grand-Est', '1');
INSERT INTO regions VALUES ('9', 'Corse', '8');
INSERT INTO regions VALUES ('10', 'Bourgogne-Franche-Comté', '5');
INSERT INTO regions VALUES ('11', 'Normandie', '4');
INSERT INTO regions VALUES ('12', 'Ile-de-France', '10');
INSERT INTO regions VALUES ('13', 'Occitanie', '11');
INSERT INTO regions VALUES ('14', 'Nouvelle-Aquitaine', '2');
INSERT INTO regions VALUES ('15', 'Grand-Est', '1');
INSERT INTO regions VALUES ('16', 'Occitanie', '11');
INSERT INTO regions VALUES ('17', 'Hauts-de-France', '12');
INSERT INTO regions VALUES ('18', "Provence-Alpes-Côte-d'Azur", '14');
INSERT INTO regions VALUES ('19', 'Pays-de-la-Loire', '13');
INSERT INTO regions VALUES ('20', 'Hauts-de-France', '12');
INSERT INTO regions VALUES ('21', 'Nouvelle-Aquitaine', '2');
INSERT INTO regions VALUES ('22', 'Auvergne-Rhône-Alpes', '3');





CREATE TABLE IF NOT EXISTS old_regions(
  num_old_region TEXT NOT NULL,
  nom TEXT NOT NULL,
  PRIMARY KEY  (num_old_region),
  FOREIGN KEY(num_old_region) REFERENCES regions(num_old_region)
);


INSERT INTO old_regions VALUES ('1', 'Alsace');
INSERT INTO old_regions VALUES ('2', 'Aquitaine');
INSERT INTO old_regions VALUES ('3', 'Auvergne');
INSERT INTO old_regions VALUES ('4', 'Basse Normandie');
INSERT INTO old_regions VALUES ('5', 'Bourgogne');
INSERT INTO old_regions VALUES ('6', 'Bretagne');
INSERT INTO old_regions VALUES ('7', 'Centre');
INSERT INTO old_regions VALUES ('8', 'Champagne Ardenne');
INSERT INTO old_regions VALUES ('9', 'Corse');
INSERT INTO old_regions VALUES ('10', 'Franche Comte');
INSERT INTO old_regions VALUES ('11', 'Haute Normandie');
INSERT INTO old_regions VALUES ('12', 'Ile de France');
INSERT INTO old_regions VALUES ('13', 'Languedoc Roussillon');
INSERT INTO old_regions VALUES ('14', 'Limousin');
INSERT INTO old_regions VALUES ('15', 'Lorraine');
INSERT INTO old_regions VALUES ('16', 'Midi-Pyrénées');
INSERT INTO old_regions VALUES ('17', 'Nord Pas de Calais');
INSERT INTO old_regions VALUES ('18', "Provence Alpes Côte d'Azur");
INSERT INTO old_regions VALUES ('19', 'Pays de la Loire');
INSERT INTO old_regions VALUES ('20', 'Picardie');
INSERT INTO old_regions VALUES ('21', 'Poitou Charente');
INSERT INTO old_regions VALUES ('22', 'Rhone Alpes');


CREATE TABLE IF NOT EXISTS departements(
  num_departement TEXT NOT NULL,
  num_old_region TEXT NOT NULL,
  nom  TEXT NOT NULL,
  PRIMARY KEY(num_departement),
  FOREIGN KEY(num_old_region) REFERENCES old_regions(num_old_region)
);


INSERT INTO departements VALUES ('01', '22', 'Ain');
INSERT INTO departements VALUES ('02', '20', 'Aisne');
INSERT INTO departements VALUES ('03', '3', 'Allier');
INSERT INTO departements VALUES ('04', '18', 'Alpes de haute provence');
INSERT INTO departements VALUES ('05', '18', 'Hautes alpes');
INSERT INTO departements VALUES ('06', '18', 'Alpes maritimes');
INSERT INTO departements VALUES ('07', '22', 'Ardèche');
INSERT INTO departements VALUES ('08', '8', 'Ardennes');
INSERT INTO departements VALUES ('09', '16', 'Ariège');
INSERT INTO departements VALUES ('10', '8', 'Aube');
INSERT INTO departements VALUES ('11', '13', 'Aude');
INSERT INTO departements VALUES ('12', '16', 'Aveyron');
INSERT INTO departements VALUES ('13', '18', 'Bouches du rhône');
INSERT INTO departements VALUES ('14', '4', 'Calvados');
INSERT INTO departements VALUES ('15', '3', 'Cantal');
INSERT INTO departements VALUES ('16', '21', 'Charente');
INSERT INTO departements VALUES ('17', '21', 'Charente maritime');
INSERT INTO departements VALUES ('18', '7', 'Cher');
INSERT INTO departements VALUES ('19', '14', 'Corrèze');
INSERT INTO departements VALUES ('21', '5', "Côte d'or");
INSERT INTO departements VALUES ('22', '6', "Côtes d'Armor");
INSERT INTO departements VALUES ('23', '14', 'Creuse');
INSERT INTO departements VALUES ('24', '2', 'Dordogne');
INSERT INTO departements VALUES ('25', '10', 'Doubs');
INSERT INTO departements VALUES ('26', '22', 'Drôme');
INSERT INTO departements VALUES ('27', '11', 'Eure');
INSERT INTO departements VALUES ('28', '7', 'Eure et Loir');
INSERT INTO departements VALUES ('29', '6', 'Finistère');
INSERT INTO departements VALUES ('30', '13', 'Gard');
INSERT INTO departements VALUES ('31', '16', 'Haute garonne');
INSERT INTO departements VALUES ('32', '16', 'Gers');
INSERT INTO departements VALUES ('33', '2', 'Gironde');
INSERT INTO departements VALUES ('34', '13', 'Hérault');
INSERT INTO departements VALUES ('35', '6', 'Ile et Vilaine');
INSERT INTO departements VALUES ('36', '7', 'Indre');
INSERT INTO departements VALUES ('37', '7', 'Indre et Loire');
INSERT INTO departements VALUES ('38', '22', 'Isère');
INSERT INTO departements VALUES ('39', '10', 'Jura');
INSERT INTO departements VALUES ('40', '2', 'Landes');
INSERT INTO departements VALUES ('41', '7', 'Loir et Cher');
INSERT INTO departements VALUES ('42', '22', 'Loire');
INSERT INTO departements VALUES ('43', '3', 'Haute Loire');
INSERT INTO departements VALUES ('44', '19', 'Loire Atlantique');
INSERT INTO departements VALUES ('45', '7', 'Loiret');
INSERT INTO departements VALUES ('46', '16', 'Lot');
INSERT INTO departements VALUES ('47', '2', 'Lot et Garonne');
INSERT INTO departements VALUES ('48', '13', 'Lozère');
INSERT INTO departements VALUES ('49', '19', 'Maine et Loire');
INSERT INTO departements VALUES ('50', '4', 'Manche');
INSERT INTO departements VALUES ('51', '8', 'Marne');
INSERT INTO departements VALUES ('52', '8', 'Haute Marne');
INSERT INTO departements VALUES ('53', '19', 'Mayenne');
INSERT INTO departements VALUES ('54', '15', 'Meurthe et Moselle');
INSERT INTO departements VALUES ('55', '15', 'Meuse');
INSERT INTO departements VALUES ('56', '6', 'Morbihan');
INSERT INTO departements VALUES ('57', '15', 'Moselle');
INSERT INTO departements VALUES ('58', '5', 'Nièvre');
INSERT INTO departements VALUES ('59', '17', 'Nord');
INSERT INTO departements VALUES ('60', '20', 'Oise');
INSERT INTO departements VALUES ('61', '4', 'Orne');
INSERT INTO departements VALUES ('62', '17', 'Pas de Calais');
INSERT INTO departements VALUES ('63', '3', 'Puy de Dôme');
INSERT INTO departements VALUES ('64', '2', 'Pyrénées Atlantiques');
INSERT INTO departements VALUES ('65', '16', 'Hautes Pyrénées');
INSERT INTO departements VALUES ('66', '13', 'Pyrénées Orientales');
INSERT INTO departements VALUES ('67', '1', 'Bas Rhin');
INSERT INTO departements VALUES ('68', '1', 'Haut Rhin');
INSERT INTO departements VALUES ('69', '22', 'Rhône');
INSERT INTO departements VALUES ('70', '10', 'Haute Saône');
INSERT INTO departements VALUES ('71', '5', 'Saône et Loire');
INSERT INTO departements VALUES ('72', '19', 'Sarthe');
INSERT INTO departements VALUES ('73', '22', 'Savoie');
INSERT INTO departements VALUES ('74', '22', 'Haute Savoie');
INSERT INTO departements VALUES ('75', '12', 'Paris');
INSERT INTO departements VALUES ('76', '11', 'Seine Maritime');
INSERT INTO departements VALUES ('77', '12', 'Seine et Marne');
INSERT INTO departements VALUES ('78', '12', 'Yvelines');
INSERT INTO departements VALUES ('79', '21', 'Deux Sèvres');
INSERT INTO departements VALUES ('80', '20', 'Somme');
INSERT INTO departements VALUES ('81', '16', 'Tarn');
INSERT INTO departements VALUES ('82', '16', 'Tarn et Garonne');
INSERT INTO departements VALUES ('83', '18', 'Var');
INSERT INTO departements VALUES ('84', '18', 'Vaucluse');
INSERT INTO departements VALUES ('85', '19', 'Vendée');
INSERT INTO departements VALUES ('86', '21', 'Vienne');
INSERT INTO departements VALUES ('87', '14', 'Haute Vienne');
INSERT INTO departements VALUES ('88', '15', 'Vosges');
INSERT INTO departements VALUES ('89', '5', 'Yonne');
INSERT INTO departements VALUES ('90', '10', 'Territoire de Belfort');
INSERT INTO departements VALUES ('91', '12', 'Essonne');
INSERT INTO departements VALUES ('92', '12', 'Hauts de Seine');
INSERT INTO departements VALUES ('93', '12', 'Seine Saint Denis');
INSERT INTO departements VALUES ('94', '12', 'Val de Marne');
INSERT INTO departements VALUES ('95', '12', "Val d'Oise");
INSERT INTO departements VALUES ('20', '9', 'Corse');

COMMIT;