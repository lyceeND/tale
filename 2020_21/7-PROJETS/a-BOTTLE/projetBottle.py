from bottle import route, post, install, template, request, run
from bottle_sqlite import SQLitePlugin

install(SQLitePlugin(dbfile='./baseEleves.db'))

# La racine du site
@route('/')
def index():
   return template('index.tpl')

# Pour créer les bases à la première utilisation
@post('/creation')
def creation(db):
    db.execute("""
    CREATE TABLE IF NOT EXISTS eleves (
       id INTEGER PRIMARY KEY AUTOINCREMENT,
       prenom TEXT, 
       nom TEXT, 
       nais TEXT, 
       adr TEXT, 
       ville TEXT, 
       cp TEXT,  
       email TEXT,
       UNIQUE(nom, prenom, email)
       )
       """)
    print("Table élèves créée avec succès")
    with open('./baseDpts.sql', 'r') as dpt:
        d = dpt.read() 
    db.executescript(d)
    print('Tables Departements, Regions créées avec succès')
    return template('index.tpl')
 
 # le formulaire d'inscription
@route('/entreNouveau')
def entreNouveau():
   return template('eleve.tpl')

# inscription des données du formulaire dans la BDD
@post('/ajouteEntree')
def ajouteEntree(db):
    prenom = request.forms['prenom']
    nom = request.forms['nom']
    nais = request.forms['nais']
    adr = request.forms['adr']
    ville = request.forms['ville']
    cp = request.forms['cp']
    email = request.forms['email']    
    db.execute("""
        INSERT INTO eleves (prenom, nom, nais, adr, ville, cp, email)
         VALUES (?,?,?,?,?,?,?)
        """,(prenom, nom, nais, adr, ville, cp, email)
        )
    return template("index.tpl")

@route('/liste')
def liste(db):
    req = db.execute("select * from eleves")
    lignes = req.fetchall()
    return template("liste.tpl", lignes=lignes)

# moteur de recherche
#@route('/recherche')
#def recherche():
#   return template('recherche.tpl')

# resultats du moteur de recherche
#@post('/requete')
#def requete(db):

# centres d'examens 
#@route('/centreExamens')
#def centres(db):

# on ouvre un serveur en local sur le port 7000 par exemple
if __name__ == '__main__':
   run(reloader = True, debug = True, host='127.0.0.1', port=7000)



