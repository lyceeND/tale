# PROJET BOTTLE

## Préliminaires : construction d'une base de travail


### Construire une base de données



On voudrait inscrire des élèves en ligne en entrant les renseignements suivants :


```mermaid
classDiagram
    class Eleves{
        Text prenom 
        Text nom 
        Text nais 
        Text adr
        Text ville
        Text email
    }
```

Cette relation a l'inconvénient de ne pas avoir de clé primaire. On ajoutera donc  un identifiant numérique unique sous la forme d'un entier.
Pour ne pas à devoir manipuler directement cet identifiant, on laissera `SQLite` choisir et mettre à jour l'identifiant grâce à `AUTOINCREMENT`. Il suffira de créer la colonne avec l'argument :

```sql
id INTEGER PRIMARY KEY AUTOINCREMENT
```
Donner alors la commande SQLite permettant de créer la table `eleves` selon le schéma précédemment introduit.

Cependant, cette méthode a un inconvénient : lequel ? Regarder quel est le rôle de la contrainte `UNIQUE` et remédier à ce problème.



### Rechercher dans une table selon un motif 

On voudrait effectuer des recherches dans la table élèves selon certains motifs (noms qui commencent par "abs", habitants de la Loire-Atlantique, élèves nés en 2003, etc.).

On peut pour cela utiliser `WHERE` et même `WHERE...LIKE...` avec des *wildcards* (quantificateurs). Les plus utiles sont :
* `%` qui représente 0, 1 ou plusieurs caractères quelconques;
* `_` qui représente un et un seul caractère quelconque.

Par exemple `%s` permettra d'avoir toutes les chaînes qui finissent par *s*, `ro_e` permettra d'avoir *robe*, *rose*,...

La syntaxe est :

```sql
SELECT
	liste_de_colonnes
FROM
	nom_de_la_table
WHERE
	colonne LIKE motif;
```

On pourra utiliser la fonction `substr(chaine, deb, fin)` qui extraît une sous-chaîne de `chaine` du caractère numéro `deb` jusqu'au caractère numéro `fin', sachant que le premier caractère porte le numéro 1.

On pourra enfin utiliser `IN...(....)` pour vérifier qu'un champ est élément d'une liste ou d'une sous-requête:

```sql
expression [NOT] IN (liste_de_valeurs|sous_requete);
```

1. Définir une requête qui permet d'obtenir tous les élèves nés en 2002.
2. Définir une requête qui permet d'obtenir tous les élèves ayant un compte e-mail chez l'opérateur sql (du type `pseudo@sql.fr`).
3. Définir une requête qui permet d'obtenir les élèves dont l'adresse e-mail commence par la première lettre de leur prénom.
4. Définir une requête qui permet d'obtenir tous les élèves habitant en Bretagne.
 




### Effectuer des jointures

Un élève doit passer des examens dans la capitale régionale dont dépend son domicile. Mais le ministère ne dispose que de vieilles tables qui correspondaient aux anciennes régions puis de nouvelles tables ajoutées pour s'adapter au nouveau découpage. Voici leur structure:


```mermaid
classDiagram
    class Departements{
        Text num_departement
        Text num_old_region
        Text nom_departement
    }
    class Old_regions{
        Text num_old_region
        Text nom_old_region
    }
    class New_regions{
        Text num_old_region
        Text nom_new_region
        Text num_new_region
    }
    class Capitales{
        Text num_new_region
        Text ville
    }
```



Ainsi 
* une ligne de `Departements` sera : `('01', '22', 'Ain')`
* une ligne de `Old_regions` sera : `('14', 'Limousin')`
* une ligne de New_regions` sera : `('14', 'Nouvelle-Aquitaine', '2')` 
* une ligne de `Capitales` sera : `('13', 'Nantes')`

1. Quelles pourraient être les clés primaires et éventuellement étrangères de ces tables ?
2. Simplifier ces tables pour qu'elles ne dépendent plus des anciennes régions. Donner les requêtes `SQL` nécessaires.
3. Récuperer le fichier initial `dpts.sql` et effectuer les changements proposés. Enregistrer ensuite les deux  tables restantes dans un fichier SQL nommé `baseDpts.sql` dont nous aurons besoin dans l'exercice **Gérer une base de données SQLite avec Python**.

### Découvrir de nouvelles clauses 

On dispose des tables `Eleves`, `Departements` et `Regions` construites précédemment.

1. On voudrait créer une table comportant l'identifiant, le prénom, le nom et le centre d'examen des élèves inscrits. Le centre d'examen est la capitale de la région à laquelle appartient la ville où habite l'élève (colonne `ville` dans `Eleves`).



```mermaid
classDiagram
    class Centres_eleves{
        Int id
        Text prenom
        Text nom
        Text centre
    }
```




2. Regarder la documentation de la clause `GROUP BY` et de la fonction `COUNT` pour renvoyer la table des centres d'examen et du nombre de candidats inscrits dans ces centres.


```mermaid
classDiagram
    class Centres_stats{
        Text centre
        Int effectif
    }
```













## Gérer une base de données SQLite avec un *framework* Python

Nous allons utiliser Python pour automatiser la gestion de nos BDD avec le *web framework* (ou environnement de développement web) `bottle` (`https://bottlepy.org`).

On installera aussi le *plugin* (greffon) `bottle-sqlite` qui facilitera le dialogue avec la base de données SQLite associée et évite ainsi de connaître en profondeur la bibliothèque Python `sqlite3`.

```console
$ python3.x -m pip install bottle bottle-sqlite
```
Nous importerons ensuite quelques fonctions depuis ces bibliothèques :

```python
from bottle import route, post, install, template, request, run
from bottle_sqlite import SQLitePlugin


# Code principal qui va être construit au cours de l'exercice

# on ouvre un serveur en local sur le port 7000 par exemple
if __name__ == '__main__':
   run(reloader = True, debug = True, host='127.0.0.1', port=7000)
```

Les deux dernières lignes permettent de lancer un serveur local sur le port 7000. Il suffira ensuite d'ouvrir un navigateur et de taper l'adresse `http://localhost:7000/` qui sera la racine du site que nous allons fabriquer.


Maintenant, un plan d'action : il s'agit de manipuler les tables créées précédemment et d'effectuer des recherches dans ces tables via des applications web.

Il nous faut donc:
* une page d'index pour commencer à naviguer (`index.html`);
* une page contenant un formulaire HTML permettant de créer un élève dans la base (`eleve.html`);
* une page contenant une zone de texte où l'utilisateur rentre sa recherche (`recherche.html`);
* une page pour afficher le résultat de la recherche (`requete.html`)
* une page pour afficher tous les inscrits (`liste.html`)

Ces pages dépendent des données entrées et d'éventuelles variables Python. On va donc, plutôt que des pages HTML, créer des *templates* (des modèles, des gabarits) qui permettront de construire ensuite les pages HTML. Pour les distinguer, on leur donnera l'extension `tpl` et on les placera dans un sous-répertoire nommé `views`. Cela peut même être une page écrite exclusivement en HTML comme nous allons le voir tout de suite.



1. Créer un formulaire HTML (voir [cette initiation](https://developer.mozilla.org/fr/docs/Learn/Forms/Your_first_form)) qui permettra d'entrer les différents attributs d'un élève (cf première question). Il enverra les valeurs à la page `/ajouteEntree`. Il aura cette structure :

```html
<!doctype html>
<html  lang="fr-FR">
   <head><meta charset="utf-8"></head>
   <body>
      
      <form action = "/ajouteEntree" method = "POST">
         <h3>Données de l'élève</h3>
         <label for = "prenom">Prénom</label>
         <input type = "text" name = "prenom" id = "prenom" required/></br>
         .
         .
         .
      </form>
      <a href = "/">Retourner à la page d'accueil</a>
   </body>
</html>
```

2. On veut maintenant récupérer ces valeurs et les placer dans la base de données. 

Pour demander une valeur, on utilise tout simplement `request.forms[name]`. En effet, `request.forms` renvoie un objet dont la structure est celle d'un dictionnaire, les clés étant le nom (`name`) du champ du formulaire qui est associée à sa valeur.


Ainsi `var = request.forms['nom'] permet de récupérer la valeur associée à `nom` rentré par l'utilisateur et on la lie à la variable Python `var` par exemple.

Une fois récupérées, ces valeurs doivent être insérées dans la base de données avec une commande SQL introduite par `db.execute('requête SQL')`. Nous allons voir plus bas que `db` est un objet représentant la base de donnée manipulée.

Créer la requête selon ce modèle :

```python
@post('/ajouteEntree')
def ajouteEntree(db):
    prenom = request.forms['prenom']
    .
    .
    .
    db.execute(...)
    return template("index.tpl")
```
Veiller à éviter les injections SQL en utilisant une requête préparée (fiche 26).
On renvoie à la fin à la page d'index (`return template("index.tpl")`).

3. Voici comment obtenir la liste des élèves inscrits.
    * On va récupérer tous les inscrits dans la table `eleves`. On passe ces lignes (dans la variable `lignes` ici) à la page de template :

```python
    @route('/liste')
    def liste(db):
        req = db.execute("select * from eleves")
        lignes = req.fetchall()
        return template("liste.tpl", lignes=lignes)
```

Il reste à fabriquer la page `liste.tpl`. On va découvrir la puissance des *templates* à cette occasion...

Toute commande qui suit un `%` placé directement après des espaces est interprétée comme une commande Python. Toute variable passée en argument ou introduite dans la page *template* est placée entre doubles accolades. On peut donc construire dynamiquement le tableau  de la page HTML résultante :


```html
<!doctype html>
<html  lang="fr-FR">
<head><meta charset="utf-8"></head>
<body>   
% titres = ["id", "Prénom", "Nom", "Date de naissance", "Adresse", "Ville", "Code postal", "E-mail"]
<table border = 1>
  <thead>
    %for titre in titres:
    <td>{{titre}}</td>
    %end
  </thead>
    % for ligne in lignes:
        <tr>
        %for col in ligne:
           <td>{{col}}</td>
        %end
        </tr>
    % end
</table>
<h2><a href = "/entreNouveau">Retourner à la page d'enregistrement</a></h2>
<h2><a href = "/">Retourner à la page d'accueil</a></h2>
</body></html>
```

Construire alors un moteur de recherche dans la table élève. Il faudra choisir une colonne à l'aide d'un menu déroulant et entrer un motif de recherche.  On utilisera les résultats de l'exercice 2.



4. Expliquer pourquoi effectuer la requête :

```python
   db.execute("SELECT * FROM eleves WHERE nom LIKE " + str(rekete) )
```

au lieu d'une requête préparée peut  présenter un danger. Donner une requête qui
permettrait à un utilisateur malveillant de détruire la table `eleves`.  


5. On voudrait à présent associer à chaque inscrit son centre d'examen comme vu dans l'exercice 4. Donner le code nécessaire en combinant les connaissances introduites jusqu'à présent.


