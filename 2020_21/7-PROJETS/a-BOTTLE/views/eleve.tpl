<!doctype html>
<html  lang="fr-FR">
   <head><meta charset="utf-8"></head>
   <body>
      
      <form action = "/ajouteEntree" method = "POST">
         <h3>Données de l'élève</h3>
         Prénom<br>
         <input type = "text" name = "prenom" id = "prenom" required/></br>
         
         Nom<br>
         <input type = "text" name = "nom" id = "nom" required/></br>
         
         Date de naissance<br>
         <input type = "text" name = "nais" id = "nais" placeholder = "2002-02-29" pattern="[1-2][0-9]{3}-[0-1][0-9]-[0-3][0-9]" required/></br>

         Adresse<br>
         <textarea name = "adr" id = "adr" placeholder="numéro, rue"></textarea><br>
         
         Ville<br>
         <input type = "text" name = "ville" id = "ville" required/><br>
         
         Code Postal<br>
         <input type = "text" name = "cp" id="cp" pattern="[0-9]{5}" required/><br>

         E-mail<br>
         <input type = "email" name = "email" id="email" required/><br>

         <input type = "submit" value = "Envoyer" id = "envoyer"/><br>
      </form>
      <a href = "/">Retourner à la page d'accueil</a>
   </body>
</html>
