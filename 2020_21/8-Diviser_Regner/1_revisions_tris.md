# Révisions et nouveautés sur les tris


## 1 - Tri sportif


Que pensez-vous de  la complexité de
cet  algorithme  de tri? présenté  et expliqué ci-dessous? 
([source](https://unclecode.blogspot.tw/2012/02/stupid-sort\_2390.html))


```cpp
void StupidSort()
{
    int i = 0;
    while(i < (size - 1))
    {
           if(data[i] > data[i+1])
          {
                int tmp   = data[i];
                data[i]   = data[i+1];
                data[i+1] = tmp;
                i = 0;
          }
          else
         {
               i++;
         }
     }
}
```


## 2 - Mélange de cartes


Savoir bien trier c'est aussi savoir bien mélanger.
  
Afin de convaincre  les utilsateurs que leur algorithme de  mélange était juste,
la société ASF Software, qui produit les logiciels utilisés par de nombreux sites
de jeu, avait publié cet algorithme:

```pascal
procedure TDeck.Shuffle;
var
    ctr: Byte;
    tmp: Byte;

    random_number: Byte;
begin
    { Fill the deck with unique cards }
    for ctr := 1 to 52 do
        Card[ctr] := ctr;

    { Generate a new seed based on the system clock }
    randomize;

    { Randomly rearrange each card }
    for ctr := 1 to 52 do begin
        random_number := random(51)+1;
        tmp := card[random_number];
        card[random_number] := card[ctr];
        card[ctr] := tmp;
    end;

    CurrentCard := 1;
    JustShuffled := True;
end;
```

1. Considérez un jeu de 3 cartes. Dressez l'arbre de tous les mélanges
  possibles en suivant cet algorithme. Que remarquez-vous?
1.  Proposez un algorithme qui corrige ce problème.  Dressez l'arbre
  correspondant pour un jeu de trois cartes.
1.  Traduisez  la fonction  proposée en  Python puis  votre version  corrigée en
    Python.
	
	
	
## 3 - Nouveau tri 

```
Pour i de 1 à n faire
    Pour j de n à i+1 par pas de -1 faire
	    si t[j] < t[j-1] alors
		    échange t[j] et t[j - 1]
	    fin si
	fin pour
fin pour
```


Qu'est-ce que  c'est? Qu'est-ce que ça  fait? Comment ça marche?  Complexité? En
python? Donnez un nom à ce tri.


## 4 - Des tris classiques

### Tri sélectif



On parcourt la liste, on cherche le plus grand et on l'échange avec l'élément le
plus à droite et on recommence avec la liste privée du plus grand élément.


On commence  par chercher l'indice du  maximum d'une liste.  On part de 0  et on
compare à chaque élément  de la liste en faisant évoluer  l'indice du maximum si
nécessaire.


```python
def ind_maxi(xs):
    ind_tmp = 0
    n       = len(xs)
    for i in range(n):
        if ...:
            ind_tmp = ...
    return ind_tmp
```

Ensuite, on copie la liste donnée en argument pour ne pas l'écraser. On parcourt
la liste et on  procède aux échanges éventuels entre le  maximum et l'élément de
droite.


```python
def tri_select(xs):
    cs = xs[:]
    n  = len(cs)
    for i in range(n - 1, 0, -1):
        i_m = ind_maxi(cs[:i + 1])
        if ....:
            cs[...], cs[...] = cs[...], cs[...]
        print(cs) # pour suivre l'évolution
    return cs
```

On  va utiliser `permutation` de  la bibliothèque
`numpy.random` qui renvoie une permutation quelconque.

```python
In [5]: ls = list(permutation(range(10)))

In [6]: ls
Out[6]: [8, 0, 4, 3, 5, 2, 7, 1, 9, 6]

In [7]: tri_select(ls)
[8, 0, 4, 3, 5, 2, 7, 1, 6, 9]
[6, 0, 4, 3, 5, 2, 7, 1, 8, 9]
[6, 0, 4, 3, 5, 2, 1, 7, 8, 9]
[1, 0, 4, 3, 5, 2, 6, 7, 8, 9]
[1, 0, 4, 3, 2, 5, 6, 7, 8, 9]
[1, 0, 2, 3, 4, 5, 6, 7, 8, 9]
[1, 0, 2, 3, 4, 5, 6, 7, 8, 9]
[1, 0, 2, 3, 4, 5, 6, 7, 8, 9]
[0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
Out[7]: [0, 1, 2, 3, 4, 5, 6, 7, 8, 9]
```



Nous  mesurerons  la  complexité  en
nombre de comparaisons.

La complexité de `ind_maxi(xs)` est en $n$ avec $n$ la longueur
de `xs`.

En  effet, il  y a  une comparaison
par itération et $n$ itérations.


Pour `tri_select`, on effectue $n-1$ itérations, et
chacune lance `ind_maxi` sur une liste de longueur $i+1$.

Or
$\displaystyle\sum_{i=1}^{n-1}i+1=\sum_{i=2}^ni=(n-2)\frac{2+n}{2}=\frac{n^2-4}{2}$


```python
In [10]: ls = list(permutation(100))

In [11]: %timeit tri_select(ls)
1000 loops, best of 3: 561 μs per loop

In [12]: ls = list(permutation(200))

In [13]: %timeit tri_select(ls)
100 loops, best of 3: 2.03 ms per loop

In [14]: ls = list(permutation(400))

In [15]: %timeit tri_select(ls)
100 loops, best of 3: 7.89 ms per loop
```


On  observe   effectivement  que  le temps  est environ  multiplié par  4
quand la taille de la liste double.


### Insertion



C'est le  tri d'un jeu de  carte: on
insère un élément dans un tableau trié petit à petit en comparant le
nouvel élément à insérer aux éléments
déjà triés.

La   version  récursive   est  assez
naturelle.   On  commence par  créer
une fonction qui insère
un nouvel élément dans la liste:



```python
def ins_rec(carte, Main):
    if Main == Vide or carte <= tete(Main):
        return [carte] + Main
    return [tete(Main)] + ins_rec(carte, queue(Main))
```


Ensuite,  pour trier,  on insère  la
tête dans la queue triée...

```python
def tri_ins_rec(Pioche):
    if Pioche == Vide:
        return Vide
    return ins_rec(tete(Pioche),tri_ins_rec(queue(Pioche)))
```

À traduire...


Pour la version itérative, on module aussi:


```python
def insere(y, xs):
    cs = xs[:] + [y] # xs est triée et on insère y par la doite
    n = len(xs)
    i = n
    while cs[...] < cs[...] and i ...:
        cs[i - 1], cs[i] = cs[i], cs[i - 1]
        i -= 1
        print(cs)
    return cs

def tri_insere(Pioche):
    Main = Vide
    for carte in Pioche:
        Main = insere(carte, Main)
        print(Main)
    return Main
```
la complexité semble encore
quadratique:

```python
In [29]: ls = list(permutation(100))

In [30]: %timeit tri_insere(ls)
1000 loops, best of 3: 941 μs per loop

In [31]: ls = list(permutation(200))

In [32]: %timeit tri_insere(ls)
100 loops, best of 3: 3.67 ms per loop

In [33]: ls = list(permutation(400))

In [34]: %timeit tri_insere(ls)
100 loops, best of 3: 13.7 ms per loop
```

### Tri fusion : diviser pour régner


Cette  fois,  si le  tableau a  au plus
une  valeur, il  est trié,  sinon on
coupe  le tableau  en deux,  on trie
ces deux moitiés et on fusionne.


```python
def tri_fusion(xs):
    t = len(xs) 
    if t < 2:
        return xs
    return fusion(tri_fusion(xs[:t//2]), tri_fusion(xs[t//2:])) 
```


Il reste à définir la fusion:

```python
def fusion(xs,ys):
    if xs == Vide or ys == Vide:
        return xs + ys
    if tete(xs) < tete(ys):
        return [tete(xs)] + fusion(queue(xs),ys)
    return [tete(ys)] + fusion(xs,queue(ys))
```

La  version récursive  est naturelle
mais pose toujours  des problèmes en
Python:  vous  chercherez  donc  une
version    impérative     à    titre
d'exercice...



### Tri rapide

Observez :

```python
def partition(pivot,seq):
    p, m, g = [], [], []
    for item in seq:
        (p if item < pivot else (g if item > pivot  else m)).append(item)
    return p, m, g

def tri_rapide(xs):
    if estVide(xs):
        return Vide
    else:
        pivot = tete(xs)
        p,m,g = partition(pivot, xs)
        return (tri_rapide(p)) + m + (tri_rapide(g))
```


## 5 - Comparaisons de tris et tracé de graphiques

 Tracez sur un même graphique les temps d'exécution des tris étudiés en fonction de la
  longueur d'une liste «  mélangée ».

  Sur un  autre graphique, comparez ces  tris avec des listes  ordonnées dans le
  sens croissant, puis avec des listes ordonnées dans le sens décroissant.

Pour cela, voici quelques rappels sur `Matplotlib`.

```python
In [29]: import matplotlib.pyplot as plt

In [30]: p1 = plt.plot([1,2,3,4,5],[1,2,5,8,3], marker = 'o',label = "Idiot")

In [31]: p2 = plt.plot([1,2,3,4,5],[3,8,2,6,-1], marker = 'v',label = "Stupide")

In [32]: plt.legend()
Out[32]: <matplotlib.legend.Legend at 0x7fbc2c6b76d8>

In [33]: plt.title("Essai idiot")
Out[33]: <matplotlib.text.Text at 0x7fbc2babc668>

In [34]: plt.xlabel("Le temps")
Out[34]: <matplotlib.text.Text at 0x7fbc2c620898>

In [35]: plt.ylabel("C'est de l'amour")
Out[35]: <matplotlib.text.Text at 0x7fbc2baaa2b0>

In [36]: plt.show()

In [37]: plt.savefig("zig.pdf")

In [38]: plt.clf()
```

![zig](./IMG/zig.png)

Comment mesurer le temps?  On utilise la fonction `perf_counter` de la bibliothèque `time`



```python
from time import perf_counter

def temps(tri,p):
    debut = perf_counter()
    tri(p)
    return perf_counter() - debut
```


On devrait obtenir:



![benchmark](./IMG/benchmark.png)

et en regardant les trois plus rapides:


![benchmark2](./IMG/benchmark2.png)



mais si les listes sont déjà rangées:



![benchmark3](./IMG/benchmark3.png)


