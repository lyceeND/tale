from random import choices

lettres = ['G', 'A']


#ns = ['gatatca', 'gatactata', 'gatacgagac', 'gatac']

def plp_bf(xs: list[str]) -> str:
    """
    Version force brute impérative
    """
    pc = ""
    essai = ""
    rg = 0
    cpt = 0
    while True:
        pc += essai
        essai = xs[0][rg]
        for mot in xs[1:]:
            cpt += 1
            if  rg >= len(mot) or  (mot[rg] != essai):
                print(cpt)
                return pc
        rg += 1
    print(cpt)
    return pc



def prefix_commun(s1: str, s2: str) -> str:
    """
    Renvoie le préfixe commun de deux chaînes
    """
    res = ""
    max_ind = min(len(s1), len(s2))
    i = 0
    while i < max_ind and s1[i] == s2[i]:
        res += s1[i]
        i += 1
    return res


def plus_long_prefix(ns: list[str]) -> str:
    def parcours(lo: int, hi: int) -> str:
        if lo == hi:
            return ns[lo]
        if lo < hi:
            mid = lo + (hi - lo)//2 # (lo + hi)//2
            ns1 = parcours(lo, mid)
            ns2 = parcours(mid + 1, hi)
            return prefix_commun(ns1, ns2)
    return parcours(0, len(ns) - 1)




def main():
    ns = [''.join(choices(lettres, weights=[20,1], k=15)) for _ in range(10)]
    return [plp_bf(ns), plus_long_prefix(ns)]

