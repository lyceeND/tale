## Fonctions d'ordre supérieur


### Mystère

C'est tout simplement une fonction qui prend une fonction en argument.

Par exemple, expliquez ce qui se passe ici:
```python
def myst(f):
	def truc(x):
		return f(f(x))
	return truc
	
def g(c):
	return chr(ord(c) + 3)
	
h = myst(g)
```

Sans  utiliser de  machine,  devinez ce  que renvoie  et  `h(`a`)` et  expliquez
pourquoi. 
Donnez des signatures informelles de ces fonctions (ou utilisez `typing` si vous
n'avez pas peur).

Créez une autre fonction qui puisse être passée en argument à `myst`.

### Dérivées n-èmes

Vous savez toutes et tous qu'une fonction  est dérivable en $a$ si, et seulement
si $ \displaystyle\lim_{h\to 0} \dfrac{f(a+h)-f(a)}{h}$ existe. On note alors $f'(a)$ ce nombre. 
	
Sur  machine, difficile  de  calculer  des limites.  On  va  donc prendre  comme
approximation  de  $f'(a)$  la  velur  de  $  \dfrac{f(a+h)-f(a)}{h}$  pour  une
« petite » valeur de $h$.

Définissez une  fonction `derive(f)`  qui renvoie une  fonction qui  renvoie une
approximation d'une fonction dérivée en un nombre $a$ avec une précision $h$.

Définissez ensuite  une fonction  `derive_n(f,n)` qui renvoie  la dérivée  de la
dérivée...$n$ fois de suite.

Que  devrait   donner  `deriv_n(expo,10)(0)(1e-7)`   avec  `expo`   la  fonction
exponentielle  qui peut  être importée  depuis la  bibliothèque `math`.  Comment
expliquer toutes ces parenthèses ? Quelles indications cela donne pour fabriquer
`derive_n`?

### Enrobage


<iframe width="560" height="315" src="https://www.youtube.com/embed/OlcP0VIx6vc" title="YouTube video player" frameborder="0" allow="accelerometer;  encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



Une fonction, ça peut aussi s'enrober...Observez:

```python
def biscuit():
	return "Je suis une fonction inchangeable"

def deco(f):
	def enrobe():
		s = "\n Une couche de caramel\n"
		s += f()
		s += "\n une couche de chocolat"
		return s
	return enrobe
	
raider = deco(biscuit)

raider()		
```

Essayez dans cette console:

{{ pyodide()}}

Il existe en fait une fonctionnalité  `Python` qui permet d'enrober une fonction
donnée si  on a  défini un *décorateur*.  Il suffit de  l'appeler en  le faisant
précéder d'une `@`.

Reprenons l'exemple précédent. Que donne `biscuit()` ?

```python
def deco(f):
	def enrobe():
		s = "\n Une couche de caramel\n"
		s += f()
		s += "\n une couche de chocolat"
		return s
	return enrobe
	
def biscuit():
	return "Je suis une fonction inchangeable"
	
biscuit()
```

Recommencez  en rajoutant  `#!python  @deco` juste  au-dessus  de `#!python  def
biscuit()`. Que fait `#!python @deco`?


Utilisons nos connaissances nouvelles pour réaliser un décorateur qui 
 va chronométer une fonction quelconque avec un argument.
 Nous allons utiliser `perf_counter_ns` du module `time` comme chronomètre.
 (Consultez https://docs.python.org/fr/3/library/time.html)

L'idée est de noter le temps avant l'appel de la fonction, d'appeller la fonction, de renoter le nouveau temps puis d'afficher la différence des 2 temps mémorisés. 
Implémentez ce nouveau décorateur en l'appelant `@chrono`.
il faudra qu'après l'avoir défini, par exemple, l'appel à :

```python
@chrono
def boucle(n):
    for _ in range(n):
        pass

boucle(10000)
```

affiche le temps pris par cette fonction sur la valeur 10000 .


### Fibonacci et la mémoïsation


<iframe width="560" height="315" src="https://www.youtube.com/embed/t9WWf9tYopk" title="YouTube video player" frameborder="0" allow="accelerometer;  encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


Rappelez-vous de la suite du bon Fibonacci.  Créez une fonction récursive `#! python fibo(n: int)
-> int` qui calcule le $n$-ème terme de  la suite. Évaluez le temps de calcul de
`fibo(15)` avec le décorateur précédent.


En fait,  on s'aperçoit qu'on  effectue souvent les  mêmes appels à  la fonction
`fibo`:



```mermaid
graph TD;
6-->5;
6-->4.1;
5-->4.2;
5-->3.1;
4.1-->3.2;
4.1-->2.1;
4.2-->3.3;
4.2-->2.2;
3.1-->2.3;
3.1-->1.1;
3.2-->2.4;
3.2-->1.2;
2.1-->1.3;
2.1-->0.1;
3.3-->2.5;
3.3-->1.4;
2.2-->1.5;
2.2-->0.2;
2.3-->1.6;
2.3-->0.3;
2.4-->1.7;
2.4-->0.4;
2.5-->1.8;
2.5-->0.5;
```

On fabrique le décorateur suivant:

```python
def memoise(f):
    cache = dict()
    def f_avec_cache(n):
        if n not in cache:
            cache[n] = f(n)
        return cache[n]
    return f_avec_cache
```

En quoi peut-il nous aider ? Nous permet-il d'être plus efficace ?

Et si  on créait  une fonction  non récursive...Que donnent  les mesures  de son
efficacité ?
