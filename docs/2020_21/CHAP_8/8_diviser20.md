---
hide:
  # - navigation # Hide navigation
  #- toc # Hide table of contents
---



{{ chapitre(8, "Diviser pour régner")}}

## With a little help from my friends

Réfléchissons...

<iframe width="560" height="315" src="https://www.youtube.com/embed/-n_mnOjTQlM?start=20&mute=1&showinfo=0&rel=0" title="YouTube video player" frameborder="0" allow="accelerometer;  encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>



On  a  l'impression qu'en  étant  deux,  on ira  plus  vite  pour effectuer  une
tâche. Mais est-ce toujours le cas?


### Maxi

!!! {{ exercice()}}

    === "Énoncé"

        1. Déterminer une fonction `maxi_d(xs: list[int]) -> int` qui renvoie le maximum d'une
        liste d'entiers.  Vous déterminerez récursivement ce  maximum en coupant
        la liste  en deux. On pourra  utiliser `max` pour trouver  le maximum de
        deux entiers.
		```python
		def maxi_d(xs:list[int]) -> int:
		    n = len(xs)
		    if n... :
			    return None
			elif n.... :
			    return ....
			else:
			    return max(maxi(xs[:n//2]), ... )
	    ```
		2. Déterminer ensuite une fonction `maxi` qui fait la même chose mais sans
		couper la liste en 2.
		3. Comparer les temps d'exécution sur différentes listes avec `%timeit`.
		4. Déterminer une  fonction qui renvoie le maximum  d'une matrice (liste
		   de listes).
		   Étudiez et interprétez les résultats suivants:
		   ```python
		   In [194]: m = lambda n: [list(range(n)) for _ in range(n)]

           In [195]: mat = m(100)
		   In [196]: %timeit max_mat(mat)
		   200 µs ± 3.88 µs per loop (mean ± std. dev. of 7 runs, 10000 loops each)

	       In [197]: mat = m(200)

	       In [198]: %timeit max_mat(mat)
		   650 µs ± 2.21 µs per loop (mean ± std. dev. of 7 runs, 1000 loops each)

           In [199]: mat = m(300)

	       In [200]: %timeit max_mat(mat)
	       1.39 ms ± 6.84 µs per loop (mean ± std. dev. of 7 runs, 1000 loops each)

           In [201]: mat = m(400)

           In [202]: %timeit max_mat(mat)
           2.39 ms ± 21.6 µs per loop (mean ± std. dev. of 7 runs, 100 loops each)
           ```
		

	=== "Indications"
        
		Bientôt peut-être...
		

### Tri

Que vous inspire cette image?

![merge](./IMG/merge.png "merge")

### Recherche

		
![usual](./IMG/usual.jpeg "usual suspects"){: align="center" class="centre"  style="width:50%""}

Comment  rechercher un  élément dans  une liste  triée ?  Donner une  version de
complexité linéaire et une autre de complexité logarithmique.



### Algorithme de Karatsouba

On cherche  à multiplier deux  entiers de  $n$ chiffres. Souvenez-vous  de votre
algorithme de l'école primaire...



![mul](./IMG/mul.png "multiplication"){: style="width:30%; align:center"}




* Addition de deux entiers de $n$ chiffres: $O(n)$
* Multiplier un entier de $n$ chiffres par un entier de 1 chiffre: $O(n)$
* Algorithme de l'école  primaire pour multiplier deux  nombres de
$n$ chiffres : $n$ multiplications d'un nombre de $n$ chiffres par un
nombre de 1 chiffre puis une addition des $n$ nombres obtenus: $O(n^2)$
>**Idée**  $O(n^2)$: multiplication  de nombres  deux fois  plus petits  $⟶$
>quatre fois plus rapide ?


$xy=(10^mx_1+x_2)(10^my_1+y_2)=10^{2m}x_1y_1+10^m(x_2y_1+x_1y_2)+x_2y_2$

```
Fonction MUL(x: entier, y:entier) -> entier
Si n==1 Alors
   Retourner x.y
Sinon
   m  <- ENT(n/2)
   x1 <- ENT(x/10^m)
   x2 <- x mod 10^m
   y1 <- ENT(y/10^m)
   y2 <- y mod 10^m
   a  <- MUL(x1, y1, m)
   b  <- MUL(x2, y1, m)
   c  <- MUL(x1, y2, m)
   d  <- MUL(x2, y2, m)
   Retourner a.10^2m + (b +c).10^m + d
FinSi
```

Étudier  le problème  de  la  complexité de  cet  algorithme  nécessite de  bien
maîtriser l'étude des suites numériques...certains verront ça en Prépa.


En Terminale, utilisons juste un peu de bon sens : sachant que la multiplication
est  en  $n^2$,  on  divise  ici  la  taille par  2  mais  se  retrouve  avec  4
sous-problèmes,    plus   précisément    4   sous-problèmes    indépendants   en
$(n/2)^2=n^2/4$ et $4\times  (n^2/4)=n^2$ ce qui n'arrange pas  nos affaires, ce
qui fait dire  au grand Kolmogorov en  1956 qu'on ne fera  sûrement jamais mieux
que du $n^2$. 

Mais  en  1960,  le  jeune  Anatoli  Karatsouba se  souvient  de  ses  cours  de
mathématiques de collège: $bc + ad = ac+bd -(a-b)(c-d)$. 



**En quoi cela simplifie le problème? Quelle est alors la nouvelle complexité?**

$$
x_2y_1+x_1y_2=x_1y_1+x_2y_2-(x_1-x_2)(y_1-y_2)
$$



### Diviser pour régner


>*Le commandement  du grand  nombre est le  même pour le  petit nombre,  ce n'est
>qu'une question de division en groupes.*

>*孙子兵法 VIe siècle avant JC*



On va  pouvoir mettre  au point  un algorithme  *diviser pour  régner* lorsqu'on
pourra résoudre un problème en trois étapes:

1. **Diviser**:  transformer un problème en sous-problèmes de taille plus petite (typiquement deux fois plus petite)
2. **Régner**: ici *conquérir* serait plus approprié car ce terme donne une vision plus dynamique du phénomène. On conquiert chaque sous-problème en appelant récursivement notre méthode de division sur des sous-problèmes de plus en plus petits jusqu'à devenir résolvables en temps constant.
3. **Reconstruire**: on combine tous les sous-problèmes résolus de proche en proche jusqu'à obtenir la solution du problème initial.
Les Babyloniens n'ont pas attendu d'avoir des ordinateurs et ont eu l'idée d'améliorer les recherches dans une liste si elle est déjà  triée avec ce que nous avons appelé la *recherche dichotomique*. 

On peut formaliser la méthode en *pseudo*-python:

```python
def DPR(Tab: liste, i: indice , j: indice) :
    if petit_probleme(Tab, i, j):
        return solution(Tab, i, j)
    else:
        spb1 = DPR(Tab, i, (i+j)//2)
        spb2 = DPR(Tab, (i+j)//2, j)
        return combine(spb1, spb2)
```

Cela s'applique bien au tri fusion.  Pour Karatsouba, on a trois sous-problèmes,
pour la recherche dichotomique, on élimine un des deux sous-problèmes. 







!!! {{exercice("Multiplication du paysan russe")}}

    === "Énoncé"
	
		Voici ce qu'apprenaient les petits soviétiques pour multiplier deux
		entiers. Une variante de cet algorithme a été retrouvée sur le papyrus
		de Rhind datant de 1650 avant JC, le scribe Ahmes affirmant que cet
		algorithme était à l'époque vieux de 350 ans. Il a survécu en Europe
		occidentale jusqu'aux travaux de Fibonacci.
		```console
		FONCTION MULRUSSE( x entier ,y: entier, acc:entier) → entier
		   SI x == 0
			  RETOURNΕ acc
		   SINON
			  SI x est pair
				 RETOURNE MULRUSSE(x/2, y*2, acc)
			  SINON
				 RETOURNE MULRUSSE((x-1)/2, y*2, acc + y)
			  FINSI
		   FINSI
		```

		1. Que vaut `acc` au départ?
		1. Écrivez une version récursive de cet algorithme en évitant l'alternative
		des lignes 5 à 9.
		1. Écrivez une version impérative de cet algorithme.
		1. Prouvez la correction de cet algorithme.
		1. Étudiez sa complexité.
		1. En python, `x >> 1` décale l'entier *x* d'un bit vers la doite et `x << 1` décale *x* d'un
		bit vers la gauche en complétant par un zéro à droite, `x & y` renvoie l'entier
		obtenu en faisant la conjonction logique bit à bit des représentations
		binaires de *x* et *y* et `~x` renvoie le complément à 1 de *x*.
		Ré-écrivez la multiplication russe en n'utilisant que ces opérations bit à
		bit (pas de division ni de multiplication).
   
    === "Indications"
    
		```python
		def mulRusse(x: int, y: int, acc: int = 0) -> int:
			if x == 0:
				return acc
			else:
				return mulRusse(x >> 1, y << 1, acc + (y & (-(x & 1))))


		# x      y    acc
		# 140   305   0
		# 70    610   0
		# 35    1220  0
		# 17    2440  1220
		# 8     4880  1220 + 2440
		# 4     9760  3660
		# 2    19520  3660
		# 1    39040  3660
		# 0    *****  3660 + 39040 = 42700


		def mulRusseImp(x: int, y: int) -> int:
			acc = 0
			while x != 0:
				acc += y*(x%2)
				x //= 2
				y *= 2
				# invariant : res = x*y + acc 
			return acc # = x*y + acc = res

		# si x_n est pair :
		# res_(n + 1) =  x_n/2 * y_n*2 + acc_n = x_n*y_n + acc_n = res_n
		# si x_n impair
		# res_(n + 1) = (x_n - 1)/2 * y_n*2 + acc_n + y_n = (x_n-1)*y_n + acc_n + y_n = x_n*y_n - y_n + acc_n + y_n = res_n
		# On vérifie ainsi qu'à chaque étape x*y + acc est constant

		# On fait log_2(x) + 1 étapes : complexité en log_2(x)

		#  b7 b6 b5 b4 b3 b2 b1 b0  = y
		#  0  0  0  0  0  0  0  0   =   (x&1 = 0)  ⟺ x pair
		#  0  0  0  0  0  0  0  0   = y & -(x&1) = y * (x&1)

		#  b7 b6 b5 b4 b3 b2 b1 b0  = y
		#  1  1  1  1  1  1  1  1   =     (x&1 = 1)  ⟺ x impair
		#  b7 b6 b5 b4 b3 b2 b1 b0  = y & -(x&1) = y * (x&1)


		# b = 170 : 
		# 171 = 10101011
		# comp 1 = 01010100
		# ~b = -171 = comp 1 + 1 : 01010101

		# ~b 01010101
		#  b 10101010

		# b = 00000001 = 1
		# comp 1 = 11111110
		# comp 1 + 1 = 11111111 = -1
		```


!!! {{exercice("Plus long préfixe commun")}}

    Le travail sur les  chaînes de caractère est à la mode  dans le programme de
    Terminale.  En voici  un nouvel  avatar :  on a  une série  de chaînes  (des
    chaînes de  nucléotides ?) et on  voudrait avoir la longueur  des plus longs
    préfixes communs (les  généticiens ont des raisons que la  raison ne connait
    pas). 

    ```python
    >>> ns = ['gatacca', 'gatatata', 'gatagagac', 'gata']
    >>> plus_long_prefix(ns)
    gata
    ```


!!! {{ exercice("Recherche du plus fort")}}

    On suppose  qu'on dispose de  $n$ éléments  distincts dans un  tableau. La
    seule  chose qu'on  sait faire  avec ces  éléments consiste  à dire  si deux
    éléments sont  égaux ou  non. On  dit qu'un  élément est  *majoritaire* s'il
    apparaît  strictement  plus  que  $n/2$  fois dans  le  tableau.  Pour  se
    simplifier la  vie, on peut supposer  au départ que $n$  est une puissance
    de 2. 
    
	1. Donner une fonction qui calcule le nombre d'occurrences d'un élément donné
	   dans  une  liste  Python.  Déduisez-en un  algorithme  naïf  qui  vérifie
	   l'existence d'un élément majoritaire. Quelle est sa complexité au pire ? 
    2. Déterminer une fonction récursive qui utilise le principe de diviser pour
       régner. Complexité ?
    3. Et si $n$ n'est pas une puissance de 2?


!!! {{ exercice("Tri rapide")}}
    
	Ce tri  est un  des plus  célèbres et  a été  proposé en  1959 par  Sir Tony
	(Hoare). L'idée est simple  : on choisit un pivot, on  place les plus petits
	que  lui  à sa  gauche  et  les  plus grands  à  sa  droite et  on  continue
	récursivement jusqu'à obtenir une liste de longueur 1. 
 

!!! {{ exercice("Deux points les plus proches (pas facile...défi pour les motivés)")}}

    On  a  $n$  points  dans   le  plan  caractérisés  par  leurs  coordonnées
    cartésiennes. On voudrait  les deux plus proches. Commencez par  de la force
    brute  puis  essayez  de  diviser pour  régner...Vous  pourrez  par  exemple
    considérer que le plan se divise en deux catégories : les points à gauche du
    point milieu (en considérant les points  horizontalement) et les points à sa
    droite. 
	
    Vous                   pourrez                    lire                   [ce
    document](http://people.csail.mit.edu/indyk/6.838-old/handouts/lec17.pdf)
    pour  vous   aider.  Ce  qui  est   intéressant  ici  c'est  que   c'est  la
    reconstruction qui est compliquée. 
	
    ![close pair](./IMG/closepair.png)

